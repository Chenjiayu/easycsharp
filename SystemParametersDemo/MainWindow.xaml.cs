﻿using EasyCSharp.WPF.Controls;
using EasyCSharp.WPF.Core;
using EasyCSharp.WPF.DirectShow;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace SystemParametersDemo
{
    public class FolderItem : BindableObject
    {
        public FolderItem()
        {
            Items = new ObservableCollection<FolderItem>();
        }

        public ImageSource Icon
        {
            get { return GetValue(() => Icon); }
            set { SetValue(() => Icon, value); }
        }


        public string Path
        {
            get { return GetValue(() => Path); }
            set { SetValue(() => Path, value); }
        }

        public string Name
        {
            get { return GetValue(() => Name); }
            set { SetValue(() => Name, value); }
        }


        public ObservableCollection<FolderItem> Items
        {
            get { return GetValue(() => Items); }
            set { SetValue(() => Items, value); }
        }
    }

    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : EasyWindow
    {
        string Root;

        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
            this.DataContext = this;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void EasyButton_Click(object sender, RoutedEventArgs e)
        {
           
        }

       

    }
}

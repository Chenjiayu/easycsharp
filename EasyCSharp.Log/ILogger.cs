﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.Log
{
    /// <summary>
    /// 公共日志服务接口
    /// </summary>
    public interface ILogger
    {
        void WriteInfo(string info);

        void WriteError(string info, Exception e);
    }
}

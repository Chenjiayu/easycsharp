﻿using EasyCSharp.Device;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSahrp.Camera
{
    /// <summary>
    /// 一个获取视频流捕获设备
    /// </summary>
    public interface IVideoCaptureDevice : IDevice
    {

    }

    /// <summary>
    /// 摄像头设备
    /// </summary>
    public interface ICamera: IVideoCaptureDevice
    {

    }
}

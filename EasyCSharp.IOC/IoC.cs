﻿using EasyCSharp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.IOC
{
    public class IoC
    {
        public static Func<Type, string, object> GetInstance = (service, key) =>
        {
            throw new InvalidOperationException("IoC容器尚未初始化。");
        };

        public static Func<Type, IEnumerable<object>> GetAllInstances = service =>
        {
            throw new InvalidOperationException("IoC容器尚未初始化。");
        };

        public static Action<object> BuildUp = instance =>
        {
            throw new InvalidOperationException("IoC容器尚未初始化。");
        };

        public static T Get<T>(string key = null)
        {
            return (T)GetInstance(typeof(T), key);
        }

        public static object Get(string key)
        {
            var type = FindTypeByName(key);

            return type == null ? null : ReflectionHelper.GetInstance(type);
        }

        public static IEnumerable<T> GetAll<T>()
        {
            return GetAllInstances(typeof(T)).Cast<T>();
        }

        public static IEnumerable<T> GetAll<T, TService>()
        {
            return GetAllInstances(typeof(TService)).Cast<T>();
        }

        public static List<Assembly> Assemblies;

        /// <summary>
        /// 通过类型全称寻找类型
        /// </summary>
        public static Func<string, Type> FindTypeByName = name =>
        {
            if (name == null || name == string.Empty)
            {
                return null;
            }

            var componentTypes = Assemblies?.SelectMany(a => a.GetExportedTypes());

            var entryTypes = Assembly.GetEntryAssembly().ExportedTypes;

            var types = componentTypes.Union(entryTypes).Distinct();

            var type = types?.FirstOrDefault(t => t.FullName == name || t.Name == name);

            return type;
        };
    }
}

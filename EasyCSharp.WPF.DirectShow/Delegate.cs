﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.WPF.DirectShow
{
    /// <summary>
    /// Delegate signature to notify of a new surface
    /// </summary>
    /// <param name="sender">The sender of the event</param>
    /// <param name="pSurface">The pointer to the D3D surface</param>
    public delegate void NewAllocatorSurfaceDelegate(object sender, IntPtr pSurface);
}

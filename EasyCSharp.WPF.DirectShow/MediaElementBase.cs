﻿using DirectShowLib;
using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;
using Application = System.Windows.Application;

namespace EasyCSharp.WPF.DirectShow
{
    /// <summary>
    /// The MediaElementBase is the base WPF control for
    /// making custom media players.  The MediaElement uses the
    /// D3DRenderer class for rendering video
    /// </summary>
    public abstract class MediaElementBase : D3DRenderer
    {
        private Window m_currentWindow;
        private bool m_windowHooked;

        ~MediaElementBase()
        {

        }

        #region Routed Events
        #region MediaOpened

        public static readonly RoutedEvent MediaOpenedEvent = EventManager.RegisterRoutedEvent("MediaOpened",
                                                                                               RoutingStrategy.Bubble,
                                                                                               typeof(RoutedEventHandler
                                                                                                   ),
                                                                                               typeof(MediaElementBase));

        /// <summary>
        /// Fires when media has successfully been opened
        /// </summary>
        public event RoutedEventHandler MediaOpened
        {
            add { AddHandler(MediaOpenedEvent, value); }
            remove { RemoveHandler(MediaOpenedEvent, value); }
        }

        #endregion

        #region MediaClosed

        public static readonly RoutedEvent MediaClosedEvent = EventManager.RegisterRoutedEvent("MediaClosed",
                                                                                               RoutingStrategy.Bubble,
                                                                                               typeof(RoutedEventHandler),
                                                                                               typeof(MediaElementBase));

        /// <summary>
        /// Fires when media has been closed
        /// </summary>
        public event RoutedEventHandler MediaClosed
        {
            add { AddHandler(MediaClosedEvent, value); }
            remove { RemoveHandler(MediaClosedEvent, value); }
        }

        #endregion

        #region MediaEnded

        public static readonly RoutedEvent MediaEndedEvent = EventManager.RegisterRoutedEvent("MediaEnded",
                                                                                              RoutingStrategy.Bubble,
                                                                                              typeof(RoutedEventHandler),
                                                                                              typeof(MediaElementBase));

        /// <summary>
        /// Fires when media has completed playing
        /// </summary>
        public event RoutedEventHandler MediaEnded
        {
            add { AddHandler(MediaEndedEvent, value); }
            remove { RemoveHandler(MediaEndedEvent, value); }
        }

        #endregion
        #endregion

        #region Dependency Properties
        #region UnloadedBehavior

        public static readonly DependencyProperty UnloadedBehaviorProperty =
            DependencyProperty.Register("UnloadedBehavior", typeof(MediaState), typeof(MediaElementBase),
                                        new FrameworkPropertyMetadata(MediaState.Close));

        /// <summary>
        /// Defines the behavior of the control when it is unloaded
        /// </summary>
        public MediaState UnloadedBehavior
        {
            get { return (MediaState)GetValue(UnloadedBehaviorProperty); }
            set { SetValue(UnloadedBehaviorProperty, value); }
        }

        #endregion

        #region LoadedBehavior

        public static readonly DependencyProperty LoadedBehaviorProperty =
            DependencyProperty.Register("LoadedBehavior", typeof(MediaState), typeof(MediaElementBase),
                                        new FrameworkPropertyMetadata(MediaState.Play));

        /// <summary>
        /// Defines the behavior of the control when it is loaded
        /// </summary>
        public MediaState LoadedBehavior
        {
            get { return (MediaState)GetValue(LoadedBehaviorProperty); }
            set { SetValue(LoadedBehaviorProperty, value); }
        }

        #endregion

        #region Volume

        public static readonly DependencyProperty VolumeProperty =
            DependencyProperty.Register("Volume", typeof(double), typeof(MediaElementBase),
                new FrameworkPropertyMetadata(1.0d,
                    new PropertyChangedCallback(OnVolumeChanged)));

        /// <summary>
        /// Gets or sets the audio volume.  Specifies the volume, as a 
        /// number from 0 to 1.  Full volume is 1, and 0 is silence.
        /// </summary>
        public double Volume
        {
            get { return (double)GetValue(VolumeProperty); }
            set { SetValue(VolumeProperty, value); }
        }

        private static void OnVolumeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((MediaElementBase)d).OnVolumeChanged(e);
        }

        protected virtual void OnVolumeChanged(DependencyPropertyChangedEventArgs e)
        {
            if (HasInitialized)
                MediaPlayerBase.Dispatcher.BeginInvoke((Action)delegate
                {
                    MediaPlayerBase.Volume = (double)e.NewValue;
                });
        }

        #endregion

        #region Balance

        public static readonly DependencyProperty BalanceProperty =
            DependencyProperty.Register("Balance", typeof(double), typeof(MediaElementBase),
                new FrameworkPropertyMetadata(0d,
                    new PropertyChangedCallback(OnBalanceChanged)));

        /// <summary>
        /// Gets or sets the balance on the audio.
        /// The value can range from -1 to 1. The value -1 means the right channel is attenuated by 100 dB 
        /// and is effectively silent. The value 1 means the left channel is silent. The neutral value is 0, 
        /// which means that both channels are at full volume. When one channel is attenuated, the other 
        /// remains at full volume.
        /// </summary>
        public double Balance
        {
            get { return (double)GetValue(BalanceProperty); }
            set { SetValue(BalanceProperty, value); }
        }

        private static void OnBalanceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((MediaElementBase)d).OnBalanceChanged(e);
        }

        protected virtual void OnBalanceChanged(DependencyPropertyChangedEventArgs e)
        {
            if (HasInitialized)
                MediaPlayerBase.Dispatcher.BeginInvoke((Action)delegate
                {
                    MediaPlayerBase.Balance = (double)e.NewValue;
                });
        }

        #endregion

        #region IsPlaying

        private static readonly DependencyPropertyKey IsPlayingPropertyKey
            = DependencyProperty.RegisterReadOnly("IsPlaying", typeof(bool), typeof(MediaElementBase),
                new FrameworkPropertyMetadata(false));

        public static readonly DependencyProperty IsPlayingProperty
            = IsPlayingPropertyKey.DependencyProperty;

        public bool IsPlaying
        {
            get { return (bool)GetValue(IsPlayingProperty); }
        }

        protected void SetIsPlaying(bool value)
        {
            SetValue(IsPlayingPropertyKey, value);
        }
        #endregion

        #endregion

        #region Commands
        public static readonly RoutedCommand PlayerStateCommand = new RoutedCommand();
        public static readonly RoutedCommand TogglePlayPauseCommand = new RoutedCommand();

        protected virtual void OnPlayerStateCommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Parameter is MediaState == false)
                return;

            var state = (MediaState)e.Parameter;

            ExecuteMediaState(state);
        }

        protected virtual void OnCanExecutePlayerStateCommand(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        protected virtual void OnTogglePlayPauseCommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (IsPlaying)
                Pause();
            else
                Play();
        }

        protected virtual void OnCanExecuteTogglePlayPauseCommand(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        #endregion

        /// <summary>
        /// Notifies when the media has failed and produced an exception
        /// </summary>
        public event EventHandler<MediaFailedEventArgs> MediaFailed;

        protected MediaElementBase()
        {
            DefaultApartmentState = ApartmentState.MTA;

            InitializeMediaPlayerPrivate();
            Loaded += MediaElementBaseLoaded;
            Unloaded += MediaElementBaseUnloaded;



            CommandBindings.Add(new CommandBinding(PlayerStateCommand,
                                                   OnPlayerStateCommandExecuted,
                                                   OnCanExecutePlayerStateCommand));

            CommandBindings.Add(new CommandBinding(TogglePlayPauseCommand,
                                                   OnTogglePlayPauseCommandExecuted,
                                                   OnCanExecuteTogglePlayPauseCommand));
        }

        private void InitializeMediaPlayerPrivate()
        {
            InitializeMediaPlayer();
        }

        protected MediaPlayerBase MediaPlayerBase { get; set; }

        protected ApartmentState DefaultApartmentState { get; set; }

        protected void EnsurePlayerThread()
        {
            MediaPlayerBase.EnsureThread(DefaultApartmentState);
        }

        /// <summary>
        /// Initializes the media player, hooking into events
        /// and other general setup.
        /// </summary>
        protected virtual void InitializeMediaPlayer()
        {
            if (MediaPlayerBase != null)
                return;

            MediaPlayerBase = OnRequestMediaPlayer();
            EnsurePlayerThread();

            if (MediaPlayerBase == null)
            {
                throw new Exception("OnRequestMediaPlayer cannot return null");
            }

            /* Hook into the normal .NET events */
            MediaPlayerBase.MediaOpened += OnMediaPlayerOpenedPrivate;
            MediaPlayerBase.MediaClosed += OnMediaPlayerClosedPrivate;
            MediaPlayerBase.MediaFailed += OnMediaPlayerFailedPrivate;
            MediaPlayerBase.MediaEnded += OnMediaPlayerEndedPrivate;

            /* These events fire when we get new D3Dsurfaces or frames */
            MediaPlayerBase.NewAllocatorFrame += OnMediaPlayerNewAllocatorFramePrivate;
            MediaPlayerBase.NewAllocatorSurface += OnMediaPlayerNewAllocatorSurfacePrivate;
        }

        #region Private Event Handlers
        private void OnMediaPlayerFailedPrivate(object sender, MediaFailedEventArgs e)
        {
            OnMediaPlayerFailed(e);
        }

        private void OnMediaPlayerNewAllocatorSurfacePrivate(object sender, IntPtr pSurface)
        {
            OnMediaPlayerNewAllocatorSurface(pSurface);
        }

        private void OnMediaPlayerNewAllocatorFramePrivate()
        {
            OnMediaPlayerNewAllocatorFrame();
        }

        private void OnMediaPlayerClosedPrivate()
        {
            OnMediaPlayerClosed();
        }

        private void OnMediaPlayerEndedPrivate()
        {
            OnMediaPlayerEnded();
        }

        private void OnMediaPlayerOpenedPrivate()
        {
            OnMediaPlayerOpened();
        }
        #endregion

        /// <summary>
        /// Fires the MediaFailed event
        /// </summary>
        /// <param name="e">The failed media arguments</param>
        protected void InvokeMediaFailed(MediaFailedEventArgs e)
        {
            EventHandler<MediaFailedEventArgs> mediaFailedHandler = MediaFailed;
            if (mediaFailedHandler != null) mediaFailedHandler(this, e);
        }

        /// <summary>
        /// Executes when a media operation failed
        /// </summary>
        /// <param name="e">The failed event arguments</param>
        protected virtual void OnMediaPlayerFailed(MediaFailedEventArgs e)
        {
            Dispatcher.BeginInvoke((Action)(() => SetIsPlaying(false)));
            InvokeMediaFailed(e);
        }

        /// <summary>
        /// Is executes when a new D3D surfaces has been allocated
        /// </summary>
        /// <param name="pSurface">The pointer to the D3D surface</param>
        protected virtual void OnMediaPlayerNewAllocatorSurface(IntPtr pSurface)
        {
            SetBackBuffer(pSurface);
        }

        /// <summary>
        /// Called for every frame in media that has video
        /// </summary>
        protected virtual void OnMediaPlayerNewAllocatorFrame()
        {
            InvalidateVideoImage();
        }

        /// <summary>
        /// Called when the media has been closed
        /// </summary>
        protected virtual void OnMediaPlayerClosed()
        {
            Dispatcher.BeginInvoke((Action)(() => SetIsPlaying(false)));
            Dispatcher.BeginInvoke((Action)(() => RaiseEvent(new RoutedEventArgs(MediaClosedEvent))));
        }

        /// <summary>
        /// Called when the media has ended
        /// </summary>
        protected virtual void OnMediaPlayerEnded()
        {
            Dispatcher.BeginInvoke((Action)(() => SetIsPlaying(false)));
            Dispatcher.BeginInvoke((Action)(() => RaiseEvent(new RoutedEventArgs(MediaEndedEvent))));
        }

        /// <summary>
        /// Executed when media has successfully been opened.
        /// </summary>
        protected virtual void OnMediaPlayerOpened()
        {
            /* Safely grab out our values */
            bool hasVideo = MediaPlayerBase.HasVideo;
            int videoWidth = MediaPlayerBase.NaturalVideoWidth;
            int videoHeight = MediaPlayerBase.NaturalVideoHeight;
            double volume;
            double balance;

            Dispatcher.BeginInvoke((Action)delegate
            {
                /* If we have no video just black out the video
                 * area by releasing the D3D surface */
                if (!hasVideo)
                {
                    SetBackBuffer(IntPtr.Zero);
                }

                SetNaturalVideoWidth(videoWidth);
                SetNaturalVideoHeight(videoHeight);

                /* Set our dp values to match the media player */
                SetHasVideo(hasVideo);

                /* Get our DP values */
                volume = Volume;
                balance = Balance;

                /* Make sure our volume and balances are set */
                MediaPlayerBase.Dispatcher.BeginInvoke((Action)delegate
                {
                    MediaPlayerBase.Volume = volume;
                    MediaPlayerBase.Balance = balance;
                });
                SetIsPlaying(true);
                RaiseEvent(new RoutedEventArgs(MediaOpenedEvent));
            });
        }

        /// <summary>
        /// Fires when the owner window is closed.  Nothing will happen
        /// if the visual does not belong to the visual tree with a root
        /// of a WPF window
        /// </summary>
        private void WindowOwnerClosed(object sender, EventArgs e)
        {
            ExecuteMediaState(UnloadedBehavior);
        }

        /// <summary>
        /// Local handler for the Loaded event
        /// </summary>
        private void MediaElementBaseUnloaded(object sender, RoutedEventArgs e)
        {
            /* Make sure we call our virtual method every time! */
            OnUnloadedOverride();

            if (Application.Current == null)
                return;

            m_windowHooked = false;

            if (m_currentWindow == null)
                return;

            m_currentWindow.Closed -= WindowOwnerClosed;
            m_currentWindow = null;
        }

        /// <summary>
        /// Local handler for the Unloaded event
        /// </summary>
        private void MediaElementBaseLoaded(object sender, RoutedEventArgs e)
        {
            m_currentWindow = Window.GetWindow(this);

            if (m_currentWindow != null && !m_windowHooked)
            {
                m_currentWindow.Closed += WindowOwnerClosed;
                m_windowHooked = true;
            }

            OnLoadedOverride();
        }

        /// <summary>
        /// Runs when the Loaded event is fired and executes
        /// the LoadedBehavior
        /// </summary>
        protected virtual void OnLoadedOverride()
        {
            ExecuteMediaState(LoadedBehavior);
        }

        /// <summary>
        /// Runs when the Unloaded event is fired and executes
        /// the UnloadedBehavior
        /// </summary>
        protected virtual void OnUnloadedOverride()
        {
            ExecuteMediaState(UnloadedBehavior);
        }

        /// <summary>
        /// Executes the actions associated to a MediaState
        /// </summary>
        /// <param name="state">The MediaState to execute</param>
        protected void ExecuteMediaState(MediaState state)
        {
            switch (state)
            {
                case MediaState.Manual:
                    break;
                case MediaState.Play:
                    Play();
                    break;
                case MediaState.Stop:
                    Stop();
                    break;
                case MediaState.Close:
                    Close();
                    break;
                case MediaState.Pause:
                    Pause();
                    break;
                default:
                    throw new ArgumentOutOfRangeException("state");
            }
        }

        public override void BeginInit()
        {
            HasInitialized = false;
            base.BeginInit();
        }

        public override void EndInit()
        {
            double balance = Balance;
            double volume = Volume;

            MediaPlayerBase.Dispatcher.BeginInvoke((Action)delegate
            {
                MediaPlayerBase.Balance = balance;
                MediaPlayerBase.Volume = volume;
            });

            HasInitialized = true;
            base.EndInit();
        }

        public bool HasInitialized
        {
            get;
            protected set;
        }

        /// <summary>
        /// Plays the media
        /// </summary>
        public virtual void Play()
        {
            MediaPlayerBase.EnsureThread(DefaultApartmentState);
            MediaPlayerBase.Dispatcher.BeginInvoke((Action)(delegate
            {
                MediaPlayerBase.Play();
                Dispatcher.BeginInvoke(((Action)(() => SetIsPlaying(true))));
            }));

        }

        /// <summary>
        /// Pauses the media
        /// </summary>
        public virtual void Pause()
        {
            MediaPlayerBase.EnsureThread(DefaultApartmentState);
            MediaPlayerBase.Dispatcher.BeginInvoke((Action)(() => MediaPlayerBase.Pause()));
            SetIsPlaying(false);
        }

        /// <summary>
        /// Closes the media
        /// </summary>
        public virtual void Close()
        {
            SetBackBuffer(IntPtr.Zero);
            InvalidateVideoImage();

            if (!MediaPlayerBase.Dispatcher.ShuttingOrShutDown)
                MediaPlayerBase.Dispatcher.BeginInvoke((Action)(delegate
                {
                    MediaPlayerBase.Close();
                    MediaPlayerBase.Dispose();
                }));

            SetIsPlaying(false);
        }

        /// <summary>
        /// Stops the media
        /// </summary>
        public virtual void Stop()
        {
            if (!MediaPlayerBase.Dispatcher.ShuttingOrShutDown)
                MediaPlayerBase.Dispatcher.BeginInvoke((Action)(() => MediaPlayerBase.Stop()));

            SetIsPlaying(false);
        }

        /// <summary>
        /// Called when a MediaPlayerBase is required.
        /// </summary>
        /// <returns>This method must return a valid (not null) MediaPlayerBase</returns>
        protected virtual MediaPlayerBase OnRequestMediaPlayer()
        {
            return null;
        }

        [ComVisible(true)]
        public class EvrPresenter : ICustomAllocator, IEVRPresenterCallback
        {
            private const int PRESENTER_BUFFER_COUNT = 5;
            private IntPtr m_lastSurface;
            private IMFVideoPresenter m_VideoPresenter;

            private EvrPresenter()
            {
            }

            ~EvrPresenter()
            {
                Dispose(false);
            }

            #region Interop

            /// <summary>
            /// The GUID of our EVR custom presenter COM object
            /// </summary>
            private static readonly Guid EVR_PRESENTER_CLSID = new Guid(0x9807fc9c, 0x807b, 0x41e3, 0x98, 0xa8, 0x75, 0x17,
                                                                        0x6f, 0x95, 0xa0, 0x63);
            /*
                    /// <summary>
                    /// The GUID of IUnknown
                    /// </summary>
                    private static readonly Guid IUNKNOWN_GUID = new Guid("{00000000-0000-0000-C000-000000000046}");

                    /// <summary>
                    /// Static method in the 32 bit dll to create our IClassFactory
                    /// </summary>
                    [PreserveSig]
                    [DllImport("EvrPresenter32.dll", EntryPoint = "DllGetClassObject")]
                    private static extern int DllGetClassObject32([MarshalAs(UnmanagedType.LPStruct)] Guid clsid,
                                                                  [MarshalAs(UnmanagedType.LPStruct)] Guid riid,
                                                                  [MarshalAs(UnmanagedType.IUnknown)] out object ppv);

                    /// <summary>
                    /// Static method in the 62 bit dll to create our IClassFactory
                    /// </summary>
                    [PreserveSig]
                    [DllImport("EvrPresenter64.dll", EntryPoint = "DllGetClassObject")]
                    private static extern int DllGetClassObject64([MarshalAs(UnmanagedType.LPStruct)] Guid clsid,
                                                                  [MarshalAs(UnmanagedType.LPStruct)] Guid riid,
                                                                  [MarshalAs(UnmanagedType.IUnknown)] out object ppv);
            */
            #endregion
            /*
            /// <summary>
            /// Returns the bittage of this process, ie 32 or 64 bit
            /// </summary>
            private static int ProcessBits
            {
                get { return IntPtr.Size * 8; }
            }
    */
            /// <summary>
            /// The custom EVR video presenter COM object
            /// </summary>
            public IMFVideoPresenter VideoPresenter
            {
                get { return m_VideoPresenter; }
                private set { m_VideoPresenter = value; }
            }

            #region ICustomAllocator Members
            /// <summary>
            /// Invokes when a new frame has been allocated
            /// to a surface
            /// </summary>
            public event Action NewAllocatorFrame;

            /// <summary>
            /// Invokes when a new surface has been allocated
            /// </summary>
            public event NewAllocatorSurfaceDelegate NewAllocatorSurface;

            #region IDisposable

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected void Dispose(bool dispose)
            {
                if (dispose)
                {
                    var settings = m_VideoPresenter as IEVRPresenterSettings;

                    if (settings != null)
                        settings.RegisterCallback(null);
                }
                COMUtil.TryFinalRelease(ref m_VideoPresenter);
            }

            public void Stop()
            {
                var settings = m_VideoPresenter as IEVRPresenterSettings;

                if (settings != null)
                    settings.RegisterCallback(null);
            }

            #endregion

            #endregion

            #region IEVRPresenterCallback Members

            /// <summary>
            /// Called by the custom EVR Presenter, notifying that
            /// there is a new D3D surface and/or there needs to be
            /// a frame rendered
            /// </summary>
            /// <param name="pSurface">The Direct3D surface</param>
            /// <returns>A HRESULT</returns>
            public int PresentSurfaceCB(IntPtr pSurface)
            {
                /* Check if the surface is the same as the last*/
                if (m_lastSurface != pSurface)
                    InvokeNewAllocatorSurface(pSurface);

                /* Store ref to the pointer so we can compare
                 * it next time this method is called */
                m_lastSurface = pSurface;

                InvokeNewAllocatorFrame();
                return 0;
            }

            #endregion

            /// <summary>
            /// Create a new EVR video presenter
            /// </summary>
            /// <returns></returns>
            public static EvrPresenter CreateNew()
            {
                //            object comObject;
                //            int hr;



                /* Our exception var we use to hold the exception
                 * until we need to throw it (after clean up) */
                //           Exception exception = null;

                /* A COM object we query form our native library */
                //          IClassFactory factory = null;

                /* Create our 'helper' class */
                var evrPresenter = new EvrPresenter();
                IMFVideoPresenter presenter = null;
                try
                {
                    var path = System.IO.Path.GetDirectoryName(new Uri(typeof(EvrPresenter).Assembly.CodeBase).LocalPath);
                    var dlltoload = System.IO.Path.Combine(path, IntPtr.Size == 8 ? @"EvrPresenter64.dll" : @"EvrPresenter32.dll");
                    presenter = COMUtil.CreateFromDll<IMFVideoPresenter>(dlltoload, EVR_PRESENTER_CLSID);

                    int count;
                    var settings = presenter as IEVRPresenterSettings;
                    DsError.ThrowExceptionForHR(settings.RegisterCallback(evrPresenter));
                    DsError.ThrowExceptionForHR(settings.GetBufferCount(out count));
                    DsError.ThrowExceptionForHR(settings.SetBufferCount(PRESENTER_BUFFER_COUNT));

                    /* Populate the IMFVideoPresenter */
                    evrPresenter.VideoPresenter = presenter;

                }
                catch (Exception ex)
                {
                    COMUtil.TryFinalRelease(ref presenter);
                    throw new Exception("Could not create EnhancedVideoRenderer", ex);
                }

                return evrPresenter;

                //         /* Call the DLL export to create the class factory */
                /*       if(ProcessBits == 32)
                               hr = DllGetClassObject32(EVR_PRESENTER_CLSID, IUNKNOWN_GUID, out comObject);
                           else if(ProcessBits == 64)
                               hr = DllGetClassObject64(EVR_PRESENTER_CLSID, IUNKNOWN_GUID, out comObject);
                           else
                           {
                               exception = new Exception(string.Format("{0} bit processes are unsupported", ProcessBits));
                               goto bottom;
                           }
               */
                /* Check if our call to our DLL failed */
                /*          if(hr != 0 || comObject == null)
                            {
                                exception = new COMException("Could not create a new class factory.", hr);
                                goto bottom;
                            }
                */
                /* Cast the COM object that was returned to a COM interface type */
                /*          factory = comObject as IClassFactory;

                            if(factory == null)
                            {
                                exception = new Exception("Could not QueryInterface for the IClassFactory interface");
                                goto bottom;
                            }

                            /* Get the GUID of the IMFVideoPresenter */
                //          Guid guidVideoPresenter = typeof(IMFVideoPresenter).GUID;

                /* Creates a new instance of the IMFVideoPresenter */
                //          factory.CreateInstance(null, ref guidVideoPresenter, out comObject);

                /* QueryInterface for the IMFVideoPresenter */
                //           var presenter = comObject as IMFVideoPresenter;

                /* QueryInterface for our callback registration interface */
                /*            var registerCb = comObject as IEVRPresenterRegisterCallback;
                            if(registerCb == null)
                            {
                                exception = new Exception("Could not QueryInterface for IEVRPresenterRegisterCallback");
                                goto bottom;
                            }
                */
                /* Register the callback to the 'helper' class we created */
                //            registerCb.RegisterCallback(evrPresenter);

                /* Populate the IMFVideoPresenter */
                //            evrPresenter.VideoPresenter = presenter;

                //            bottom:

                //          if(factory != null)
                //              Marshal.FinalReleaseComObject(factory);

                /*            if(exception != null)
                                throw exception;

                            return evrPresenter;*/
            }

            #region Event Invokers
            /// <summary>
            /// Fires the NewAllocatorFrame event
            /// </summary>
            private void InvokeNewAllocatorFrame()
            {
                var newAllocatorFrameAction = NewAllocatorFrame;
                if (newAllocatorFrameAction != null) newAllocatorFrameAction();
            }

            /// <summary>
            /// Fires the NewAlloctorSurface event
            /// </summary>
            /// <param name="pSurface">D3D surface pointer</param>
            private void InvokeNewAllocatorSurface(IntPtr pSurface)
            {
                var del = NewAllocatorSurface;
                if (del != null) del(this, pSurface);
            }
            #endregion
        }
    }
}

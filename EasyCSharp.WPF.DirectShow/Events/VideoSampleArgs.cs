﻿using System;
using System.Drawing;

namespace EasyCSharp.WPF.DirectShow
{
    public class VideoSampleArgs : EventArgs
    {
        public Bitmap VideoFrame { get; internal set; }
    }
}

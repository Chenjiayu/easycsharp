﻿using System;

namespace EasyCSharp.WPF.DirectShow
{
    public class MediaFailedEventArgs : EventArgs
    {
        public MediaFailedEventArgs(string message, Exception exception)
        {
            Message = message;
            Exception = exception;
        }

        public Exception Exception { get; protected set; }
        public string Message { get; protected set; }
    }
}

﻿using System;

namespace EasyCSharp.WPF.DirectShow
{
    public class ShutdownFinishedEventArgs : EventArgs
    {
        public bool CancelShutdown { get; set; }
    }
}

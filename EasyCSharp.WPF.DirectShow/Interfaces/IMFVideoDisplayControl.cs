﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace EasyCSharp.WPF.DirectShow
{
    [ComImport, SuppressUnmanagedCodeSecurity,
    Guid("A490B1E4-AB84-4D31-A1B2-181E03B1077A"),
    InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IMFVideoDisplayControl
    {
        [PreserveSig]
        int GetNativeVideoSize(/* not impl */);
        [PreserveSig]
        int GetIdealVideoSize(/* not impl */);
        [PreserveSig]
        int SetVideoPosition(/* not impl */);
        [PreserveSig]
        int GetVideoPosition(/* not impl */);
        [PreserveSig]
        int SetAspectRatioMode(/* not impl */);
        [PreserveSig]
        int GetAspectRatioMode(/* not impl */);
        [PreserveSig]
        int SetVideoWindow([In] IntPtr hwndVideo);
        [PreserveSig]
        int GetVideoWindow(out IntPtr phwndVideo);
        [PreserveSig]
        int RepaintVideo();
        [PreserveSig]
        int GetCurrentImage(/* not impl */);
        [PreserveSig]
        int SetBorderColor([In] int Clr);
        [PreserveSig]
        int GetBorderColor(out int pClr);
        [PreserveSig]
        int SetRenderingPrefs(/* not impl */);
        [PreserveSig]
        int GetRenderingPrefs(/* not impl */);
        [PreserveSig]
        int SetFullscreen(/* not impl */);
        [PreserveSig]
        int GetFullscreen(/* not impl */);
    }
}

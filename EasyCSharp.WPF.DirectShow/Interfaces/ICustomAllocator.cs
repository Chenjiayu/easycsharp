﻿using System;

namespace EasyCSharp.WPF.DirectShow
{
    /// <summary>
    /// The custom allocator interface.  All custom allocators need
    /// to implement this interface.
    /// </summary>
    public interface ICustomAllocator : IDisposable
    {
        /// <summary>
        /// Invokes when a new frame has been allocated
        /// to a surface
        /// </summary>
        event Action NewAllocatorFrame;

        /// <summary>
        /// Invokes when a new surface has been allocated
        /// </summary>
        event NewAllocatorSurfaceDelegate NewAllocatorSurface;
    }
}

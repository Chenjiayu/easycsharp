﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace EasyCSharp.WPF.DirectShow
{
    [ComVisible(true), ComImport, SuppressUnmanagedCodeSecurity,
     Guid("9019EA9C-F1B4-44b5-ADD5-D25704313E48"),
     InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    internal interface IEVRPresenterRegisterCallback
    {
        [PreserveSig]
        int RegisterCallback(IEVRPresenterCallback pCallback);
    }
}

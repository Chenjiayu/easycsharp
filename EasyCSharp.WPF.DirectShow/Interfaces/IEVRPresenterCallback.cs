﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace EasyCSharp.WPF.DirectShow
{
    [ComVisible(true), ComImport, SuppressUnmanagedCodeSecurity,
     Guid("B92D8991-6C42-4e51-B942-E61CB8696FCB"),
     InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    internal interface IEVRPresenterCallback
    {
        [PreserveSig]
        int PresentSurfaceCB(IntPtr pSurface);
    }
}

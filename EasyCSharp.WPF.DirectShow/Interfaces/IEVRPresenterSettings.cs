﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace EasyCSharp.WPF.DirectShow
{
    [ComVisible(true), ComImport, SuppressUnmanagedCodeSecurity,
     Guid("4527B2E7-49BE-4b61-A19D-429066D93A99"),
     InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    internal interface IEVRPresenterSettings
    {
        [PreserveSig]
        int SetBufferCount(int count);

        [PreserveSig]
        int GetBufferCount(out int count); // Added

        [PreserveSig]
        int RegisterCallback(IEVRPresenterCallback pCallback); // Added
    }
}

﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace EasyCSharp.WPF.DirectShow
{
    [ComImport, SuppressUnmanagedCodeSecurity,
    InterfaceType(ComInterfaceType.InterfaceIsIUnknown),
    Guid("29AFF080-182A-4A5D-AF3B-448F3A6346CB")]
    public interface IMFVideoPresenter : IMFClockStateSink
    {
        #region IMFClockStateSink
        [PreserveSig]
        new void OnClockStart([In] long hnsSystemTime, [In] long llClockStartOffset);
        [PreserveSig]
        new void OnClockStop([In] long hnsSystemTime);
        [PreserveSig]
        new void OnClockPause([In] long hnsSystemTime);
        [PreserveSig]
        new void OnClockRestart([In] long hnsSystemTime);
        [PreserveSig]
        new void OnClockSetRate([In] long hnsSystemTime, [In] float flRate);
        #endregion
        [PreserveSig]
        int ProcessMessage();
        [PreserveSig]
        int GetCurrentMediaType();
    }
}

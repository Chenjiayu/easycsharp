﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace EasyCSharp.WPF.DirectShow
{
    [ComImport, SuppressUnmanagedCodeSecurity,
    InterfaceType(ComInterfaceType.InterfaceIsIUnknown),
    Guid("F6696E82-74F7-4F3D-A178-8A5E09C3659F")]
    public interface IMFClockStateSink
    {
        [PreserveSig]
        int OnClockStart([In] long hnsSystemTime, [In] long llClockStartOffset);
        [PreserveSig]
        int OnClockStop([In] long hnsSystemTime);
        [PreserveSig]
        int OnClockPause([In] long hnsSystemTime);
        [PreserveSig]
        int OnClockRestart([In] long hnsSystemTime);
        [PreserveSig]
        int OnClockSetRate([In] long hnsSystemTime, [In] float flRate);
    }
}

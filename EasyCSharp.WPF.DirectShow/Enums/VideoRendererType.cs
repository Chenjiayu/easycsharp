﻿namespace EasyCSharp.WPF.DirectShow
{

    public abstract partial class MediaPlayerBase
    {
        /// <summary>
        /// Specifies different types of DirectShow
        /// Video Renderers
        /// </summary>
        public enum VideoRendererType
        {
            VideoMixingRenderer9 = 0,
            EnhancedVideoRenderer
        }
    }

}

﻿namespace EasyCSharp.WPF.DirectShow
{
    public enum MediaState
    {
        Manual,
        Play,
        Stop,
        Close,
        Pause
    }
}

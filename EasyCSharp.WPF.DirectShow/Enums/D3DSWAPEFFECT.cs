﻿using System;

namespace EasyCSharp.WPF.DirectShow
{
    [Flags]
    public enum D3DSWAPEFFECT
    {
        D3DSWAPEFFECT_DISCARD = 1,
        D3DSWAPEFFECT_FLIP = 2,
        D3DSWAPEFFECT_COPY = 3,
    }
}

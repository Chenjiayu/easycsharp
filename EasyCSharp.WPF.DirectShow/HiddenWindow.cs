﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace EasyCSharp.WPF.DirectShow
{
    /// <summary>
    /// A low level window class that is used to provide interop with libraries
    /// that require an hWnd 
    /// </summary>
    public class HiddenWindow : NativeWindow
    {
        public delegate IntPtr WndProcHookDelegate(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled);

        readonly List<WndProcHookDelegate> m_handlerlist = new List<WndProcHookDelegate>();

        public void AddHook(WndProcHookDelegate method)
        {
            if (m_handlerlist.Contains(method))
                return;

            lock (((System.Collections.ICollection)m_handlerlist).SyncRoot)
                m_handlerlist.Add(method);
        }

        public void RemoveHook(WndProcHookDelegate method)
        {
            lock (((System.Collections.ICollection)m_handlerlist).SyncRoot)
                m_handlerlist.Remove(method);
        }

        /// <summary>
        /// Invokes the windows procedure associated to this window
        /// </summary>
        /// <param name="m">The window message to send to window</param>
        protected override void WndProc(ref Message m)
        {
            bool isHandled = false;

            lock (((System.Collections.ICollection)m_handlerlist).SyncRoot)
            {
                foreach (WndProcHookDelegate method in m_handlerlist)
                {
                    method.Invoke(m.HWnd, m.Msg, m.WParam, m.LParam, ref isHandled);
                    if (isHandled)
                        break;
                }
            }

            base.WndProc(ref m);
        }
    }
}

﻿using System.Runtime.InteropServices;

namespace EasyCSharp.WPF.DirectShow
{
    [StructLayout(LayoutKind.Sequential)]
    public class RECT
    {
        public int left;
        public int top;
        public int right;
        public int bottom;
    }
}

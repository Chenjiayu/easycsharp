﻿using System.Runtime.InteropServices;

namespace EasyCSharp.WPF.DirectShow
{
    [StructLayout(LayoutKind.Sequential)]
    public struct D3DDISPLAYMODE
    {
        public uint Width;
        public uint Height;
        public uint RefreshRate;
        public D3DFORMAT Format;
    }
}

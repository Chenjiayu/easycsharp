﻿using System;
using System.Runtime.InteropServices;

namespace EasyCSharp.WPF.DirectShow
{
    [StructLayout(LayoutKind.Sequential)]
    public struct D3DDEVICE_CREATION_PARAMETERS
    {
        uint AdapterOrdinal;
        D3DDEVTYPE DeviceType;
        IntPtr hFocusWindow;
        int BehaviorFlags;
    }
}

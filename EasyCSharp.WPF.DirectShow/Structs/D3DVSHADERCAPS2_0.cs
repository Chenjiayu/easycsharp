﻿using System.Runtime.InteropServices;

namespace EasyCSharp.WPF.DirectShow
{
    [StructLayout(LayoutKind.Sequential)]
    public struct D3DVSHADERCAPS2_0
    {
        int Caps;
        int DynamicFlowControlDepth;
        int NumTemps;
        int StaticFlowControlDepth;
    }
}

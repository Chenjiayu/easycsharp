﻿using System.Runtime.InteropServices;

namespace EasyCSharp.WPF.DirectShow
{
    [StructLayout(LayoutKind.Sequential)]
    public struct D3DPSHADERCAPS2_0
    {
        int Caps;
        int DynamicFlowControlDepth;
        int NumTemps;
        int StaticFlowControlDepth;
        int NumInstructionSlots;
    }
}

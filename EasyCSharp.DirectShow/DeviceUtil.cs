﻿using DirectShowLib;
using System;
using System.Linq;

namespace EasyCSharp.DirectShow
{
    public static class DeviceUtil
    {
        private static DsDevice[] GetDevices(Guid filterCategory)
        {
            return (from d in DsDevice.GetDevicesOfCat(filterCategory)
                    select d).ToArray();
        }

        #region Video

        private static DsDevice[] m_videoInputDevices;
        private static string[] m_videoInputsDevicePaths;
        private static string[] m_videoInputNames;

        /// <summary>
        /// 视频输入设备路径集合
        /// </summary>
        public static string[] VideoInputsDevicePaths
        {
            get
            {
                if (m_videoInputsDevicePaths == null)
                {
                    m_videoInputsDevicePaths = (from d in VideoInputDevices
                                                select d.DevicePath).ToArray();
                }
                return m_videoInputsDevicePaths;
            }
        }


        /// <summary>
        /// 视频输入设备名称
        /// </summary>
        public static string[] VideoInputNames
        {
            get
            {
                if (m_videoInputNames == null)
                {
                    m_videoInputNames = (from d in VideoInputDevices
                                         select d.Name).ToArray();
                }
                return m_videoInputNames;
            }
        }

        /// <summary>
        /// 视频输入设备集合
        /// </summary>
        public static DsDevice[] VideoInputDevices
        {
            get
            {
                if (m_videoInputDevices == null)
                {
                    m_videoInputDevices = GetDevices(FilterCategory.VideoInputDevice);
                }
                return m_videoInputDevices;
            }
        }

        #endregion

        #region Audio
      
        private static string[] m_audioRendererNames;

        /// <summary>
        /// 音频渲染设备名称
        /// </summary>
        public static string[] AudioRendererNames
        {
            get
            {
                if (m_audioRendererNames == null)
                {
                    m_audioRendererNames = (from a in GetDevices(FilterCategory.AudioRendererCategory)
                                            select a.Name).ToArray();
                }
                return m_audioRendererNames;
            }
        }
        #endregion
    }
}

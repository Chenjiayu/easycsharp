﻿using System;
using System.Windows;
using System.Windows.Interop;

namespace SystemParametersDemo
{
    public static class D3DImageUtils
    {
        public static void AddDirtyRectAll(D3DImage d3d)
        {
            d3d.AddDirtyRect(new Int32Rect(0, 0, d3d.PixelWidth, d3d.PixelHeight));
        }

        /// <summary>
        /// Convenient method for making frame shots.
        /// For the exceptions, see <see cref="D3DImage.SetBackBuffer(D3DResourceType, IntPtr)". />
        /// </summary>
        public static void SetBackBufferWithLock(D3DImage d3d, IntPtr backBuffer)
        {
            try
            {
                d3d.Lock();
                d3d.SetBackBuffer(D3DResourceType.IDirect3DSurface9, backBuffer);
                // necessary when rendering on WPF screen only
                AddDirtyRectAll(d3d);
            }
            finally
            {
                d3d.Unlock();
            }
        }
    }
}

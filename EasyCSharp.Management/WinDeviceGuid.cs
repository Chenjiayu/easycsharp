﻿using System;

namespace EasyCSharp.Management
{
    public static class WinDeviceGuid
    {
        public static readonly Guid HostBusController = Guid.Parse("6bdd1fc1-810f-11d0-bec7-08002be2092f");

        public static readonly Guid BatteryDevices = Guid.Parse("72631e54-78a4-11d0-bcf7-00aa00b7b32a");

        public static readonly Guid CDROMDrives = Guid.Parse("4d36e965-e325-11ce-bfc1-08002be10318");

        public static readonly Guid DiskDrives = Guid.Parse("4d36e967-e325-11ce-bfc1-08002be10318");

        public static readonly Guid DisplayAdapters = Guid.Parse("4d36e968-e325-11ce-bfc1-08002be10318");

        public static readonly Guid HardDiskControllers = Guid.Parse("4d36e96a-e325-11ce-bfc1-08002be10318");

        public static readonly Guid HumanInputDevices = Guid.Parse("745a17a0-74d3-11d0-b6fe-00a0c90f57da");

        public static readonly Guid ImagingDevice = Guid.Parse("6bdd1fc6-810f-11d0-bec7-08002be2092f");

        public static readonly Guid Keyboard = Guid.Parse("4d36e96b-e325-11ce-bfc1-08002be10318");

        public static readonly Guid MediumChangers = Guid.Parse("ce5939ae-ebde-11d0-b181-0000f8753ec4");

        public static readonly Guid Multimedia = Guid.Parse("4d36e96c-e325-11ce-bfc1-08002be10318");

        public static readonly Guid Monitor = Guid.Parse("4d36e96e-e325-11ce-bfc1-08002be10318");

        public static readonly Guid Mouse = Guid.Parse("4d36e96f-e325-11ce-bfc1-08002be10318");

        public static readonly Guid MultifunctionDevices = Guid.Parse("4d36e971-e325-11ce-bfc1-08002be10318");

        public static readonly Guid MultiportSerialAdapters = Guid.Parse("50906cb8-ba12-11d1-bf5d-0000f805f530");

        public static readonly Guid Ports = Guid.Parse("4d36e978-e325-11ce-bfc1-08002be10318");

        public static readonly Guid Printer = Guid.Parse("4d36e979-e325-11ce-bfc1-08002be10318");

        public static readonly Guid SCSIAndRAIDControllers = Guid.Parse("4d36e97b-e325-11ce-bfc1-08002be10318");

        public static readonly Guid StorageVolumes = Guid.Parse("71a27cdd-812a-11d0-bec7-08002be2092f");

        public static readonly Guid SystemDevices = Guid.Parse("4d36e97d-e325-11ce-bfc1-08002be10318");

        public static readonly Guid TapeDrives = Guid.Parse("6d807884-7d21-11cf-801c-08002be10318");

        public static readonly Guid USB = Guid.Parse("36fc9e60-c465-11cf-8056-444553540000");

        public static readonly Guid USBMassStorageDevice = Guid.Parse("a5dcbf10-6530-11d2-901f-00c04fb951ed");

        public static readonly Guid NetworkAdapter = Guid.Parse("4d36e972-e325-11ce-bfc1-08002be10318");
    }
}

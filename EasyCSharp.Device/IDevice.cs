﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.Device
{
    public interface IDevice
    {
        string Id { get; }
        string Name { get; }
    }
}

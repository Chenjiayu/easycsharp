﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Animation;

namespace EasyCSharp.WPF.Controls
{
    public class AnimationHelper : DependencyObject
    {
        public static bool GetEnable(DependencyObject obj)
        {
            return (bool)obj.GetValue(EnableProperty);
        }

        public static void SetEnable(DependencyObject obj, bool value)
        {
            obj.SetValue(EnableProperty, value);
        }

        // Using a DependencyProperty as the backing store for Enable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnableProperty =
            DependencyProperty.RegisterAttached("Enable", typeof(bool), typeof(AnimationHelper), new System.Windows.PropertyMetadata(false));

        public static Storyboard GetMouseEnterStoryboard(DependencyObject obj)
        {
            return (Storyboard)obj.GetValue(MouseEnterStoryboardProperty);
        }

        public static void SetMouseEnterStoryboard(DependencyObject obj, BeginStoryboard value)
        {
            obj.SetValue(MouseEnterStoryboardProperty, value);
        }

        // Using a DependencyProperty as the backing store for MouseEnterBeginStoryboard.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MouseEnterStoryboardProperty =
            DependencyProperty.RegisterAttached("MouseEnterStoryboard", typeof(Storyboard), typeof(AnimationHelper), new System.Windows.PropertyMetadata(null));



        public static Storyboard GetMouseLeaveStoryboard(DependencyObject obj)
        {
            return (Storyboard)obj.GetValue(MouseLeaveStoryboardProperty);
        }

        public static void SetMouseLeaveStoryboard(DependencyObject obj, Storyboard value)
        {
            obj.SetValue(MouseLeaveStoryboardProperty, value);
        }

        // Using a DependencyProperty as the backing store for MouseLeaveStoryboard.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MouseLeaveStoryboardProperty =
            DependencyProperty.RegisterAttached("MouseLeaveStoryboard", typeof(Storyboard), typeof(AnimationHelper), new System.Windows.PropertyMetadata(null));



        public static Storyboard GetFocusedStoryboard(DependencyObject obj)
        {
            return (Storyboard)obj.GetValue(FocusedStoryboardProperty);
        }

        public static void SetFocusedStoryboard(DependencyObject obj, Storyboard value)
        {
            obj.SetValue(FocusedStoryboardProperty, value);
        }

        // Using a DependencyProperty as the backing store for FocusedStoryboard.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FocusedStoryboardProperty =
            DependencyProperty.RegisterAttached("FocusedStoryboard", typeof(Storyboard), typeof(AnimationHelper), new System.Windows.PropertyMetadata(null));



        public static Storyboard GetLostFocusedStoryboard(DependencyObject obj)
        {
            return (Storyboard)obj.GetValue(LostFocusedStoryboardProperty);
        }

        public static void SetLostFocusedStoryboard(DependencyObject obj, Storyboard value)
        {
            obj.SetValue(LostFocusedStoryboardProperty, value);
        }

        // Using a DependencyProperty as the backing store for LostFocusedStoryboard.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LostFocusedStoryboardProperty =
            DependencyProperty.RegisterAttached("LostFocusedStoryboard", typeof(Storyboard), typeof(AnimationHelper), new System.Windows.PropertyMetadata(null));


    }
}

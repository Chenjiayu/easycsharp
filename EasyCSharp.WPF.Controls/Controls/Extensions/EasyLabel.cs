﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace EasyCSharp.WPF.Controls
{
    public class EasyLabel : HeaderedContentControl
    {
        static EasyLabel()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EasyLabel), new FrameworkPropertyMetadata(typeof(EasyLabel)));
        }

        public HorizontalAlignment HeaderContentHorizontalAlignment
        {
            get { return (HorizontalAlignment)GetValue(HeaderContentHorizontalAlignmentProperty); }
            set { SetValue(HeaderContentHorizontalAlignmentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HeaderContentHorizontalAlignment.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HeaderContentHorizontalAlignmentProperty =
            DependencyProperty.Register("HeaderContentHorizontalAlignment", typeof(HorizontalAlignment), typeof(EasyLabel), new System.Windows.PropertyMetadata(HorizontalAlignment.Left));

        public VerticalAlignment HeaderContentVerticalAlignment
        {
            get { return (VerticalAlignment)GetValue(HeaderContentVerticalAlignmentProperty); }
            set { SetValue(HeaderContentVerticalAlignmentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HeaderContentVerticalAlignment.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HeaderContentVerticalAlignmentProperty =
            DependencyProperty.Register("HeaderContentVerticalAlignment", typeof(VerticalAlignment), typeof(EasyLabel), new System.Windows.PropertyMetadata(VerticalAlignment.Center));



        public GridLength ContentLength
        {
            get { return (GridLength)GetValue(ContentLengthProperty); }
            set { SetValue(ContentLengthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ContentLength.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContentLengthProperty =
            DependencyProperty.Register("ContentLength", typeof(GridLength), typeof(EasyLabel), new System.Windows.PropertyMetadata(GridLength.Auto));


    }
}

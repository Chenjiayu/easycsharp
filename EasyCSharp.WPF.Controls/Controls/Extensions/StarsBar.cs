﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace EasyCSharp.WPF.Controls
{
    public class StarsBar : Control
    {
        static StarsBar()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(StarsBar), new FrameworkPropertyMetadata(typeof(StarsBar)));
        }

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(StarsBar), new System.Windows.PropertyMetadata(.0, OnValueChangedCallBack));

        private static void OnValueChangedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as StarsBar).OnValueChanged(e.NewValue);
        }

        public virtual void OnValueChanged(object nV)
        {
            var newValue = Convert.ToDouble(nV);

            if (double.IsNaN(newValue) || newValue == 0)
            {
                Stars = 0;
            }
            else if (newValue > 0 && newValue <= 0.5)
            {
                Stars = 0.5;
            }
            else if (newValue > 0.5 && newValue <= 1)
            {
                Stars = 1;
            }
            else if (newValue > 1 && newValue <= 1.5)
            {
                Stars = 1.5;
            }
            else if (newValue > 1.5 && newValue <= 2)
            {
                Stars = 2;
            }
            else if (newValue > 2 && newValue <= 2.5)
            {
                Stars = 2.5;
            }
            else if (newValue > 2.5 && newValue <= 3)
            {
                Stars = 3;
            }
            else if (newValue > 3 && newValue <= 3.5)
            {
                Stars = 3.5;
            }
            else if (newValue > 3.5 && newValue <= 4)
            {
                Stars = 4;
            }
            else if (newValue > 4 && newValue <= 4.5)
            {
                Stars = 4.5;
            }
            else if (newValue > 4.5)
            {
                Stars = 5;
            }
        }

        public double Stars
        {
            get { return (double)GetValue(StarsProperty); }
            set { SetValue(StarsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Starts.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StarsProperty =
            DependencyProperty.Register("Stars", typeof(double), typeof(StarsBar), new System.Windows.PropertyMetadata(.0));



        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Orientation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(Orientation), typeof(StarsBar), new System.Windows.PropertyMetadata(Orientation.Horizontal));





        public Thickness InnerMargin
        {
            get { return (Thickness)GetValue(InnerMarginProperty); }
            set { SetValue(InnerMarginProperty, value); }
        }

        // Using a DependencyProperty as the backing store for InnerMargin.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty InnerMarginProperty =
            DependencyProperty.Register("InnerMargin", typeof(Thickness), typeof(StarsBar), new System.Windows.PropertyMetadata(new Thickness(2)));



    }
}

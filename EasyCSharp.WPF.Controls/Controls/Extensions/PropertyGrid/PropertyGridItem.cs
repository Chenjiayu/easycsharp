﻿using EasyCSharp.WPF.Core;
using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Controls;

namespace EasyCSharp.WPF.Controls
{
    public class PropertyGridItem : GridEntry
    {
        #region .ctor

        public PropertyGridItem(PropertyGrid propertyGrid, object component, PropertyDescriptor descriptor) : this(null)
        {
            Owner = propertyGrid ?? throw new ArgumentNullException("owner");
            Component = component ?? throw new ArgumentNullException("component");
            PropertyDescriptor = descriptor ?? throw new ArgumentNullException("descriptor");

            Name = descriptor.Name;
            IsBrowsable = descriptor.IsBrowsable;
            _isReadOnly = descriptor.IsReadOnly;
            _description = descriptor.Description;
            CategoryName = descriptor.Category;
            IsLocalizable = descriptor.IsLocalizable;

            Metadata = new AttributesContainer(descriptor.Attributes);
            PropertyDescriptor.AddValueChanged(component, ComponentValueChanged);
        }

        private void ComponentValueChanged(object sender, EventArgs e)
        {
            OnPropertyChanged("PropertyValue");
        }

        protected PropertyGridItem(PropertyGridItemValue parentValue)
        {
            ParentValue = parentValue;
        }

        #endregion

        #region Properties        

        public PropertyDescriptor PropertyDescriptor { get; }
        public PropertyGridItemValue ParentValue { get; }

        private PropertyGridItemValue _value;
        
        public PropertyGridItemValue PropertyValue
        {
            get
            {
                if (_value == null) _value = CreatePropertyValueInstance();
                return _value;
            }
        }     
      

        private string _displayName;
        /// <summary>
        /// Gets the display name for the property.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// The display name for the property.
        /// </returns>
        public string DisplayName
        {
            get
            {
                if (string.IsNullOrEmpty(_displayName))
                    _displayName = GetDisplayName();

                return _displayName;
            }
            set
            {
                if (_displayName == value) return;
                _displayName = value;
                OnPropertyChanged("DisplayName");
            }
        }
        public TypeConverter Converter
        {
            get { return KnownTypes.GetPropertyConverter(PropertyDescriptor); }
        }

        /// <summary>
        /// 属性特性集合容器
        /// </summary>
        public AttributesContainer Metadata { get; }

        /// <summary>
        /// 属性特性集合。在PropertyGridItem中，数据从属性解释器中获取
        /// </summary>
        public virtual AttributeCollection Attributes
        {
            get
            {
                if (PropertyDescriptor == null) return null;
                return PropertyDescriptor.Attributes;
            }
        }

        /// <summary>
        /// 未装箱属性对象
        /// </summary>
        public object Component { get; }

        /// <summary>
        /// 属性的组名
        /// </summary>
        public string CategoryName { get; }

        /// <summary>
        /// 是否本地化属性
        /// </summary>
        public bool IsLocalizable { get; }     

        private bool _isReadOnly;

        /// <summary>
        /// 是否为只读属性
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
            set
            {
                if (_isReadOnly == value) return;
                _isReadOnly = value;
                OnPropertyChanged("IsReadOnly");
            }
        }

        private string _description;

        /// <summary>
        /// 属性的描述文本
        /// </summary>
        public string Description
        {
            get { return _description; }
            set
            {
                if (_description == value) return;
                _description = value;
                OnPropertyChanged("Description");
            }
        }

        #endregion

        #region Methods

        private string GetDisplayName()
        {
            // TODO: decide what to be returned in the worst case (no descriptor)
            if (PropertyDescriptor == null) return "UnKnown";

            // Try getting Parenthesize attribute
            var attr = GetAttribute<ParenthesizePropertyNameAttribute>();

            // if property needs parenthesizing then apply parenthesis to resulting display name      
            return (attr != null && attr.NeedParenthesis)
              ? "(" + PropertyDescriptor.DisplayName + ")"
              : PropertyDescriptor.DisplayName;
        }

        public virtual T GetAttribute<T>() where T : Attribute
        {
            if (Attributes == null) return null;
            return Attributes[typeof(T)] as T;
        }

        protected PropertyGridItemValue CreatePropertyValueInstance()
        {
            return new PropertyGridItemValue(this);
        }
     
        public object GetValue()
        {
            if (PropertyDescriptor == null) return null;
            var target = GetViaCustomTypeDescriptor(Component, PropertyDescriptor);
            return PropertyDescriptor.GetValue(target);
        }

        #endregion

        private static object GetViaCustomTypeDescriptor(object obj, PropertyDescriptor descriptor)
        {
            return obj is ICustomTypeDescriptor customTypeDescriptor ? customTypeDescriptor.GetPropertyOwner(descriptor) : obj;
        }

    }

    public class PropertyGridItemValue : INotifyPropertyChanged
    {
        public PropertyGridItemValue(PropertyGridItem item)
        {
            ParentProperty = item ?? throw new ArgumentNullException("item");
            HasSubProperties = item.Converter.GetPropertiesSupported();

            if (HasSubProperties)
            {
                object value = item.GetValue();

                PropertyDescriptorCollection descriptors = item.Converter.GetProperties(value);
                foreach (PropertyDescriptor d in descriptors)
                {
                    SubProperties.Add(new PropertyGridItem(item.Owner, value, d));
                    // TODO: Move to PropertyData as a public property
                    NotifyParentPropertyAttribute notifyParent = d.Attributes[KnownTypes.Attributes.NotifyParentPropertyAttribute] as NotifyParentPropertyAttribute;
                    if (notifyParent != null && notifyParent.NotifyParent)
                    {
                        d.AddValueChanged(value, NotifySubPropertyChanged);
                    }
                }
            }

            this.ParentProperty.PropertyChanged += new PropertyChangedEventHandler(ParentPropertyChanged);
        }

        private void NotifySubPropertyChanged(object sender, EventArgs e)
        {
            
        }

        private void ParentPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
        }

        #region Properties

        public PropertyGridItem ParentProperty { get; }
        public bool HasSubProperties { get; }
        public GridEntryCollection<PropertyGridItem> SubProperties { get; } = new GridEntryCollection<PropertyGridItem>();


        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}

﻿using EasyCSharp.WPF.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace EasyCSharp.WPF.Controls
{
    internal static class KnownTypes
    {
        public static class Collections
        {
            public static readonly Type IList = typeof(IList);
        }

        public static class Attributes
        {
            public static readonly Type EditorBrowsableAttribute = typeof(EditorBrowsableAttribute);
            public static readonly Type MergablePropertyAttribute = typeof(MergablePropertyAttribute);
            public static readonly Type DefaultValueAttribute = typeof(DefaultValueAttribute);
            public static readonly Type DescriptionAttribute = typeof(DescriptionAttribute);
            //public static readonly Type StringFormatAttribute = 
            //public static readonly Type PropertyEditorAttribute = typeof(PropertyAttribute);
            public static readonly Type CategoryAttribute = typeof(CategoryAttribute);
            public static readonly Type NotifyParentPropertyAttribute = typeof(NotifyParentPropertyAttribute);
        }

        public static class WPF
        {
            public static readonly Type Geometry = typeof(Geometry);
            public static readonly Type CornerRadius = typeof(CornerRadius);
            public static readonly Type Point3D = typeof(Point3D);
            public static readonly Type Point4D = typeof(Point4D);
            public static readonly Type Point3DCollection = typeof(Point3DCollection);
            public static readonly Type Matrix3D = typeof(Matrix3D);
            public static readonly Type Quaternion = typeof(Quaternion);
            public static readonly Type Rect3D = typeof(Rect3D);
            public static readonly Type Size3D = typeof(Size3D);
            public static readonly Type Vector3D = typeof(Vector3D);
            public static readonly Type Vector3DCollection = typeof(Vector3DCollection);
            public static readonly Type PointCollection = typeof(PointCollection);
            public static readonly Type VectorCollection = typeof(VectorCollection);
            public static readonly Type Point = typeof(Point);
            public static readonly Type Rect = typeof(Rect);
            public static readonly Type Size = typeof(Size);
            public static readonly Type Thickness = typeof(Thickness);
            public static readonly Type Vector = typeof(Vector);
            public static readonly Type FontStretch = typeof(FontStretch);
            public static readonly Type FontStyle = typeof(FontStyle);
            public static readonly Type FontWeight = typeof(FontWeight);
            public static readonly Type FontFamily = typeof(FontFamily);
            public static readonly Type Cursor = typeof(Cursor);
            public static readonly Type Brush = typeof(Brush);
        }

        public static readonly List<Type> CultureInvariantTypes = new List<Type>
        {
            WPF.CornerRadius,
            WPF.Point3D,
            WPF.Point4D,
            WPF.Point3DCollection,
            WPF.Matrix3D,
            WPF.Quaternion,
            WPF.Rect3D,
            WPF.Size3D,
            WPF.Vector3D,
            WPF.Vector3DCollection,
            WPF.PointCollection,
            WPF.VectorCollection,
            WPF.Point,
            WPF.Rect,
            WPF.Size,
            WPF.Thickness,
            WPF.Vector
        };

        public static readonly string[] StringConverterMembers = { "Content", "Header", "ToolTip", "Tag" };

        #region DefaultStringConverter
        private static StringConverter _defaultStringConverter;
        public static StringConverter DefaultStringConverter
        {
            get
            {
                if (_defaultStringConverter == null)
                    _defaultStringConverter = new StringConverter();
                return _defaultStringConverter;
            }
        }
        #endregion

        #region DefaultFontStretchConverterDecorator
        private static FontStretchConverterDecorator _defaultFontStretchConverterDecorator;
        public static FontStretchConverterDecorator DefaultFontStretchConverterDecorator
        {
            get { return _defaultFontStretchConverterDecorator ?? (_defaultFontStretchConverterDecorator = new FontStretchConverterDecorator()); }
        }
        #endregion

        #region DefaultFontStyleConverterDecorator
        private static FontStyleConverterDecorator _DefaultFontStyleConverterDecorator;
        public static FontStyleConverterDecorator DefaultFontStyleConverterDecorator
        {
            get
            {
                if (_DefaultFontStyleConverterDecorator == null)
                    _DefaultFontStyleConverterDecorator = new FontStyleConverterDecorator();
                return _DefaultFontStyleConverterDecorator;
            }
        }
        #endregion

        #region DefaultFontWeightConverterDecorator
        private static FontWeightConverterDecorator _defaultFontWeightConverterDecorator;
        public static FontWeightConverterDecorator DefaultFontWeightConverterDecorator
        {
            get { return _defaultFontWeightConverterDecorator ?? (_defaultFontWeightConverterDecorator = new FontWeightConverterDecorator()); }
        }
        #endregion

        public static TypeConverter GetPropertyConverter(PropertyDescriptor propertyDescriptor)
        {
            if (propertyDescriptor == null)
                throw new ArgumentNullException("propertyDescriptor");

            if (StringConverterMembers.Contains(propertyDescriptor.Name)
              && propertyDescriptor.PropertyType.IsAssignableFrom(typeof(object)))
                return DefaultStringConverter;
            if (typeof(FontStretch).IsAssignableFrom(propertyDescriptor.PropertyType))
                return DefaultFontStretchConverterDecorator;
            if (typeof(FontStyle).IsAssignableFrom(propertyDescriptor.PropertyType))
                return DefaultFontStyleConverterDecorator;
            if (typeof(FontWeight).IsAssignableFrom(propertyDescriptor.PropertyType))
                return DefaultFontWeightConverterDecorator;
            return propertyDescriptor.Converter;
        }

        internal static object GetUnwrappedObject(object currentObject)
        {
            var customTypeDescriptor = currentObject as ICustomTypeDescriptor;
            return customTypeDescriptor != null ? customTypeDescriptor.GetPropertyOwner(null) : currentObject;
        }
    }
}

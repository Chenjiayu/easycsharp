﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace EasyCSharp.WPF.Controls
{
    public class PropertyGrid : Control
    {
        static PropertyGrid()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PropertyGrid), new FrameworkPropertyMetadata(typeof(PropertyGrid)));
        }

        public PropertyGrid()
        {
            Items = new ObservableCollection<PropertyGridItem>();
            Items.CollectionChanged += Items_CollectionChanged;
        }

        #region Events

        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {

        }

        #endregion

        #region Properties

        public object SelectedObject
        {
            get { return GetValue(SelectedObjectProperty); }
            set { SetValue(SelectedObjectProperty, value); }
        }

        public ObservableCollection<PropertyGridItem> Items
        {
            get { return (ObservableCollection<PropertyGridItem>)GetValue(ItemsProperty); }
            private set { SetValue(ItemsProperty, value); }
        }

        public Type ObjectType
        {
            get; private set;
        }

        #endregion

        #region DependencyProperties

        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(ObservableCollection<PropertyGridItem>), typeof(PropertyGrid), new System.Windows.PropertyMetadata(null));

        public static readonly DependencyProperty SelectedObjectProperty =
            DependencyProperty.Register("SelectedObject", typeof(object), typeof(PropertyGrid), new System.Windows.PropertyMetadata(null, OnSelectedObjectChanged));

        #endregion

        #region DependencyProperty CallBack

        private static void OnSelectedObjectChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != e.OldValue)
            {
                (d as PropertyGrid).Refresh();
            }
        }

        internal Editor GetEditor(GridEntry gridEntry)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Public Methods

        #endregion

        #region Private Methods

        private void Refresh()
        {
            if (SelectedObject == null)
            {
                IsEnabled = false;
                ClearObject();
            }
            else
            {
                IsEnabled = true;
                var newType = SelectedObject.GetType();
                if (ObjectType != newType)
                {
                    ClearObject();
                    ObjectType = newType;
                    GetObjectType();
                }
                GetObjectPropertyValue();
            }
        }

        private void ClearObject()
        {
            Items.Clear();
        }

        private void GetObjectType()
        {
            //反射获得所有编辑属性
            var propertyInfos = ObjectType.GetProperties();
            
        }

        private void GetObjectPropertyValue()
        {

        }



        #endregion
    }
}

﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;

namespace EasyCSharp.WPF.Controls
{
    /// <summary>
    /// 属性元数据
    /// </summary>
    public class PropertyGridItemMetadata : IEquatable<PropertyGridItemMetadata>
    {
        public PropertyDescriptor Descriptor { get; private set; }

        public PropertyGridItemMetadata(PropertyDescriptor descriptor)
        {
            Descriptor = descriptor;
        }

        #region IEquatable<PropertyData> Members

        public bool Equals(PropertyGridItemMetadata other)
        {
            return Descriptor.Equals(other.Descriptor);
        }

        #endregion

        public string Name
        {
            get { return Descriptor.Name; }
        }

        public string DisplayName
        {
            get { return Descriptor.DisplayName; }
        }

        public string Description
        {
            get { return Descriptor.Description; }
        }

        public string Category
        {
            get { return Descriptor.Category; }
        }

        public Type PropertyType
        {
            get { return Descriptor.PropertyType; }
        }

        public Type ComponentType
        {
            get { return Descriptor.ComponentType; }
        }

        public bool IsBrowsable
        {
            get { return Descriptor.IsBrowsable; }
        }

        public bool IsReadOnly
        {
            get { return Descriptor.IsReadOnly; }
        }

        // TODO: Cache value?
        public bool IsMergable
        {
            get { return MergablePropertyAttribute.Yes.Equals(Descriptor.Attributes[KnownTypes.Attributes.MergablePropertyAttribute]); }
        }

        // TODO: Cache value?
        public bool IsAdvanced
        {
            get
            {
                var attr = Descriptor.Attributes[KnownTypes.Attributes.EditorBrowsableAttribute] as EditorBrowsableAttribute;
                return attr != null && attr.State == EditorBrowsableState.Advanced;
            }
        }

        public bool IsLocalizable
        {
            get { return Descriptor.IsLocalizable; }
        }

        public bool IsCollection
        {
            get { return KnownTypes.Collections.IList.IsAssignableFrom(this.PropertyType); }
        }

        public DesignerSerializationVisibility SerializationVisibility
        {
            get { return Descriptor.SerializationVisibility; }
        }

        private CultureInfo _SerializationCulture;
        public CultureInfo SerializationCulture
        {
            get
            {
                if (_SerializationCulture == null)
                {
                    _SerializationCulture = (KnownTypes.CultureInvariantTypes.Contains(PropertyType) || KnownTypes.WPF.Geometry.IsAssignableFrom(PropertyType))
                      ? CultureInfo.InvariantCulture
                      : CultureInfo.CurrentCulture;
                }

                return _SerializationCulture;
            }
        }
    }
}

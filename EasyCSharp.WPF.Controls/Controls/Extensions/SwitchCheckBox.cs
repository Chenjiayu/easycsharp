﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace EasyCSharp.WPF.Controls
{
    public class SwitchCheckBox : CheckBox
    {
        static SwitchCheckBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SwitchCheckBox), new FrameworkPropertyMetadata(typeof(SwitchCheckBox)));
        }

        #region 属性


        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        #endregion

        #region 依赖属性

        public static readonly DependencyProperty CornerRadiusProperty =
                 DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(SwitchCheckBox), new System.Windows.PropertyMetadata(new CornerRadius(0)));
        #endregion

        public Brush EnableFill
        {
            get { return (Brush)GetValue(EnableFillProperty); }
            set { SetValue(EnableFillProperty, value); }
        }
        
        public static readonly DependencyProperty EnableFillProperty =
            DependencyProperty.Register("EnableFill", typeof(Brush), typeof(SwitchCheckBox), new System.Windows.PropertyMetadata(new SolidColorBrush(Color.FromRgb(0, 255, 0))));
    }
}

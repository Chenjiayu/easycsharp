﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace EasyCSharp.WPF.Controls
{
    public class FontAwesomeIcon:TextBlock
    {
        static FontAwesomeIcon()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(FontAwesomeIcon), new FrameworkPropertyMetadata(typeof(FontAwesomeIcon)));
        }
    }
}

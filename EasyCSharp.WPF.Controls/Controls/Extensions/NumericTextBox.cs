﻿using EasyCSharp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace EasyCSharp.WPF.Controls
{
    /// <summary>
    /// 只允许输入数字的输入框
    /// </summary>
    public class NumericTextBox : EasyTextBox
    {
        private bool HasSubtract
        {
            get
            {
                return Text.IsEmpty() ? false : Text.Contains("-");
            }
        }

        private bool HasDecimal
        {
            get
            {
                return Text.IsEmpty() ? false : Text.Contains(".");
            }
        }

        static NumericTextBox()
        {
            EnablePasteProperty.OverrideMetadata(typeof(NumericTextBox), new FrameworkPropertyMetadata(false));
            TextProperty.OverrideMetadata(typeof(NumericTextBox), new FrameworkPropertyMetadata("0"));
        }

        public NumericTextBox()
        {
            DisableInputMethod();
            this.PreviewKeyDown += NumericTextBox_PreviewKeyDown;
            this.PreviewTextInput += NumericTextBox_PreviewTextInput;
        }

        /// <summary>
        /// 在此屏蔽多余-、.号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NumericTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }

        #region Private Methods

        /// <summary>
        /// 禁用输入法
        /// </summary>
        private void DisableInputMethod()
        {
            SetValue(InputMethod.IsInputMethodEnabledProperty, false);
        }

        /// <summary>
        /// 校验上下限
        /// </summary>
        private void ValidataValueRegion()
        {
            //最大最小值都不为NaN时不限制上下限
            if (!double.IsNaN(MinValue) && !double.IsNaN(MaxValue))
            {
                if (MinValue > MaxValue)
                {
                    throw new ArgumentException("NumbericTextBox MinValue > MaxValue");
                }
            }
        }

        /// <summary>
        /// 值改变引起文本显示变化
        /// </summary>
        private void ValueToText()
        {
            Text = string.IsNullOrEmpty(StringFormat) ? Value.ToString() : Value.ToString(StringFormat);
        }

        /// <summary>
        /// 输入文本变化引起值变化
        /// </summary>
        private void TextToValue()
        {
            if (string.IsNullOrEmpty(Text) || string.IsNullOrWhiteSpace(Text))
            {
                Value = .0;
            }
            else
            {
                if (double.TryParse(Text, out double newValue))
                {
                    Value = newValue;
                }
                else
                {
                    //throw new ArgumentException($"Can't covert \"{Text}\" to double.");
                }
            }
        }

        /// <summary>
        /// 判断是否是方向键
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        private bool IsDirectionKey(int keyValue)
        {
            return keyValue >= 23 && keyValue <= 26;
        }

        /// <summary>
        /// 判断是否是删除键
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        private bool IsDeleteKey(int keyValue)
        {
            return keyValue == 32;
        }

        /// <summary>
        /// 判断是否是回退键
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        private bool IsBackSpaceKey(int keyValue)
        {
            return keyValue == 2;
        }

        /// <summary>
        /// 判断是否是句点号
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        private bool IsDecimalKey(int keyValue)
        {
            return keyValue == 88 || keyValue == 144;
        }

        /// <summary>
        /// 判断是否是减号
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        private bool IsSubtractKey(int keyValue)
        {
            return keyValue == 87 || keyValue == 143;
        }

        /// <summary>
        /// 判断是否是数字键
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        private bool IsNumbericKey(int keyValue)
        {
            return (keyValue >= 74 && keyValue <= 83) || (keyValue >= 34 && keyValue <= 43);
        }

        #endregion


        #region Properties

        public double MaxValue
        {
            get { return (double)GetValue(MaxValueProperty); }
            set { SetValue(MaxValueProperty, value); }
        }

        public double MinValue
        {
            get { return (double)GetValue(MinValueProperty); }
            set { SetValue(MinValueProperty, value); }
        }

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public string StringFormat
        {
            get { return (string)GetValue(StringFormatProperty); }
            set { SetValue(StringFormatProperty, value); }
        }

        #endregion

        #region DependencyProperties

        public static readonly DependencyProperty MaxValueProperty =
            DependencyProperty.Register("MaxValue", typeof(double), typeof(NumericTextBox), new PropertyMetadata(ValueBox.DefaultDouble, RegionValueChanged));

        public static readonly DependencyProperty MinValueProperty =
            DependencyProperty.Register("MinValue", typeof(double), typeof(NumericTextBox), new PropertyMetadata(ValueBox.DefaultDouble, RegionValueChanged));

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(NumericTextBox), new PropertyMetadata(ValueBox.DefaultDouble, ValueChanged));


        // Using a DependencyProperty as the backing store for StringFormat.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StringFormatProperty =
            DependencyProperty.Register("StringFormat", typeof(string), typeof(NumericTextBox), new PropertyMetadata(null));

        #endregion

        #region CallBacks

        private static void RegionValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as NumericTextBox).ValidataValueRegion();
        }

        private static void ValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as NumericTextBox).ValueToText();
        }

        #endregion

        #region Override

        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            TextToValue();
            base.OnTextChanged(e);
        }

        #endregion

        #region Events

        /// <summary>
        /// 在这里过滤按键
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NumericTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            var keyValue = (int)e.Key;

            if (!IsNumbericKey(keyValue) 
                && !IsDeleteKey(keyValue) 
                && !IsBackSpaceKey(keyValue) 
                && !IsDirectionKey(keyValue) 
                && !IsDecimalKey(keyValue) 
                && !IsSubtractKey(keyValue))
            {
                e.Handled = true;
                return;
            }

            if (IsDecimalKey(keyValue) && HasDecimal)
            {
                e.Handled = true;
                return;
            }

            if (IsSubtractKey(keyValue) && HasSubtract)
            {
                e.Handled = true;
                return;
            }
        }

        #endregion
    }
}

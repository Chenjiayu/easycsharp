﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace EasyCSharp.WPF.Controls
{
    public class TransitioningContentControl : ContentControl
    {
        DoubleAnimation animation;

        public TransitioningContentControl()
        {
            animation = new DoubleAnimation(0, 1, new Duration(new TimeSpan(0, 0, 0, 0, 200)));
            Storyboard.SetTarget(animation, this);
        }

        protected override void OnContentChanged(object oldContent, object newContent)
        {
            this.BeginAnimation(OpacityProperty, animation);
            base.OnContentChanged(oldContent, newContent);
        }
    }
}

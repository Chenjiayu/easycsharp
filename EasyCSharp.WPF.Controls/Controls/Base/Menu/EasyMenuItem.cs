﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace EasyCSharp.WPF.Controls
{
    public class EasyMenuItem : MenuItem
    {
        static EasyMenuItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EasyMenuItem), new FrameworkPropertyMetadata(typeof(EasyMenuItem)));
        }

        public PlacementMode SubMenuPlacementMode
        {
            get { return (PlacementMode)GetValue(SubMenuPlacementModeProperty); }
            set { SetValue(SubMenuPlacementModeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SubMenuPlacementMode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SubMenuPlacementModeProperty =
            DependencyProperty.Register("SubMenuPlacementMode", typeof(PlacementMode), typeof(EasyMenuItem), new System.Windows.PropertyMetadata(PlacementMode.Bottom));

        public PopupAnimation SubMenuPopupAnimation
        {
            get { return (PopupAnimation)GetValue(SubMenuPopupAnimationProperty); }
            set { SetValue(SubMenuPopupAnimationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SubMenuPopupAnimation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SubMenuPopupAnimationProperty =
            DependencyProperty.Register("SubMenuPopupAnimation", typeof(PopupAnimation), typeof(EasyMenuItem), new System.Windows.PropertyMetadata(PopupAnimation.Fade));



        public Thickness SubMenuPanelBorderThickness
        {
            get { return (Thickness)GetValue(SubMenuPanelBorderThicknessProperty); }
            set { SetValue(SubMenuPanelBorderThicknessProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SubMenuPanelBorderThickness.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SubMenuPanelBorderThicknessProperty =
            DependencyProperty.Register("SubMenuPanelBorderThickness", typeof(Thickness), typeof(EasyMenuItem), new System.Windows.PropertyMetadata(ValueBox.DefaultThickness));    



        public Brush SubMenuPanelBackground
        {
            get { return (Brush)GetValue(SubMenuPanelBackgroundProperty); }
            set { SetValue(SubMenuPanelBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SubMenuPanelBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SubMenuPanelBackgroundProperty =
            DependencyProperty.Register("SubMenuPanelBackground", typeof(Brush), typeof(EasyMenuItem), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));



        public Thickness SubMenuPanelPadding
        {
            get { return (Thickness)GetValue(SubMenuPanelPaddingProperty); }
            set { SetValue(SubMenuPanelPaddingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SubMenuPanelPadding.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SubMenuPanelPaddingProperty =
            DependencyProperty.Register("SubMenuPanelPadding", typeof(Thickness), typeof(EasyMenuItem), new System.Windows.PropertyMetadata(ValueBox.DefaultThickness));
    }
}

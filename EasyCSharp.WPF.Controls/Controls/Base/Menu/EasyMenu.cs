﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace EasyCSharp.WPF.Controls
{
    public class EasyMenu : Menu
    {
        static EasyMenu()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EasyMenu), new FrameworkPropertyMetadata(typeof(EasyMenu)));
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            var newItem = new EasyMenuItem();

            return newItem;
        }
      
    }
}

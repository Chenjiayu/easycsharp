﻿using EasyCSharp.WPF.Core;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace EasyCSharp.WPF.Controls
{
    public class EasyTreeViewItem : TreeViewItem
    {
        private CheckBox CheckBox;

        private EasyTreeViewItem parentItem;

        public EasyTreeViewItem ParentItem
        {
            get
            {
                if (parentItem == null)
                {
                    parentItem = VisualTreeHelperEx.FindParent<EasyTreeViewItem>(this);
                }

                return parentItem;
            }
            set { parentItem = value; }
        }

        private EasyTreeView treeView;
        public EasyTreeView TreeView
        {
            get
            {
                if (treeView == null)
                {
                    treeView = VisualTreeHelperEx.FindParent<EasyTreeView>(this);
                }
                return treeView;
            }
            set
            {
                treeView = value;
            }
        }

        public EasyTreeViewItem()
        {
            ItemContainerGenerator.StatusChanged += ItemContainerGenerator_StatusChanged;
        }

        private void ItemContainerGenerator_StatusChanged(object sender, EventArgs e)
        {
            CheckItems();

            if (ItemContainerGenerator.Status == GeneratorStatus.ContainersGenerated)
            {
                foreach (var itemData in Items)
                {
                    var item = ItemContainerGenerator.ContainerFromItem(itemData) as EasyTreeViewItem;
                    item.IsChecked = IsChecked;
                }
            }
        }

        static EasyTreeViewItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EasyTreeViewItem), new FrameworkPropertyMetadata(typeof(EasyTreeViewItem)));
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            var itemContainer = new EasyTreeViewItem();
            itemContainer.ParentItem = this;
            itemContainer.treeView = TreeView;
            return itemContainer;
        }

        public bool IsFirst
        {
            get { return (bool)GetValue(IsFirstProperty); }
            set { SetValue(IsFirstProperty, value); }
        }

        public static readonly DependencyProperty IsFirstProperty =
            DependencyProperty.Register("IsFirst", typeof(bool), typeof(EasyTreeViewItem), new PropertyMetadata(ValueBox.False));

        public bool IsLast
        {
            get { return (bool)GetValue(IsLastProperty); }
            set { SetValue(IsLastProperty, value); }
        }

        public static readonly DependencyProperty IsLastProperty =
            DependencyProperty.Register("IsLast", typeof(bool), typeof(EasyTreeViewItem), new PropertyMetadata(ValueBox.False));

        protected override void OnItemsChanged(NotifyCollectionChangedEventArgs e)
        {
            base.OnItemsChanged(e);
            CheckItems();
        }

        public bool? IsChecked
        {
            get { return (bool?)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsChecked.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsCheckedProperty =
            DependencyProperty.Register("IsChecked", typeof(bool?), typeof(EasyTreeViewItem), new PropertyMetadata(ValueBox.False, IsCheckedChangedCallBack));

        private static void IsCheckedChangedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var item = d as EasyTreeViewItem;
            if (e.NewValue != e.OldValue)
            {
                item.OnCheckedChanged();
            }
        }

        public event EventHandler<bool?> CheckedChangedEvent;

        public void OnCheckedChanged()
        {
            CheckedChangedEvent?.Invoke(this, IsChecked);
            if (IsChecked != null)
            {
                var isChecked = IsChecked.Value;
                if (isChecked)
                {
                    TreeView?.AddToSelectedItems(this);
                }
                else
                {
                    TreeView?.RemoveFromSelectedItems(this);
                }
            }            
        }

        /// <summary>
        /// 改变子项IsChecked状态
        /// </summary>
        /// <param name="isChecked"></param>
        private void SetSubItemsIsChecked(bool isChecked)
        {
            if (!HasItems) return;

            if (ItemContainerGenerator.Status != GeneratorStatus.ContainersGenerated) return;

            foreach (var itemData in Items)
            {
                var item = ItemContainerGenerator.ContainerFromItem(itemData) as EasyTreeViewItem;
                item.IsChecked = isChecked;
            }
        }

        /// <summary>
        /// 根据子项状态更新IsChecked
        /// </summary>
        private void CheckSubItemsIsChecked()
        {
            if (!HasItems) return;

            int checkedCount = 0;
            int unCheckedCount = 0;
            int nullCount = 0;
            foreach (var itemData in Items)
            {
                var item = ItemContainerGenerator.ContainerFromItem(itemData) as EasyTreeViewItem;
                if (item.IsChecked == null)
                {
                    nullCount++;
                }
                else if (item.IsChecked.Value)
                {
                    checkedCount++;
                }
                else if (!item.IsChecked.Value)
                {
                    unCheckedCount++;
                }
            }

            if (checkedCount == Items.Count)
            {
                IsChecked = true;
            }
            else if (unCheckedCount == Items.Count)
            {
                IsChecked = false;
            }
            else if (checkedCount > 0 && unCheckedCount > 0)
            {
                IsChecked = null;
            }
        }

        public IList SelectedItems
        {
            get { return (IList)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedItems.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedItemsProperty =
            DependencyProperty.Register("SelectedItems", typeof(IList), typeof(EasyTreeViewItem), new PropertyMetadata(null));

        /// <summary>
        /// 检查子项状态
        /// </summary>
        private void CheckItems()
        {
            if (ItemContainerGenerator.Status != GeneratorStatus.ContainersGenerated) return;

            if (Items.Count > 0)
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    var item = ItemContainerGenerator.ContainerFromIndex(i) as EasyTreeViewItem;
                    item.IsFirst = false;
                    item.IsLast = false;

                    if (i == 0)
                    {
                        item.IsFirst = true;
                    }

                    if (i == (Items.Count - 1))
                    {
                        item.IsLast = true;
                    }
                }
            }
        }

        public override void OnApplyTemplate()
        {
            CheckBox = GetTemplateChild("PART_CheckBox") as CheckBox;
            CheckBox.SetBinding(ToggleButton.IsCheckedProperty, new Binding("IsChecked") { Source = this });
            CheckBox.Click += CheckBox_Click;
            CheckBox.Checked += CheckBox_Checked;
            CheckBox.Unchecked += CheckBox_Unchecked;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {           
            ParentItem?.CheckSubItemsIsChecked();
            SetSubItemsIsChecked(false);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {                       
            ParentItem?.CheckSubItemsIsChecked();
            SetSubItemsIsChecked(true);
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (sender is CheckBox checkBox)
            {
                if (checkBox.IsChecked == null)
                {
                    checkBox.IsChecked = false;
                }
            }
        }
    }
}

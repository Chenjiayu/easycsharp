﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace EasyCSharp.WPF.Controls
{
    public class EasyTreeView : TreeView
    {
        static EasyTreeView()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EasyTreeView), new FrameworkPropertyMetadata(typeof(EasyTreeView)));
        }

        public EasyTreeView()
        {

        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            var itemContainer = new EasyTreeViewItem();
            itemContainer.TreeView = this;
            return itemContainer;
        }

        protected override void OnItemsChanged(NotifyCollectionChangedEventArgs e)
        {
            base.OnItemsChanged(e);
            CheckItems();
        }

        /// <summary>
        /// 更新子项状态
        /// </summary>
        private void CheckItems()
        {
            if (Items.Count > 0)
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    var item = ItemContainerGenerator.ContainerFromIndex(i) as EasyTreeViewItem;
                    item.IsFirst = false;
                    item.IsLast = false;

                    if (i == 0)
                    {
                        item.IsFirst = true;
                    }

                    if (i == (Items.Count - 1))
                    {
                        item.IsLast = true;
                    }
                }
            }
        }

        public Visibility LineVisibility
        {
            get { return (Visibility)GetValue(LineVisibilityProperty); }
            set { SetValue(LineVisibilityProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LineVisibility.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LineVisibilityProperty =
            DependencyProperty.Register("LineVisibility", typeof(Visibility), typeof(EasyTreeView), new PropertyMetadata(Visibility.Collapsed));


        public bool SelectMode
        {
            get { return (bool)GetValue(SelectModeProperty); }
            set { SetValue(SelectModeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectMode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectModeProperty =
            DependencyProperty.Register("SelectMode", typeof(bool), typeof(EasyTreeView), new PropertyMetadata(false));

        public IList SelectedItems
        {
            get { return (IList)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedItems.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedItemsProperty =
            DependencyProperty.Register("SelectedItems", typeof(IList), typeof(EasyTreeView), new PropertyMetadata(null));

        public void AddToSelectedItems(EasyTreeViewItem item)
        {
            if (item.TreeView == this)
            {
                if (this.SelectedItems == null)
                {
                    SelectedItems = new ObservableCollection<object>();
                }

                if (!SelectedItems.Contains(item.Header))
                {
                    SelectedItems.Add(item.Header);
                }
            }
        }

        public void RemoveFromSelectedItems(EasyTreeViewItem item)
        {
            if (item.TreeView == this)
            {
                if (this.SelectedItems == null)
                {
                    SelectedItems = new ObservableCollection<object>();
                }

                if (SelectedItems.Contains(item.Header))
                {
                    SelectedItems.Remove(item.Header);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace EasyCSharp.WPF.Controls
{
    public class EasyTextBox : TextBox
    {
        static EasyTextBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EasyTextBox), new FrameworkPropertyMetadata(typeof(EasyTextBox)));
        }

        public EasyTextBox()
        {
            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Copy, CopyExcuted, CanCopyExcute));
            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Paste, PasteExcuted, CanPasteExcute));
            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Cut, CutExcuted, CanCutExcute));
        }

        private void CutExcuted(object sender, ExecutedRoutedEventArgs e)
        {
            CutCommand?.Execute(e.Parameter);
        }

        private void CanCutExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (!EnableCut)
            {
                e.CanExecute = false;
                e.Handled = true;
            }
        }

        private void CanPasteExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (!EnablePaste)
            {
                e.CanExecute = false;
                e.Handled = true;
            }
        }

        private void PasteExcuted(object sender, ExecutedRoutedEventArgs e)
        {
            PasteCommand?.Execute(e.Parameter);
        }

        private void CopyExcuted(object sender, ExecutedRoutedEventArgs e)
        {
            CopyCommand?.Execute(e.Parameter);
        }

        private void CanCopyExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (!EnableCopy)
            {
                e.CanExecute = false;
                e.Handled = true;
            }
        }

        public string PlaceHolder
        {
            get { return (string)GetValue(PlaceHolderProperty); }
            set { SetValue(PlaceHolderProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PlaceHolder.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PlaceHolderProperty =
            DependencyProperty.Register("PlaceHolder", typeof(string), typeof(EasyTextBox), new System.Windows.PropertyMetadata(string.Empty));

        public bool EnableCopy
        {
            get { return (bool)GetValue(EnableCopyProperty); }
            set { SetValue(EnableCopyProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EnableCopy.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnableCopyProperty =
            DependencyProperty.Register("EnableCopy", typeof(bool), typeof(EasyTextBox), new PropertyMetadata(ValueBox.True));



        public bool EnableCut
        {
            get { return (bool)GetValue(EnableCutProperty); }
            set { SetValue(EnableCutProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EnableCut.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnableCutProperty =
            DependencyProperty.Register("EnableCut", typeof(bool), typeof(EasyTextBox), new PropertyMetadata(ValueBox.True));


        public bool EnablePaste
        {
            get { return (bool)GetValue(EnablePasteProperty); }
            set { SetValue(EnablePasteProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EnablePaste.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnablePasteProperty =
            DependencyProperty.Register("EnablePaste", typeof(bool), typeof(EasyTextBox), new PropertyMetadata(ValueBox.True));



        public ICommand CopyCommand
        {
            get { return (ICommand)GetValue(CopyCommandProperty); }
            set { SetValue(CopyCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CopyCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CopyCommandProperty =
            DependencyProperty.Register("CopyCommand", typeof(ICommand), typeof(EasyTextBox), new PropertyMetadata(null));



        public ICommand PasteCommand
        {
            get { return (ICommand)GetValue(PasteCommandProperty); }
            set { SetValue(PasteCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PasteCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PasteCommandProperty =
            DependencyProperty.Register("PasteCommand", typeof(ICommand), typeof(EasyTextBox), new PropertyMetadata(null));



        public ICommand CutCommand
        {
            get { return (ICommand)GetValue(CutCommandProperty); }
            set { SetValue(CutCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CutCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CutCommandProperty =
            DependencyProperty.Register("CutCommand", typeof(ICommand), typeof(EasyTextBox), new PropertyMetadata(null));


    }
}

﻿using System.Windows;
using System.Windows.Controls;

namespace EasyCSharp.WPF.Controls
{
    public class EasyExpander : Expander
    {
        static EasyExpander()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EasyExpander), new FrameworkPropertyMetadata(typeof(EasyExpander)));
        }
      
        public object ExpandedHeaderContent
        {
            get { return (object)GetValue(ExpandedHeaderContentProperty); }
            set { SetValue(ExpandedHeaderContentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ExpandedHeaderContent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ExpandedHeaderContentProperty =
            DependencyProperty.Register("ExpandedHeaderContent", typeof(object), typeof(EasyExpander), new PropertyMetadata(null));


    }
}

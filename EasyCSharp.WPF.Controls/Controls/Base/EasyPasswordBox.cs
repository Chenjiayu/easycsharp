﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace EasyCSharp.WPF.Controls
{
    public class EasyPasswordBox : Control
    {
        PasswordBox PasswordBox;
        static EasyPasswordBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EasyPasswordBox), new FrameworkPropertyMetadata(typeof(EasyPasswordBox)));
        }

        /// <summary>
        /// 边框圆角半径
        /// </summary>
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty =
               DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(EasyPasswordBox), new System.Windows.PropertyMetadata(new CornerRadius(0)));

        public string PlaceHolder
        {
            get { return (string)GetValue(PlaceHolderProperty); }
            set { SetValue(PlaceHolderProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PlaceHolder.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PlaceHolderProperty =
            DependencyProperty.Register("PlaceHolder", typeof(string), typeof(EasyPasswordBox), new System.Windows.PropertyMetadata(string.Empty));



        public Brush SelectionBrush
        {
            get { return (Brush)GetValue(SelectionBrushProperty); }
            set { SetValue(SelectionBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectionBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectionBrushProperty =
            DependencyProperty.Register("SelectionBrush", typeof(Brush), typeof(EasyPasswordBox), new System.Windows.PropertyMetadata(new SolidColorBrush()));

        public string Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Password.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password", typeof(string), typeof(EasyPasswordBox), new System.Windows.PropertyMetadata("", OnPasswordChanged));

        private static void OnPasswordChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as EasyPasswordBox).PasswordChangedCommand?.Execute(e.NewValue);
        }

        public bool ShowPassword
        {
            get { return (bool)GetValue(ShowPasswordProperty); }
            set { SetValue(ShowPasswordProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowPassword.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowPasswordProperty =
            DependencyProperty.Register("ShowPassword", typeof(bool), typeof(EasyPasswordBox), new System.Windows.PropertyMetadata(false));



        public int MaxLength
        {
            get { return (int)GetValue(MaxLengthProperty); }
            set { SetValue(MaxLengthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MaxLength.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaxLengthProperty =
            DependencyProperty.Register("MaxLength", typeof(int), typeof(EasyPasswordBox), new System.Windows.PropertyMetadata(int.MaxValue));



        public double SelectionOpacity
        {
            get { return (double)GetValue(SelectionOpacityProperty); }
            set { SetValue(SelectionOpacityProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectionOpacity.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectionOpacityProperty =
            DependencyProperty.Register("SelectionOpacity", typeof(double), typeof(EasyPasswordBox), new System.Windows.PropertyMetadata(1.0));

        public ICommand PasswordChangedCommand
        {
            get { return (ICommand)GetValue(PasswordChangedCommandProperty); }
            set { SetValue(PasswordChangedCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PasswordChangedCommandProperty =
            DependencyProperty.Register("PasswordChangedCommand", typeof(ICommand), typeof(EasyPasswordBox), new System.Windows.PropertyMetadata(null));

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PasswordBox = GetTemplateChild("PART_Password") as PasswordBox;
            PasswordBox.PasswordChanged += PasswordBox_PasswordChanged;
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Password = PasswordBox.Password;
        }
    }
}

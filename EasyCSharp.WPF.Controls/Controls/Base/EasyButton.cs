﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace EasyCSharp.WPF.Controls
{
    [TemplatePart(Name = ControlPartParameters.Border, Type = typeof(Border))]
    public class EasyButton : Button
    {
        static EasyButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EasyButton), new FrameworkPropertyMetadata(typeof(EasyButton)));
        }
    }
}

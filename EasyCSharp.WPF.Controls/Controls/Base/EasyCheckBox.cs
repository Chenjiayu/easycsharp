﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace EasyCSharp.WPF.Controls
{
    public class EasyCheckBox : CheckBox
    {
        static EasyCheckBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EasyCheckBox), new FrameworkPropertyMetadata(typeof(EasyCheckBox)));
        }

        public Brush MaskFill
        {
            get { return (Brush)GetValue(MaskFillProperty); }
            set { SetValue(MaskFillProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MaskFill.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaskFillProperty =
            DependencyProperty.Register("MaskFill", typeof(Brush), typeof(EasyCheckBox), new System.Windows.PropertyMetadata(new SolidColorBrush(Color.FromRgb(0, 0, 0))));



        public Brush HoverMaskFill
        {
            get { return (Brush)GetValue(HoverMaskFillProperty); }
            set { SetValue(HoverMaskFillProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HoverMaskFill.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HoverMaskFillProperty =
            DependencyProperty.Register("HoverMaskFill", typeof(Brush), typeof(EasyCheckBox), new System.Windows.PropertyMetadata(new SolidColorBrush()));



        public Brush PressedMaskFill
        {
            get { return (Brush)GetValue(PressedMaskFillProperty); }
            set { SetValue(PressedMaskFillProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PressedMaskFill.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PressedMaskFillProperty =
            DependencyProperty.Register("PressedMaskFill", typeof(Brush), typeof(EasyCheckBox), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));



        public Visibility MaskVisibility
        {
            get { return (Visibility)GetValue(MaskVisibilityProperty); }
            set { SetValue(MaskVisibilityProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MaskVisibility.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaskVisibilityProperty =
            DependencyProperty.Register("MaskVisibility", typeof(Visibility), typeof(EasyCheckBox), new PropertyMetadata(Visibility.Visible));


    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace EasyCSharp.WPF.Controls
{
    public class EasyListBox : ListBox
    {
        static EasyListBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EasyListBox), new FrameworkPropertyMetadata(typeof(EasyListBox)));
        }

        public EasyListBox()
        {
            this.SelectionChanged += EasyListBox_SelectionChanged;
           
            this.CollectionViewSource = new CollectionViewSource();
        }      

        protected override DependencyObject GetContainerForItemOverride()
        {
            var newItem = new EasyListBoxItem();          
            return newItem;
        }
       

        public ICommand SelectionChangedCommand
        {
            get { return (ICommand)GetValue(SelectionChangedCommandProperty); }
            set { SetValue(SelectionChangedCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectionChangedCommandProperty =
            DependencyProperty.Register("SelectionChangedCommand", typeof(ICommand), typeof(EasyListBox), new System.Windows.PropertyMetadata(null));

        public bool AutoScrollToNew
        {
            get { return (bool)GetValue(AutoScrollToNewProperty); }
            set { SetValue(AutoScrollToNewProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AutoScrollToNew.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AutoScrollToNewProperty =
            DependencyProperty.Register("AutoScrollToNew", typeof(bool), typeof(EasyListBox), new System.Windows.PropertyMetadata(false));      

        public string GroupPropertyName
        {
            get { return (string)GetValue(GroupPropertyNameProperty); }
            set { SetValue(GroupPropertyNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for GroupPropertyName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GroupPropertyNameProperty =
            DependencyProperty.Register("GroupPropertyName", typeof(string), typeof(EasyListBox), new System.Windows.PropertyMetadata(string.Empty, OnGroupPropertyNameChanged));



        public CollectionViewSource CollectionViewSource
        {
            get { return (CollectionViewSource)GetValue(CollectionViewSourceProperty); }
            set { SetValue(CollectionViewSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CollectionViewSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CollectionViewSourceProperty =
            DependencyProperty.Register("CollectionViewSource", typeof(CollectionViewSource), typeof(EasyListBox), new System.Windows.PropertyMetadata(null, OnCollectionViewSourceChanged));

        private static void OnCollectionViewSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue != e.NewValue)
            {
                var lb = (d as EasyListBox);

                if (e.OldValue != null) (e.OldValue as CollectionViewSource).Filter -= EasyListBox_Filter;
                if (e.NewValue != null) (e.NewValue as CollectionViewSource).Filter += EasyListBox_Filter;

                lb.ReFilter();
            }

        }

        private static void OnGroupPropertyNameChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as EasyListBox).ReFilter();
        }

        private static void EasyListBox_Filter(object sender, FilterEventArgs e)
        {

        }

        private void EasyListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AutoScrollToNew)
            {
                ScrollIntoView(this.SelectedItem);
            }

            if (SelectionChangedCommand?.CanExecute(SelectedItem) ?? false)
            {
                SelectionChangedCommand.Execute(SelectedItem);
            }
        }

        protected override void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            base.OnItemsSourceChanged(oldValue, newValue);
            ReFilter();
        }

        public void ReFilter()
        {
            if (!string.IsNullOrEmpty(GroupPropertyName) && ItemsSource != null)
            {
                var cv = CollectionViewSource.GetDefaultView(ItemsSource);
                cv.GroupDescriptions.Clear();
                cv.GroupDescriptions.Add(new PropertyGroupDescription(GroupPropertyName));
            }
        }

        
    }
}

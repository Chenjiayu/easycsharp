﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace EasyCSharp.WPF.Controls
{
    public class EasyListBoxItem : ListBoxItem
    {
        static EasyListBoxItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EasyListBoxItem), new FrameworkPropertyMetadata(typeof(EasyListBoxItem)));
        }

        public EasyListBoxItem()
        {
            this.PreviewMouseLeftButtonDown += EasyListBoxItem_PreviewMouseLeftButtonDown;
            this.PreviewDragOver += EasyListBoxItem_PreviewDragOver;
            this.PreviewDragEnter += EasyListBoxItem_PreviewDragEnter;
            this.PreviewDragLeave += EasyListBoxItem_PreviewDragLeave;
            this.PreviewDrop += EasyListBoxItem_PreviewDrop;
        }

        private void EasyListBoxItem_PreviewDrop(object sender, DragEventArgs e)
        {
            EasyDragDrop.SetIsDragOver(this, false);
        }

        private void EasyListBoxItem_PreviewDragLeave(object sender, DragEventArgs e)
        {
            EasyDragDrop.SetIsDragOver(this, false);
        }

        private void EasyListBoxItem_PreviewDragEnter(object sender, DragEventArgs e)
        {
            EasyDragDrop.SetIsDragOver(this, true);
        }

        private void EasyListBoxItem_PreviewDragOver(object sender, DragEventArgs e)
        {
           
        }

        private void EasyListBoxItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //RoutedEventArgs args = new RoutedEventArgs(EasyItemsControl.ItemsMouseLeftButtonDownEvent, sender);      
            RoutedEventArgs args = new MouseButtonEventArgs(e.MouseDevice, e.Timestamp, e.ChangedButton) { RoutedEvent = EasyItemsControl.ItemsMouseLeftButtonDownEvent, Source = sender };

            this.RaiseEvent(args);
        }
    }
}

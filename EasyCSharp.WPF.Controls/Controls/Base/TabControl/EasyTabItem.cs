﻿using System.Windows;
using System.Windows.Controls;

namespace EasyCSharp.WPF.Controls
{
    public class EasyTabItem : TabItem
    {
        static EasyTabItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EasyTabItem), new FrameworkPropertyMetadata(typeof(EasyTabItem)));
        }
    }
}

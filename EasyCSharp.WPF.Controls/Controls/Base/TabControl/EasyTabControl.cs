﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace EasyCSharp.WPF.Controls
{
    public class EasyTabControl : TabControl
    {
        static EasyTabControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EasyTabControl), new FrameworkPropertyMetadata(typeof(EasyTabControl)));
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            var newItem = new EasyTabItem();

            return newItem;
        }



        public Brush ContentBackground
        {
            get { return (Brush)GetValue(ContentBackgroundProperty); }
            set { SetValue(ContentBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ContentBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContentBackgroundProperty =
            DependencyProperty.Register("ContentBackground", typeof(Brush), typeof(EasyTabControl), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace EasyCSharp.WPF.Controls
{
    public class EasyToggleButton : ToggleButton
    {
        static EasyToggleButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EasyToggleButton), new FrameworkPropertyMetadata(typeof(EasyToggleButton)));
        }

        public EasyToggleButton()
        {

        }

        #region 属性

        public object CheckedContent
        {
            get { return (object)GetValue(CheckedContentProperty); }
            set { SetValue(CheckedContentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CheckedContent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CheckedContentProperty =
            DependencyProperty.Register("CheckedContent", typeof(object), typeof(EasyToggleButton), new System.Windows.PropertyMetadata(null));




        #endregion

        #region 依赖属性



        #endregion

        #region 重载



        #endregion
    }
}

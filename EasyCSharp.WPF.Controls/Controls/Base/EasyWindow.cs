﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace EasyCSharp.WPF.Controls
{
    public class EasyWindow : Window
    {
        public double ScreenWidth { get; private set; }
        public double ScreenHeight { get; private set; }
        public Grid MainGrid { get; private set; }
        public AdornerDecorator AdornerDecorator { get; private set; }
        public AdornerLayer AdornerLayer { get; private set; }
        public ContentAdorner ContentAdorner { get; private set; }

        static EasyWindow()
        {
            WindowStyleProperty.OverrideMetadata(typeof(EasyWindow), new FrameworkPropertyMetadata(WindowStyle.SingleBorderWindow));
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EasyWindow), new FrameworkPropertyMetadata(typeof(EasyWindow)));
        }

        public EasyWindow()
        {
            this.CommandBindings.Add(new CommandBinding(SystemCommands.CloseWindowCommand, OnCloseWindow));
            this.CommandBindings.Add(new CommandBinding(SystemCommands.MaximizeWindowCommand, OnMaximizeWindow, OnCanResizeWindow));
            this.CommandBindings.Add(new CommandBinding(SystemCommands.MinimizeWindowCommand, OnMinimizeWindow, OnCanMinimizeWindow));
            this.CommandBindings.Add(new CommandBinding(SystemCommands.RestoreWindowCommand, OnRestoreWindow, OnCanResizeWindow));

            this.ScreenWidth = SystemParameters.PrimaryScreenWidth;
            this.ScreenHeight = SystemParameters.PrimaryScreenHeight;

        }

        #region 命令事件

        private void OnCanResizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            if (e.Parameter == null)
            {
                e.CanExecute = this.ResizeMode == ResizeMode.CanResize || this.ResizeMode == ResizeMode.CanResizeWithGrip;
            }
            else
            {
                e.CanExecute = true;
            }

        }

        private void OnCanMinimizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = this.ResizeMode != ResizeMode.NoResize;
        }

        private void OnRestoreWindow(object sender, ExecutedRoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
        }

        private void OnMinimizeWindow(object sender, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }

        private void OnMaximizeWindow(object sender, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
        }

        private void OnCloseWindow(object sender, ExecutedRoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }

        #endregion

        /// <summary>
        /// 窗口标题栏高度
        /// </summary>
        public double CaptionHeight
        {
            get { return (double)GetValue(CaptionHeightProperty); }
            set { SetValue(CaptionHeightProperty, value); }
        }

        /// <summary>
        /// 标题栏内容
        /// </summary>
        public object Caption
        {
            get { return GetValue(CaptionProperty); }
            set { SetValue(CaptionProperty, value); }
        }

        /// <summary>
        /// 标题栏背景色
        /// </summary>
        public Brush CaptionBackground
        {
            get { return (Brush)GetValue(CaptionBackgroundProperty); }
            set { SetValue(CaptionBackgroundProperty, value); }
        }

        public Visibility CaptionVisibility
        {
            get { return (Visibility)GetValue(CaptionVisibilityProperty); }
            set { SetValue(CaptionVisibilityProperty, value); }
        }

        public static readonly DependencyProperty CaptionHeightProperty =
                   DependencyProperty.Register("CaptionHeight", typeof(double), typeof(EasyWindow), new System.Windows.PropertyMetadata(28.0));         

        public static readonly DependencyProperty CaptionProperty =
          DependencyProperty.Register("Caption", typeof(object), typeof(EasyWindow), new System.Windows.PropertyMetadata(null));

        public static readonly DependencyProperty CaptionBackgroundProperty =
           DependencyProperty.Register("CaptionBackground", typeof(Brush), typeof(EasyWindow), new System.Windows.PropertyMetadata(new SolidColorBrush()));

        public static readonly DependencyProperty CaptionVisibilityProperty =
            DependencyProperty.Register("CaptionVisibility", typeof(Visibility), typeof(EasyWindow), new System.Windows.PropertyMetadata(Visibility.Visible));

        public bool HideCaptionWhenMaximize
        {
            get { return (bool)GetValue(HideCaptionWhenMaximizeProperty); }
            set { SetValue(HideCaptionWhenMaximizeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HideCaptionWhenMaximize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HideCaptionWhenMaximizeProperty =
            DependencyProperty.Register("HideCaptionWhenMaximize", typeof(bool), typeof(EasyWindow), new System.Windows.PropertyMetadata(false));


        #region 路由事件

        public static readonly RoutedEvent BlinkRoutedEvent = EventManager.RegisterRoutedEvent(
            "Blink",
            RoutingStrategy.Direct,
            typeof(EventHandler<RoutedEventArgs>),
            typeof(Button));

        #endregion

        #region 方法重载

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            MainGrid = GetTemplateChild("PART_Grid") as Grid;
            var grid = GetTemplateChild("PART_ContentGrid") as Grid;

            AdornerDecorator = GetTemplateChild("PART_WindowAdornerDecorator") as AdornerDecorator;
            AdornerLayer = AdornerDecorator.AdornerLayer;
            ContentAdorner = new ContentAdorner(grid);
            AdornerLayer.Add(ContentAdorner);

            var content = ContentAdorner.GetAdornerContent(this);
            if (content != null)
            {
                ContentAdorner.Content = content;
            }
            ContentAdorner.Visibility = ContentAdorner.GetAdornerShow(this) ? Visibility.Visible : Visibility.Collapsed;
            ContentAdorner.SetContentAdorner(this, ContentAdorner);
        }

        #endregion

        #region MyRegion

       

        #endregion
    }
}

﻿using EasyCSharp.WPF.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interactivity;
using System.Windows.Interop;
using System.Windows.Shell;

namespace EasyCSharp.WPF.Controls
{
    /// <summary>
    /// 无边框窗口行为
    /// </summary>
    public class BorderlessWindowBehavior : Behavior<EasyWindow>
    {
        protected override void OnAttached()
        {
            base.OnAttached();

            var window = AssociatedObject;
            window.SourceInitialized += delegate
            {
                var handle = new WindowInteropHelper(window).Handle;
                var source = HwndSource.FromHwnd(handle);
                if (source != null)
                {
                    source.AddHook(WinProc);
                }

                // 去除窗口系统菜单，包括右键菜单和右上角按钮
                int style = Win32.GetWindowLong(handle, Win32.GWL_STYLE);
                style ^= Win32.WS_SYSMENU;
                Win32.SetWindowLong(handle, Win32.GWL_STYLE, style);
            };

            var resizeBorderThickness = CreateResizeBorder(window);
            var chrome = new WindowChrome
            {
                CaptionHeight = window.CaptionHeight,
                CornerRadius = EasyPanel.GetCornerRadius(window),
                ResizeBorderThickness = resizeBorderThickness,
                GlassFrameThickness = new Thickness(1)
            };
            window.SetValue(WindowChrome.WindowChromeProperty, chrome);

            var dpd = DependencyPropertyDescriptor.FromProperty(Window.ResizeModeProperty, typeof(Window));
            dpd.AddValueChanged(window, delegate
            {
                var windowChrome = (WindowChrome)window.GetValue(WindowChrome.WindowChromeProperty);
                windowChrome.ResizeBorderThickness = CreateResizeBorder(window);
            });

            var dpd2 = DependencyPropertyDescriptor.FromProperty(EasyWindow.CaptionHeightProperty, typeof(EasyWindow));
            dpd2.AddValueChanged(window, delegate
            {
                var windowChrome = (WindowChrome)window.GetValue(WindowChrome.WindowChromeProperty);
                windowChrome.CaptionHeight = window.CaptionHeight;
            });
        }

        /// <summary>
        /// 创建窗口边框
        /// </summary>
        /// <param name="window"></param>
        /// <returns></returns>
        private Thickness CreateResizeBorder(Window window)
        {
            var resizeBorderThickness = new Thickness(0);
            if (window.ResizeMode == ResizeMode.CanResize)
            {
                resizeBorderThickness = new Thickness
                {
                    Left = window.BorderThickness.Left == 0 ? 5 : window.BorderThickness.Left,
                    Top = window.BorderThickness.Top == 0 ? 5 : window.BorderThickness.Top,
                    Right = window.BorderThickness.Right == 0 ? 5 : window.BorderThickness.Right,
                    Bottom = window.BorderThickness.Bottom == 0 ? 5 : window.BorderThickness.Bottom
                };
            }

            return resizeBorderThickness;
        }

        /// <summary>
        /// Windows消息处理
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="msg"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <param name="handled"></param>
        /// <returns></returns>
        private IntPtr WinProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case Win32.WM_GETMINMAXINFO:
                    WmGetMinMaxInfo(hwnd, lParam);
                    handled = true;
                    break;
                case Win32.WM_SETCURSOR:
                    WmSetCursor(lParam, ref handled);
                    break;
            }

            return IntPtr.Zero;
        }

        /// <summary>
        /// 窗口全屏处理
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="lParam"></param>
        private void WmGetMinMaxInfo(IntPtr hwnd, IntPtr lParam)
        {
            var mmi = (MINMAXINFO)Marshal.PtrToStructure(lParam, typeof(MINMAXINFO));
            IntPtr pMonitor = Win32.MonitorFromWindow(hwnd, Win32.MONITOR_DEFAULTTONEAREST);
            if (pMonitor != IntPtr.Zero)
            {
                var mi = new MONITORINFO();
                Win32.GetMonitorInfo(pMonitor, mi);
                RECT rcWorkArea = mi.Work;
                //RECT rcMonitorArea = mi.Monitor;
                //mmi.MaxPosition.X = Math.Abs(rcWorkArea.Left - rcMonitorArea.Left);
                //mmi.MaxPosition.Y = Math.Abs(rcWorkArea.Top - rcMonitorArea.Top);
                mmi.MaxPosition.X = 0;
                mmi.MaxPosition.Y = 0;
                mmi.MaxSize.X = Math.Abs(rcWorkArea.Right - rcWorkArea.Left);
                mmi.MaxSize.Y = Math.Abs(rcWorkArea.Bottom - rcWorkArea.Top);

                var window = AssociatedObject as EasyWindow;
                if (window.MinWidth != 0)
                {
                    mmi.MinTrackSize.X = (int)window.MinWidth;
                }
                if (window.MinHeight != 0)
                {
                    mmi.MinTrackSize.Y = (int)window.MinHeight;
                }
            }

            Marshal.StructureToPtr(mmi, lParam, true);
        }

        /// <summary>
        /// 窗口被模态窗口阻塞时，触发模态窗口Blink效果
        /// </summary>
        /// <param name="lParam"></param>
        /// <param name="handled"></param>
        private void WmSetCursor(IntPtr lParam, ref bool handled)
        {
            if (lParam.ToInt32() == 0x202fffe || lParam.ToInt32() == 0x201fffe)
            {
                var window = AssociatedObject as EasyWindow;
                if (!window.IsActive && window.OwnedWindows.Count > 0)
                {
                    foreach (Window childWindow in window.OwnedWindows)
                    {
                        var child = childWindow as EasyWindow;
                        if (child?.IsActive == true)
                        {
                            var args = new RoutedEventArgs(EasyWindow.BlinkRoutedEvent, child);
                            child.RaiseEvent(args);

                            handled = true;
                            return;
                        }
                    }
                }
            }

            handled = false;
        }
    }
}

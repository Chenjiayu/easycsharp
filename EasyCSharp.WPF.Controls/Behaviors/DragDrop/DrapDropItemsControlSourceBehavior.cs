﻿using EasyCSharp.WPF.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace EasyCSharp.WPF.Controls
{
    public class DrapDropItemsControlSourceBehavior : Behavior<ItemsControl>
    {
        private UIElement topParent;

        public UIElement TopParent
        {
            get
            {
                if (topParent == null) { topParent = VisualTreeHelperEx.FindParent<DragDropAdornerHost>(this.AssociatedObject)?.Child; }
                return topParent;
            }
            set => topParent = value;
        }

        public bool IsDragging
        {
            get { return (bool)GetValue(IsDraggingProperty); }
            set { SetValue(IsDraggingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsDragging.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsDraggingProperty =
            DependencyProperty.Register("IsDragging", typeof(bool), typeof(DrapDropItemsControlSourceBehavior), new System.Windows.PropertyMetadata(false));

        public ContentControl Item
        {
            get { return (ContentControl)GetValue(ItemProperty); }
            set { SetValue(ItemProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Item.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemProperty =
            DependencyProperty.Register("Item", typeof(ContentControl), typeof(DrapDropItemsControlSourceBehavior), new System.Windows.PropertyMetadata(ItemChanged));

        private static void ItemChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = d as DrapDropItemsControlSourceBehavior;
            if (e.NewValue != null)
            {
                behavior.Dragging();
            }
            else
            {
                behavior.StopDragging();
            }
        }

        public void Dragging()
        {            
            TopParent?.SetValue(EasyDragDrop.DragDropElementProperty, Item);
            TopParent?.SetValue(EasyDragDrop.IsDraggingProperty, true);
        }

        public void StopDragging()
        {
            TopParent?.SetValue(EasyDragDrop.DragDropElementProperty, null);
            TopParent?.SetValue(EasyDragDrop.IsDraggingProperty, false);
        }

        public DragDropEffects DragDropEffects
        {
            get { return (DragDropEffects)GetValue(DragDropEffectsProperty); }
            set { SetValue(DragDropEffectsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DragDropEffects.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DragDropEffectsProperty =
            DependencyProperty.Register("DragDropEffects", typeof(DragDropEffects), typeof(DrapDropItemsControlSourceBehavior), new System.Windows.PropertyMetadata(DragDropEffects.Copy));

        protected override void OnAttached()
        {
            base.OnAttached();
            EasyEvent.AddHandler<ItemsControl>(this.AssociatedObject, ItemsMouseLeftButtonDown, EasyItemsControl.ItemsMouseLeftButtonDownEvent);

            this.AssociatedObject.PreviewQueryContinueDrag += AssociatedObject_PreviewQueryContinueDrag;
        }

        private void AssociatedObject_PreviewQueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {




        }

        private void ItemsMouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            var args = e as MouseButtonEventArgs;
            if (args.LeftButton == System.Windows.Input.MouseButtonState.Pressed)
            {
                //TODO Source应该是谁
                var item = e.OriginalSource as ContentControl;
                Item = item;

                IsDragging = true;

                DragDrop.DoDragDrop(item, item.Content, DragDropEffects);

                IsDragging = false;

                Item = null;
            }
        }

        protected override void OnDetaching()
        {
            EasyEvent.RemoveHandler<ItemsControl>(this.AssociatedObject, ItemsMouseLeftButtonDown, EasyItemsControl.ItemsMouseLeftButtonDownEvent);
            this.AssociatedObject.PreviewQueryContinueDrag -= AssociatedObject_PreviewQueryContinueDrag;
            base.OnDetaching();
        }
    }
}

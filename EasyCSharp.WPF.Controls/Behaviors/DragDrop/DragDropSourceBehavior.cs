﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interactivity;

namespace EasyCSharp.WPF.Controls
{
    public class DragDropSourceBehavior : Behavior<UIElement>
    {
        public DependencyObject Source
        {
            get { return (DependencyObject)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Source.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(DependencyObject), typeof(DragDropSourceBehavior), new System.Windows.PropertyMetadata(null));

        public object Data
        {
            get { return (object)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Data.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(object), typeof(DragDropSourceBehavior), new System.Windows.PropertyMetadata(null));

        public DragDropEffects DragDropEffects
        {
            get { return (DragDropEffects)GetValue(DragDropEffectsProperty); }
            set { SetValue(DragDropEffectsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DragDropEffects.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DragDropEffectsProperty =
            DependencyProperty.Register("DragDropEffects", typeof(DragDropEffects), typeof(DragDropSourceBehavior), new System.Windows.PropertyMetadata(DragDropEffects.Copy));



        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.PreviewMouseLeftButtonDown += AssociatedObject_PreviewMouseLeftButtonDown;
        }

        private void AssociatedObject_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.LeftButton == System.Windows.Input.MouseButtonState.Pressed)
            {
                if (Source != null && Data != null)
                {
                    DragDrop.DoDragDrop(Source, Data, DragDropEffects);
                }
            }
        }

        protected override void OnDetaching()
        {
            this.AssociatedObject.PreviewMouseLeftButtonDown -= AssociatedObject_PreviewMouseLeftButtonDown;
            base.OnDetaching();
        }
    }
}

﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace EasyCSharp.WPF.Controls
{
    public class DragDropItemsControlTargetBehavior : Behavior<UIElement>
    {
        public Type DataFormat
        {
            get { return (Type)GetValue(DataFormatProperty); }
            set { SetValue(DataFormatProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DataFormat.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataFormatProperty =
            DependencyProperty.Register("DataFormat", typeof(Type), typeof(DragDropItemsControlTargetBehavior), new System.Windows.PropertyMetadata(typeof(object)));

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Command.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(DragDropItemsControlTargetBehavior), new System.Windows.PropertyMetadata(null));



        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.AllowDrop = true;
            this.AssociatedObject.Drop += AssociatedObject_Drop;
        }

        private void AssociatedObject_Drop(object sender, DragEventArgs e)
        {
            if (Command.CanExecute(e))
            {
                var content = e.Data.GetData(DataFormat);
                Command?.Execute(content);
            }
        }     

        protected override void OnDetaching()
        {         
            this.AssociatedObject.Drop -= AssociatedObject_Drop;
            base.OnDetaching();
        }
    }
}

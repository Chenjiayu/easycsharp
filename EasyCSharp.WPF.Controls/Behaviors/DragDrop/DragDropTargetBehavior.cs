﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace EasyCSharp.WPF.Controls
{
    public class DragDropTargetBehavior : Behavior<UIElement>
    {
        public Type DataFormat
        {
            get { return (Type)GetValue(DataFormatProperty); }
            set { SetValue(DataFormatProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DataFormat.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataFormatProperty =
            DependencyProperty.Register("DataFormat", typeof(Type), typeof(DragDropTargetBehavior), new System.Windows.PropertyMetadata(typeof(object)));

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Command.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(DragDropTargetBehavior), new System.Windows.PropertyMetadata(null));


        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.AllowDrop = true;
            this.AssociatedObject.DragEnter += AssociatedObject_DragEnter;
            this.AssociatedObject.DragOver += AssociatedObject_DragOver;
            this.AssociatedObject.DragLeave += AssociatedObject_DragLeave;
            this.AssociatedObject.Drop += AssociatedObject_Drop;
        }

        private void AssociatedObject_Drop(object sender, DragEventArgs e)
        {
            if (Command.CanExecute(e))
            {
                var content = e.Data.GetData(DataFormat);
                Command?.Execute(content);
            }
        }

        private void AssociatedObject_DragLeave(object sender, DragEventArgs e)
        {

        }

        private void AssociatedObject_DragOver(object sender, DragEventArgs e)
        {

        }

        private void AssociatedObject_DragEnter(object sender, DragEventArgs e)
        {

        }

        protected override void OnDetaching()
        {
            this.AssociatedObject.DragEnter -= AssociatedObject_DragEnter;
            this.AssociatedObject.DragOver -= AssociatedObject_DragOver;
            this.AssociatedObject.DragLeave -= AssociatedObject_DragLeave;
            this.AssociatedObject.Drop -= AssociatedObject_Drop;
            base.OnDetaching();
        }
    }
}

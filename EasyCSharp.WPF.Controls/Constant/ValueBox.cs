﻿using EasyCSharp.WPF.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace EasyCSharp.WPF.Controls
{
    public class ValueBox
    {
        public static object DefaultSolidColorBrush
        {
            get
            {
                return new SolidColorBrush();
            }
        }

        public static object DefaultThickness
        {
            get
            {
                return new Thickness();
            }
        }

        public static object DefaultDouble
        {
            get
            {
                return .0;
            }
        }

        public static object DefaultDoubleNaN
        {
            get
            {
                return double.NaN;
            }
        }

        public static object DefaultCornerRadius
        {
            get
            {
                return new CornerRadius();
            }
        }

        public static object DefaultStringEmpty
        {
            get
            {
                return string.Empty;
            }
        }

        public static object True
        {
            get
            {
                return true;
            }
        }

        public static object False
        {
            get
            {
                return false;
            }
        }

        internal static object GetUnwrappedObject(object currentObject)
        {
            var customTypeDescriptor = currentObject as ICustomTypeDescriptor;
            return customTypeDescriptor != null ? customTypeDescriptor.GetPropertyOwner(null) : currentObject;
        }

        private static readonly string[] StringConverterMembers = { "Content", "Header", "ToolTip", "Tag" };

        private static StringConverter _defaultStringConverter;
        public static StringConverter DefaultStringConverter
        {
            get
            {
                if (_defaultStringConverter == null)
                    _defaultStringConverter = new StringConverter();
                return _defaultStringConverter;
            }
        }

        private static FontStretchConverterDecorator _defaultFontStretchConverterDecorator;
        public static FontStretchConverterDecorator DefaultFontStretchConverterDecorator
        {
            get { return _defaultFontStretchConverterDecorator ?? (_defaultFontStretchConverterDecorator = new FontStretchConverterDecorator()); }
        }

        private static FontStyleConverterDecorator _DefaultFontStyleConverterDecorator;
        public static FontStyleConverterDecorator DefaultFontStyleConverterDecorator
        {
            get
            {
                if (_DefaultFontStyleConverterDecorator == null)
                    _DefaultFontStyleConverterDecorator = new FontStyleConverterDecorator();
                return _DefaultFontStyleConverterDecorator;
            }
        }

        private static FontWeightConverterDecorator _defaultFontWeightConverterDecorator;
        public static FontWeightConverterDecorator DefaultFontWeightConverterDecorator
        {
            get { return _defaultFontWeightConverterDecorator ?? (_defaultFontWeightConverterDecorator = new FontWeightConverterDecorator()); }
        }

        public static TypeConverter GetPropertyConverter(PropertyDescriptor propertyDescriptor)
        {
            if (propertyDescriptor == null)
                throw new ArgumentNullException("propertyDescriptor");

            if (StringConverterMembers.Contains(propertyDescriptor.Name)
              && propertyDescriptor.PropertyType.IsAssignableFrom(typeof(object)))
                return DefaultStringConverter;
            if (typeof(FontStretch).IsAssignableFrom(propertyDescriptor.PropertyType))
                return DefaultFontStretchConverterDecorator;
            if (typeof(FontStyle).IsAssignableFrom(propertyDescriptor.PropertyType))
                return DefaultFontStyleConverterDecorator;
            if (typeof(FontWeight).IsAssignableFrom(propertyDescriptor.PropertyType))
                return DefaultFontWeightConverterDecorator;
            return propertyDescriptor.Converter;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace EasyCSharp.WPF.Controls
{
    public class EasyBrush: DependencyObject
    {
        #region Focus

        public static Brush GetFocusedBackground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(FocusedBackgroundProperty);
        }

        public static void SetFocusedBackground(DependencyObject obj, Brush value)
        {
            obj.SetValue(FocusedBackgroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for FocusedBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FocusedBackgroundProperty =
            DependencyProperty.RegisterAttached("FocusedBackground", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));



        public static Brush GetFocusedBorderBrush(DependencyObject obj)
        {
            return (Brush)obj.GetValue(FocusedBorderBrushProperty);
        }

        public static void SetFocusedBorderBrush(DependencyObject obj, Brush value)
        {
            obj.SetValue(FocusedBorderBrushProperty, value);
        }

        // Using a DependencyProperty as the backing store for FocusedBorderBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FocusedBorderBrushProperty =
            DependencyProperty.RegisterAttached("FocusedBorderBrush", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));



        public static Brush GetFocusedForeground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(FocusedForegroundProperty);
        }

        public static void SetFocusedForeground(DependencyObject obj, Brush value)
        {
            obj.SetValue(FocusedForegroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for FocusedForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FocusedForegroundProperty =
            DependencyProperty.RegisterAttached("FocusedForeground", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));

        #endregion

        #region MouseHover

        public static Brush GetMouseHoverBackground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(MouseHoverBackgroundProperty);
        }

        public static void SetMouseHoverBackground(DependencyObject obj, Brush value)
        {
            obj.SetValue(MouseHoverBackgroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for MouseHoverBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MouseHoverBackgroundProperty =
            DependencyProperty.RegisterAttached("MouseHoverBackground", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));



        public static Brush GetMouseHoverBorderBrush(DependencyObject obj)
        {
            return (Brush)obj.GetValue(MouseHoverBorderBrushProperty);
        }

        public static void SetMouseHoverBorderBrush(DependencyObject obj, Brush value)
        {
            obj.SetValue(MouseHoverBorderBrushProperty, value);
        }

        // Using a DependencyProperty as the backing store for MouseHoverBorderBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MouseHoverBorderBrushProperty =
            DependencyProperty.RegisterAttached("MouseHoverBorderBrush", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));



        public static Brush GetMouseHoverForeground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(MouseHoverForegroundProperty);
        }

        public static void SetMouseHoverForeground(DependencyObject obj, Brush value)
        {
            obj.SetValue(MouseHoverForegroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for MouseHoverForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MouseHoverForegroundProperty =
            DependencyProperty.RegisterAttached("MouseHoverForeground", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));

        #endregion

        #region Pressed

        public static Brush GetPressedBackground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(PressedBackgroundProperty);
        }

        public static void SetPressedBackground(DependencyObject obj, Brush value)
        {
            obj.SetValue(PressedBackgroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for PressedBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PressedBackgroundProperty =
            DependencyProperty.RegisterAttached("PressedBackground", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));



        public static Brush GetPressedBorderBrush(DependencyObject obj)
        {
            return (Brush)obj.GetValue(PressedBorderBrushProperty);
        }

        public static void SetPressedBorderBrush(DependencyObject obj, Brush value)
        {
            obj.SetValue(PressedBorderBrushProperty, value);
        }

        // Using a DependencyProperty as the backing store for PressedBorderBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PressedBorderBrushProperty =
            DependencyProperty.RegisterAttached("PressedBorderBrush", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));



        public static Brush GetPressedForeground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(PressedForegroundProperty);
        }

        public static void SetPressedForeground(DependencyObject obj, Brush value)
        {
            obj.SetValue(PressedForegroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for PressedForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PressedForegroundProperty =
            DependencyProperty.RegisterAttached("PressedForeground", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));







        #endregion

        #region Disable

        public static Brush GetDisableBackground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(DisableBackgroundProperty);
        }

        public static void SetDisableBackground(DependencyObject obj, Brush value)
        {
            obj.SetValue(DisableBackgroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for DisableBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisableBackgroundProperty =
            DependencyProperty.RegisterAttached("DisableBackground", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));



        public static Brush GetDisableForeground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(DisableForegroundProperty);
        }

        public static void SetDisableForeground(DependencyObject obj, Brush value)
        {
            obj.SetValue(DisableForegroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for DisableForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisableForegroundProperty =
            DependencyProperty.RegisterAttached("DisableForeground", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));



        public static Brush GetDisableBorderBrush(DependencyObject obj)
        {
            return (Brush)obj.GetValue(DisableBorderBrushProperty);
        }

        public static void SetDisableBorderBrush(DependencyObject obj, Brush value)
        {
            obj.SetValue(DisableBorderBrushProperty, value);
        }

        // Using a DependencyProperty as the backing store for DisableBorderBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisableBorderBrushProperty =
            DependencyProperty.RegisterAttached("DisableBorderBrush", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));
               
        #endregion

        #region HightLight

        public static Brush GetHightLightBackground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(HightLightBackgroundProperty);
        }

        public static void SetHightLightBackground(DependencyObject obj, Brush value)
        {
            obj.SetValue(HightLightBackgroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for HightLightBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HightLightBackgroundProperty =
            DependencyProperty.RegisterAttached("HightLightBackground", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));

        public static Brush GetHightLightBorderBrush(DependencyObject obj)
        {
            return (Brush)obj.GetValue(HightLightBorderBrushProperty);
        }

        public static void SetHightLightBorderBrush(DependencyObject obj, Brush value)
        {
            obj.SetValue(HightLightBorderBrushProperty, value);
        }

        // Using a DependencyProperty as the backing store for HightLightBorderBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HightLightBorderBrushProperty =
            DependencyProperty.RegisterAttached("HightLightBorderBrush", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));


        public static Brush GetHightLightForeground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(HightLightForegroundProperty);
        }

        public static void SetHightLightForeground(DependencyObject obj, Brush value)
        {
            obj.SetValue(HightLightForegroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for HightLightForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HightLightForegroundProperty =
            DependencyProperty.RegisterAttached("HightLightForeground", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));


        #endregion

        #region Selected

        public static Brush GetSelectedBackground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(SelectedBackgroundProperty);
        }

        public static void SetSelectedBackground(DependencyObject obj, Brush value)
        {
            obj.SetValue(SelectedBackgroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for SelectedBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedBackgroundProperty =
            DependencyProperty.RegisterAttached("SelectedBackground", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));



        public static Brush GetSelectedForeground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(SelectedForegroundProperty);
        }

        public static void SetSelectedForeground(DependencyObject obj, Brush value)
        {
            obj.SetValue(SelectedForegroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for SelectedForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedForegroundProperty =
            DependencyProperty.RegisterAttached("SelectedForeground", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));



        public static Brush GetSelectedBorderBrush(DependencyObject obj)
        {
            return (Brush)obj.GetValue(SelectedBorderBrushProperty);
        }

        public static void SetSelectedBorderBrush(DependencyObject obj, Brush value)
        {
            obj.SetValue(SelectedBorderBrushProperty, value);
        }

        // Using a DependencyProperty as the backing store for SelectedBorderBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedBorderBrushProperty =
            DependencyProperty.RegisterAttached("SelectedBorderBrush", typeof(Brush), typeof(EasyBrush), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));


        #endregion
    }
}

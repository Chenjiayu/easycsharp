﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace EasyCSharp.WPF.Controls
{
    public class EasyItemsControl : DependencyObject
    {
        public static double GetItemsWidth(DependencyObject obj)
        {
            return (double)obj.GetValue(ItemsWidthProperty);
        }

        public static void SetItemsWidth(DependencyObject obj, double value)
        {
            obj.SetValue(ItemsWidthProperty, value);
        }

        // Using a DependencyProperty as the backing store for ItemsWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsWidthProperty =
            DependencyProperty.RegisterAttached("ItemsWidth", typeof(double), typeof(EasyItemsControl), new System.Windows.PropertyMetadata(ValueBox.DefaultDoubleNaN));


        public static double GetItemsHeight(DependencyObject obj)
        {
            return (double)obj.GetValue(ItemsHeightProperty);
        }

        public static void SetItemsHeight(DependencyObject obj, double value)
        {
            obj.SetValue(ItemsHeightProperty, value);
        }

        // Using a DependencyProperty as the backing store for ItemsHeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsHeightProperty =
            DependencyProperty.RegisterAttached("ItemsHeight", typeof(double), typeof(EasyItemsControl), new System.Windows.PropertyMetadata(ValueBox.DefaultDoubleNaN));

        public static Thickness GetItemsMargin(DependencyObject obj)
        {
            return (Thickness)obj.GetValue(ItemsMarginProperty);
        }

        public static void SetItemsMargin(DependencyObject obj, Thickness value)
        {
            obj.SetValue(ItemsMarginProperty, value);
        }

        // Using a DependencyProperty as the backing store for ItemsMargin.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsMarginProperty =
            DependencyProperty.RegisterAttached("ItemsMargin", typeof(Thickness), typeof(EasyItemsControl), new System.Windows.PropertyMetadata(ValueBox.DefaultThickness));

        public static Thickness GetItemsPadding(DependencyObject obj)
        {
            return (Thickness)obj.GetValue(ItemsPaddingProperty);
        }

        public static void SetItemsPadding(DependencyObject obj, Thickness value)
        {
            obj.SetValue(ItemsPaddingProperty, value);
        }

        // Using a DependencyProperty as the backing store for ItemsPadding.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsPaddingProperty =
            DependencyProperty.RegisterAttached("ItemsPadding", typeof(Thickness), typeof(EasyItemsControl), new System.Windows.PropertyMetadata(ValueBox.DefaultThickness));


        public static Thickness GetItemsPanelMargin(DependencyObject obj)
        {
            return (Thickness)obj.GetValue(ItemsPanelMarginProperty);
        }

        public static void SetItemsPanelMargin(DependencyObject obj, Thickness value)
        {
            obj.SetValue(ItemsPanelMarginProperty, value);
        }

        // Using a DependencyProperty as the backing store for ItemsPanelMargin.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsPanelMarginProperty =
            DependencyProperty.RegisterAttached("ItemsPanelMargin", typeof(Thickness), typeof(EasyItemsControl), new System.Windows.PropertyMetadata(ValueBox.DefaultThickness));



        public static Thickness GetItemsPanelPadding(DependencyObject obj)
        {
            return (Thickness)obj.GetValue(ItemsPanelPaddingProperty);
        }

        public static void SetItemsPanelPadding(DependencyObject obj, Thickness value)
        {
            obj.SetValue(ItemsPanelPaddingProperty, value);
        }

        // Using a DependencyProperty as the backing store for ItemsPanelPadding.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsPanelPaddingProperty =
            DependencyProperty.RegisterAttached("ItemsPanelPadding", typeof(Thickness), typeof(EasyItemsControl), new System.Windows.PropertyMetadata(ValueBox.DefaultThickness));



        public static Brush GetItemsBackground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(ItemsBackgroundProperty);
        }

        public static void SetItemsBackground(DependencyObject obj, Brush value)
        {
            obj.SetValue(ItemsBackgroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for ItemsBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsBackgroundProperty =
            DependencyProperty.RegisterAttached("ItemsBackground", typeof(Brush), typeof(EasyItemsControl), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));



        public static Brush GetItemsBorderBrush(DependencyObject obj)
        {
            return (Brush)obj.GetValue(ItemsBorderBrushProperty);
        }

        public static void SetItemsBorderBrush(DependencyObject obj, Brush value)
        {
            obj.SetValue(ItemsBorderBrushProperty, value);
        }

        // Using a DependencyProperty as the backing store for ItemsBorderBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsBorderBrushProperty =
            DependencyProperty.RegisterAttached("ItemsBorderBrush", typeof(Brush), typeof(EasyItemsControl), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));



        public static Thickness GetItemsBorderThickness(DependencyObject obj)
        {
            return (Thickness)obj.GetValue(ItemsBorderThicknessProperty);
        }

        public static void SetItemsBorderThickness(DependencyObject obj, Thickness value)
        {
            obj.SetValue(ItemsBorderThicknessProperty, value);
        }

        // Using a DependencyProperty as the backing store for ItemsBorderThickness.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsBorderThicknessProperty =
            DependencyProperty.RegisterAttached("ItemsBorderThickness", typeof(Thickness), typeof(EasyItemsControl), new System.Windows.PropertyMetadata(ValueBox.DefaultThickness));



        public static Brush GetItemsForeground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(ItemsForegroundProperty);
        }

        public static void SetItemsForeground(DependencyObject obj, Brush value)
        {
            obj.SetValue(ItemsForegroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for ItemsForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsForegroundProperty =
            DependencyProperty.RegisterAttached("ItemsForeground", typeof(Brush), typeof(EasyItemsControl), new System.Windows.PropertyMetadata(ValueBox.DefaultSolidColorBrush));



        public static CornerRadius GetItemsCornerRadius(DependencyObject obj)
        {
            return (CornerRadius)obj.GetValue(ItemsCornerRadiusProperty);
        }

        public static void SetItemsCornerRadius(DependencyObject obj, CornerRadius value)
        {
            obj.SetValue(ItemsCornerRadiusProperty, value);
        }

        // Using a DependencyProperty as the backing store for ItemsCornerRadius.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsCornerRadiusProperty =
            DependencyProperty.RegisterAttached("ItemsCornerRadius", typeof(CornerRadius), typeof(EasyItemsControl), new System.Windows.PropertyMetadata(ValueBox.DefaultCornerRadius));

        #region Events

        public static readonly RoutedEvent ItemsMouseLeftButtonDownEvent = EventManager.RegisterRoutedEvent
            ("ItemsMouseLeftButtonDownEvent", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EasyItemsControl));
     
        public static void AddItemsMouseLeftButtonDownHandler(DependencyObject d, RoutedEventHandler e)
        {
            EasyEvent.AddHandler<ItemsControl>(d, e, ItemsMouseLeftButtonDownEvent);
        }
        
        public static void RemoveItemsMouseLeftButtonDownHandler(DependencyObject d, RoutedEventHandler e)
        {
            EasyEvent.RemoveHandler<ItemsControl>(d, e, ItemsMouseLeftButtonDownEvent);           
        }

        #endregion       
    }

}

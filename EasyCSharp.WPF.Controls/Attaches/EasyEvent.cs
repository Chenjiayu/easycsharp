﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace EasyCSharp.WPF.Controls
{
    public static class EasyEvent
    {
        public static void AddHandler<T>(DependencyObject d, RoutedEventHandler e, RoutedEvent routedEvent) where T : UIElement
        {
            T ui = (T)d;
            if (ui != null)
            {
                ui.AddHandler(routedEvent, e);
            }
        }

        public static void RemoveHandler<T>(DependencyObject d, RoutedEventHandler e, RoutedEvent routedEvent) where T : UIElement
        {
            T ui = (T)d;
            if (ui != null)
            {
                ui.RemoveHandler(routedEvent, e);
            }
        }
    }
}

﻿using System;
using System.Windows;
using System.Windows.Media;

namespace EasyCSharp.WPF.Controls
{
    public class EasyDragDrop : DependencyObject
    {
        //static EasyDragDrop()
        //{
        //    PreviewDragEnterEvent = DragDrop.PreviewDragEnterEvent.AddOwner(typeof(EasyDragDrop));            
        //    PreviewDragLeaveEvent = DragDrop.PreviewDragLeaveEvent.AddOwner(typeof(EasyDragDrop));
        //    PreviewDragOverEvent = DragDrop.PreviewDragOverEvent.AddOwner(typeof(EasyDragDrop));
        //    PreviewDropEvent = DragDrop.PreviewDropEvent.AddOwner(typeof(EasyDragDrop));
        //}

        public static bool GetIsDragging(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsDraggingProperty);
        }

        public static void SetIsDragging(DependencyObject obj, bool value)
        {
            obj.SetValue(IsDraggingProperty, value);
        }

        // Using a DependencyProperty as the backing store for IsDragging.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsDraggingProperty =
            DependencyProperty.RegisterAttached("IsDragging", typeof(bool), typeof(EasyDragDrop), new System.Windows.PropertyMetadata(false));

        public static FrameworkElement GetDragDropElement(DependencyObject obj)
        {
            return (FrameworkElement)obj.GetValue(DragDropElementProperty);
        }

        public static void SetDragDropElement(DependencyObject obj, Visual value)
        {
            obj.SetValue(DragDropElementProperty, value);
        }

        // Using a DependencyProperty as the backing store for DragDropElement.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DragDropElementProperty =
            DependencyProperty.RegisterAttached("DragDropElement", typeof(FrameworkElement), typeof(EasyDragDrop), new System.Windows.PropertyMetadata(null));

        public static bool GetIsDragOver(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsDragOverProperty);
        }

        public static void SetIsDragOver(DependencyObject obj, bool value)
        {
            obj.SetValue(IsDragOverProperty, value);
        }

        // Using a DependencyProperty as the backing store for IsDragOver.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsDragOverProperty =
            DependencyProperty.RegisterAttached("IsDragOver", typeof(bool), typeof(EasyDragDrop), new System.Windows.PropertyMetadata(false, OnIsDragOverChanged));

        private static void OnIsDragOverChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                DragDropCoverAdorner.Cover(d as UIElement);
            }
            else
            {
                DragDropCoverAdorner.UnCover(d as UIElement);
            }
        }

        //#region Events
      
        //public static readonly RoutedEvent PreviewDragEnterEvent;
        //public static readonly RoutedEvent PreviewDragLeaveEvent;
        //public static readonly RoutedEvent PreviewDragOverEvent;
        //public static readonly RoutedEvent PreviewDropEvent;

        //#endregion
    }
}

﻿using EasyCSharp.WPF.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;

namespace EasyCSharp.WPF.Controls
{
    public abstract class DragDropAdorner: ContentAdorner
    {
        public DragDropAdorner(UIElement adornedElement) : base(adornedElement)
        {
            this.IsHitTestVisible = false;           
        }

        protected abstract void RenderFunc(DrawingContext drawingContext);

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);
            this.RenderFunc(drawingContext);
        }
    }

    public class DragDropSourceAdorner : DragDropAdorner
    {
        FrameworkElement mDraggedElement;
        public DragDropSourceAdorner(UIElement adornedElement) : base(adornedElement)
        {            
            adornedElement.PreviewQueryContinueDrag += AdornedElement_PreviewQueryContinueDrag;           
        }

        private void AdornedElement_PreviewQueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            InvalidateVisual();
        }

        protected override void RenderFunc(DrawingContext drawingContext)
        {
            mDraggedElement = EasyDragDrop.GetDragDropElement(AdornedElement);
            if (mDraggedElement != null)
            {
                POINT screenPos = new POINT();
                if (Win32.GetCursorPos(ref screenPos))
                {
                    Point pos = PointFromScreen(new Point(screenPos.X, screenPos.Y));
                    Rect rect = new Rect(pos.X, pos.Y, mDraggedElement.ActualWidth, mDraggedElement.ActualHeight);
                    drawingContext.PushOpacity(1.0);
                    Brush highlight = mDraggedElement.TryFindResource(SystemColors.HighlightBrushKey) as Brush;
                    if (highlight != null)
                        drawingContext.DrawRectangle(highlight, new Pen(Brushes.Red, 0), rect);
                    drawingContext.DrawRectangle(new VisualBrush(mDraggedElement),
                        new Pen(Brushes.Transparent, 0), rect);
                    drawingContext.Pop();
                }
            }
            mDraggedElement = null;
        }       
    }

    public class DragDropCoverAdorner : DragDropAdorner
    {
        public DragDropCoverAdorner(UIElement adornedElement) : base(adornedElement)
        {
                 
        }       

        public static void Cover(UIElement uIElement)
        {
            if (uIElement == null) return;
            var layer = AdornerLayer.GetAdornerLayer(uIElement);
            var adorners = layer.GetAdorners(uIElement);
            if (adorners != null && adorners.Length > 0)
            {
                foreach (var adorner in adorners)
                {
                    if (adorner is DragDropCoverAdorner)
                    {
                        return;
                    }
                }
            }

            layer.Add(new DragDropCoverAdorner(uIElement));
        }

        public static void UnCover(UIElement uIElement)
        {
            if (uIElement == null) return;
            var layer = AdornerLayer.GetAdornerLayer(uIElement);
            var adorners = layer.GetAdorners(uIElement);
            if (adorners == null || adorners.Length == 0)
            {
                return;
            }

            for (int i = 0; i < adorners.Length; i++)
            {
                var adorner = adorners[i];
                if (adorner is DragDropCoverAdorner dropTargetAdorner)
                {
                    layer.Remove(dropTargetAdorner);
                    return;
                }
            }
        }

        protected override void RenderFunc(DrawingContext drawingContext)
        {
            Brush highlight = new SolidColorBrush(Color.FromArgb(22, 255, 255, 255));
            Rect rect = new Rect(0, 0, AdornedElement.RenderSize.Width, AdornedElement.RenderSize.Height);
            drawingContext.DrawRectangle(highlight, new Pen(Brushes.Red, 0), rect);
        }
    }
}

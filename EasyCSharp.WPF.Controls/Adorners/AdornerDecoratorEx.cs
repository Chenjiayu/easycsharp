﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;

namespace EasyCSharp.WPF.Controls
{
    public class AdornerDecoratorEx : AdornerDecorator
    {
        private bool hasOpened;

        public Type ContentAdornerType
        {
            get; set;
        }

        public AdornerDecoratorEx()
        {
            this.Loaded += AdornerDecoratorEx_Loaded;
        }

        private void AdornerDecoratorEx_Loaded(object sender, RoutedEventArgs e)
        {
            if (!hasOpened)
            {
                ContentAdornerType = ContentAdornerType ?? typeof(ContentAdorner);
                ContentAdorner contentAdorner = Activator.CreateInstance(ContentAdornerType, base.Child) as ContentAdorner;
                AdornerLayer.Add(contentAdorner);
                var content = ContentAdorner.GetAdornerContent(base.Child);
                contentAdorner.Content = content;
                ContentAdorner.SetContentAdorner(base.Child, contentAdorner);

                hasOpened = true;
            }
        }
    }

    public class DragDropAdornerHost : AdornerDecorator
    {
        private bool hasOpened;

        public Type DragDropAdornerType
        {
            get; set;
        }

        public DragDropAdornerHost()
        {
            this.Loaded += AdornerDecoratorEx_Loaded;
        }

        private void AdornerDecoratorEx_Loaded(object sender, RoutedEventArgs e)
        {
            if (!hasOpened)
            {
                DragDropAdornerType = DragDropAdornerType ?? typeof(DragDropSourceAdorner);
                DragDropAdorner dragDropAdorner = Activator.CreateInstance(DragDropAdornerType, base.Child) as DragDropAdorner;
                AdornerLayer.Add(dragDropAdorner);
                //var content = ContentAdorner.GetAdornerContent(base.Child);
                //dragDropAdorner.Content = content;
                //ContentAdorner.SetContentAdorner(base.Child, dragDropAdorner);

                hasOpened = true;
            }
        }
    }
}

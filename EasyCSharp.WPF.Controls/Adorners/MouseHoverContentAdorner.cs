﻿using EasyCSharp.WPF.Core;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;

namespace EasyCSharp.WPF.Controls
{
    public class MouseHoverContentAdorner : ContentAdorner
    {
        public MouseHoverContentAdorner(UIElement adornedElement) : base(adornedElement)
        {
            //MultiBinding multiBinding = new MultiBinding();         
            //multiBinding.Converter = new BooleansToVisibilityConverter() { Any = true };
            //multiBinding.Bindings.Add(new Binding("IsMouseOver") { Source = adornedElement });
            //multiBinding.Bindings.Add(new Binding("IsMouseOver") { Source = this });
            //this.SetBinding(VisibilityProperty, multiBinding);

            var decorator = VisualTreeHelperEx.FindParent<AdornerDecoratorEx>(adornedElement);
            this.SetBinding(VisibilityProperty, new Binding("IsMouseOver") { Source = decorator, Converter = new BooleanToVisibilityConverter() });
        }
    }
}

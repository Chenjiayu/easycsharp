﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace EasyCSharp.WPF.Controls
{
    public class ContentAdorner : Adorner
    {
        protected VisualCollection visualCollection;
        protected TransitioningContentControl contentControl;

        public ContentAdorner(UIElement adornedElement) : base(adornedElement)
        {
            visualCollection = new VisualCollection(this);

            contentControl = new TransitioningContentControl();

            visualCollection.Add(contentControl);
        }

        public object Content
        {
            get { return GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Content.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContentProperty =
            DependencyProperty.Register("Content", typeof(object), typeof(ContentAdorner), new System.Windows.PropertyMetadata(null, ContentChangedCallBack));

        private static void ContentChangedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((ContentAdorner)d).Render(e.NewValue);
        }

        static ContentAdorner()
        {
            //VisibilityProperty.OverrideMetadata(typeof(ContentAdorner), new PropertyMetadata(Visibility.Collapsed));
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ContentAdorner), new FrameworkPropertyMetadata(typeof(ContentAdorner)));
        }

        public virtual void Render(object newContent)
        {
            contentControl.Content = newContent;
            contentControl.InvalidateArrange();
        }

        protected override Visual GetVisualChild(int index)
        {
            return visualCollection[index];
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            contentControl.Arrange(new Rect(finalSize));
            return base.ArrangeOverride(finalSize);
        }

        protected override int VisualChildrenCount
        {
            get
            {
                return visualCollection.Count;
            }
        }

        public static ContentAdorner GetContentAdorner(DependencyObject obj)
        {
            return (ContentAdorner)obj.GetValue(ContentAdornerProperty);
        }

        public static void SetContentAdorner(DependencyObject obj, ContentAdorner value)
        {
            obj.SetValue(ContentAdornerProperty, value);
        }

        // Using a DependencyProperty as the backing store for ContentAdorner.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContentAdornerProperty =
            DependencyProperty.RegisterAttached("ContentAdorner", typeof(ContentAdorner), typeof(ContentAdorner), new System.Windows.PropertyMetadata(null));



        public static object GetAdornerContent(DependencyObject obj)
        {
            return (object)obj.GetValue(AdornerContentProperty);
        }

        public static void SetAdornerContent(DependencyObject obj, object value)
        {
            obj.SetValue(AdornerContentProperty, value);
        }

        // Using a DependencyProperty as the backing store for AdornerContent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AdornerContentProperty =
            DependencyProperty.RegisterAttached("AdornerContent", typeof(object), typeof(ContentAdorner), new System.Windows.PropertyMetadata(null, OnAdornerContentChanged));

        private static void OnAdornerContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var adorner = GetContentAdorner(d);
            if (adorner != null)
            {
                adorner.Content = e.NewValue;
            }
        }

        public static bool GetAdornerShow(DependencyObject obj)
        {
            return (bool)obj.GetValue(AdornerShowProperty);
        }

        public static void SetAdornerShow(DependencyObject obj, bool value)
        {
            obj.SetValue(AdornerShowProperty, value);
        }

        // Using a DependencyProperty as the backing store for AdornerShow.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AdornerShowProperty =
            DependencyProperty.RegisterAttached("AdornerShow", typeof(bool), typeof(ContentAdorner), new System.Windows.PropertyMetadata(false, OnAdornerShowChanged));

        private static void OnAdornerShowChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var adorner = GetContentAdorner(d);
            if (adorner != null)
            {
                if ((bool)e.NewValue)
                {
                    adorner.Visibility = Visibility.Visible;
                    var showSb = GetShowStoryboard(d);
                    if (showSb != null)
                    {
                        var storyboard = showSb.CloneCurrentValue() as Storyboard;
                        //storyboard.Children[0].SetValue(Storyboard.TargetProperty, adorner);
                        storyboard.Completed += (s, ea) =>
                        {
                            storyboard.Remove(adorner);
                        };
                        storyboard.Begin(adorner, true);
                    }

                }
                else
                {
                    var hideSb = GetHideStoryboard(d);
                    if (hideSb != null)
                    {
                        var storyboard = hideSb.CloneCurrentValue() as Storyboard;
                        storyboard.Completed += (s, ea) =>
                        {
                            storyboard.Remove(adorner);
                            adorner.Visibility = Visibility.Collapsed;
                        };
                        storyboard.Begin(adorner, true);
                    }
                    else
                    {
                        adorner.Visibility = Visibility.Collapsed;
                    }
                }
            }
        }

        public static string GetTitle(DependencyObject obj)
        {
            return (string)obj.GetValue(TitleProperty);
        }

        public static void SetTitle(DependencyObject obj, string value)
        {
            obj.SetValue(TitleProperty, value);
        }

        // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.RegisterAttached("Title", typeof(string), typeof(ContentAdorner), new System.Windows.PropertyMetadata(string.Empty, OnTitleChanged));

        private static void OnTitleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        public static string GetMessage(DependencyObject obj)
        {
            return (string)obj.GetValue(MessageProperty);
        }

        public static void SetMessage(DependencyObject obj, string value)
        {
            obj.SetValue(MessageProperty, value);
        }

        // Using a DependencyProperty as the backing store for Message.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.RegisterAttached("Message", typeof(string), typeof(ContentAdorner), new System.Windows.PropertyMetadata(string.Empty, OnMessageChanged));

        private static void OnMessageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        public static Storyboard GetShowStoryboard(DependencyObject obj)
        {
            return (Storyboard)obj.GetValue(ShowStoryboardProperty);
        }

        public static void SetShowStoryboard(DependencyObject obj, Storyboard value)
        {
            obj.SetValue(ShowStoryboardProperty, value);
        }

        // Using a DependencyProperty as the backing store for ShowStoryboard.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowStoryboardProperty =
            DependencyProperty.RegisterAttached("ShowStoryboard", typeof(Storyboard), typeof(ContentAdorner), new System.Windows.PropertyMetadata(null));



        public static Storyboard GetHideStoryboard(DependencyObject obj)
        {
            return (Storyboard)obj.GetValue(HideStoryboardProperty);
        }

        public static void SetHideStoryboard(DependencyObject obj, Storyboard value)
        {
            obj.SetValue(HideStoryboardProperty, value);
        }

        // Using a DependencyProperty as the backing store for HideStoryboard.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HideStoryboardProperty =
            DependencyProperty.RegisterAttached("HideStoryboard", typeof(Storyboard), typeof(ContentAdorner), new System.Windows.PropertyMetadata(null));





    }
}

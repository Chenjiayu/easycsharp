﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.Core
{
    public class ConsoleManager
    {
        static ConsoleManager()
        {
#if DEBUG
            Debug.AutoFlush = true;
#elif TRACE
                Trace.AutoFlush = true;       
#endif
        }
        private const string Kernel32_DllName = "kernel32.dll";
        [DllImport(Kernel32_DllName)]
        private static extern bool AllocConsole();
        [DllImport(Kernel32_DllName)]
        private static extern bool FreeConsole();
        [DllImport(Kernel32_DllName)]
        private static extern IntPtr GetConsoleWindow();
        [DllImport(Kernel32_DllName)]
        private static extern int GetConsoleOutputCP();


        public static bool HasConsole
        {
            get { return GetConsoleWindow() != IntPtr.Zero; }
        }
        /// Creates a new console instance if the process is not attached to a console already.  
        public static void Show()
        {
            if (!HasConsole)
            {
                AllocConsole();
#if DEBUG
                ConsoleTraceListener consoleTraceListener = new ConsoleTraceListener(true);
                Debug.Listeners.Add(consoleTraceListener);
#elif TRACE
                ConsoleTraceListener consoleTraceListener = new ConsoleTraceListener(true);
                Trace.Listeners.Add(consoleTraceListener);               
#endif
                Console.BackgroundColor = ConsoleColor.Black;
                Console.Title = Assembly.GetEntryAssembly().GetName().Name;
                InvalidateOutAndError();
            }
        }
        /// If the process has a console attached to it, it will be detached and no longer visible. Writing to the System.Console is still possible, but no output will be shown.   
        public static void Hide()
        {
            if (HasConsole)
            {
                SetOutAndErrorNull();
                FreeConsole();
            }
        }

        public static void Toggle()
        {
            if (HasConsole)
            {
                Hide();
            }
            else
            {
                Show();
            }
        }

        static void InvalidateOutAndError()
        {
#if DEBUG
            Type type = typeof(System.Console);
            System.Reflection.FieldInfo _out = type.GetField("_out",
                System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
            System.Reflection.FieldInfo _error = type.GetField("_error",
                System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
            System.Reflection.MethodInfo _InitializeStdOutError = type.GetMethod("InitializeStdOutError",
                System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
            Debug.Assert(_out != null);
            Debug.Assert(_error != null);
            Debug.Assert(_InitializeStdOutError != null);
            _out.SetValue(null, null);
            _error.SetValue(null, null);
            _InitializeStdOutError.Invoke(null, new object[] { true });
#endif
        }

        static void SetOutAndErrorNull()
        {
            Console.SetOut(TextWriter.Null);
            Console.SetError(TextWriter.Null);
        }

        /// <summary>
        /// 该方法只在退出程序时，调用
        /// </summary>
        public static void Close()
        {
            FreeConsole();
        }

        public static void WriteLine(string flag, string msg)
        {
#if DEBUG
            Trace.WriteLine($"[{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}][{flag}]{msg}");
#elif TRACE
            Debug.WriteLine($"[{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}][{flag}]{msg}");
#endif

            Console.ForegroundColor = ConsoleColor.White;
        }
        public static void WriteLineError(string msg)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            WriteLine("Error", msg);
        }

        public static void WriteException(Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            WriteLine("Exception", ex.ToFullString());
        }

        public static void WriteLineInfo(string msg)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            WriteLine("Info", msg);
        }

        public static void WriteLineWarning(string msg)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            WriteLine("Warning", msg);
        }

        public static void LogListen(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                fileName = Assembly.GetEntryAssembly().GetName().Name;
#if DEBUG
            TextWriterTraceListener textWriterTraceListener = new TextWriterTraceListener(fileName);
            Debug.Listeners.Add(textWriterTraceListener);
#elif TRACE
            TextWriterTraceListener textWriterTraceListener = new TextWriterTraceListener(fileName);
            Trace.Listeners.Add(textWriterTraceListener);           
#endif
        }
    }
}

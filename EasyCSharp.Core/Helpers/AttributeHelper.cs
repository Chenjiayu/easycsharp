﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.Core
{
    public class AttributeHelper
    {
        #region Common   

        /// <summary>
        /// 获取类型某个特性的某属性值
        /// </summary>
        /// <typeparam name="T">特性类型</typeparam>
        /// <param name="type">类名</param>
        /// <returns></returns>
        public static object GetValue<T>(Type type, string propertyName) where T : Attribute
        {
            var attrType = typeof(T);
            var attribute = type.GetCustomAttributes(attrType, false).FirstOrDefault();

            if (attribute is Attribute att)
            {
                return ReflectionHelper.GetPropertyValue(attribute, propertyName);
            }

            return null;
        }

        /// <summary>
        /// 获取描述特性内容
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetEnumDescription(Enum value)
        {
            var type = value.GetType();
            var field = type.GetField(Enum.GetName(type, value));
            if (field != null)
            {
                var des = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (des != null)
                {
                    return des.Description;
                }
            }

            return value.ToString();
        }

        #endregion

        #region Enum

        /// <summary>
        /// 获取枚举值与描述字典
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static Dictionary<T, string> GetEnumDescriptions<T>() where T : Enum
        {
            var type = typeof(T);
            if (!type.IsEnum)
            {
                return null;
            }

            Dictionary<T, string> dic = new Dictionary<T, string>();

            string description = string.Empty;
            foreach (var item in Enum.GetValues(type))
            {
                var attrs = item.GetType().GetField(item.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                {
                    DescriptionAttribute descAttr = attrs[0] as DescriptionAttribute;
                    description = descAttr.Description;
                }
                dic.Add((T)item, description);
            }

            return dic;
        }

        /// <summary>
        /// 获取枚举值与描述对象列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<EnumMapObject<TEnum>> GetEnumMaps<TEnum>() where TEnum : Enum
        {
            var type = typeof(TEnum);
            string description = string.Empty;
            foreach (var item in Enum.GetValues(type))
            {
                description = item.ToString();
                var attrs = item.GetType().GetField(item.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                {
                    DescriptionAttribute descAttr = attrs[0] as DescriptionAttribute;
                    description = descAttr.Description;
                }

                yield return new EnumMapObject<TEnum>((TEnum)item, description);
            }
        }

        /// <summary>
        /// 获取枚举值与描述对象列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<EnumMapObject> GetEnumMaps(Type enumType)
        {
            return enumType.GetEnumMaps();
        }

        #endregion

        #region MEF

        /// <summary>
        /// 获取MEF框架下类协议导出名
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string GetExportAttributeName(Type type)
        {
            return GetValue<ExportAttribute>(type, "ContractName")?.ToString();
        }

        /// <summary>
        /// 获取MEF框架下类协议导出类型
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static Type GetExportAttributeType(Type type)
        {
            return GetValue<ExportAttribute>(type, "ContractType") as Type;
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.Core
{
    public class AssemblyHelper
    {
        /// <summary>
        /// 加载路径下所有DLL
        /// </summary>
        /// <param name="path">路径</param>
        /// <param name="key">关键词，null代表加载全部</param>
        public static IEnumerable<Assembly> Load(string path, Predicate<string> key = null)
        {
            var assemblies = Load(path);
            return assemblies.Where(i =>
            {
                return key == null ? true : key(i.FullName);
            });
        }

        /// <summary>
        /// 加载目录下所有dll
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static IEnumerable<Assembly> Load(string path)
        {
            string[] files = Directory.GetFiles(path, "*.dll");

            foreach (string file in files)
            {
                yield return Assembly.LoadFile(file);
            }
        }
    }
}

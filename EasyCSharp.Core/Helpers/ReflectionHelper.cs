﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.Core
{
    public class ReflectionHelper
    {
        /// <summary>
        /// 获取对象属性值
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static object GetPropertyValue(object obj, string propertyName)
        {
            Type Ts = obj.GetType();
            object o = Ts.GetProperty(propertyName).GetValue(obj, null);
            string Value = Convert.ToString(o);
            if (string.IsNullOrEmpty(Value))
                return null;
            return Value;
        }

        /// <summary>
        /// 设置值
        /// </summary>
        public static void SetValue(object obj, string propertyName, object value)
        {
            Type type = obj.GetType();
            type.GetProperty(propertyName).SetValue(obj, value, null);
        }

        /// <summary>
        /// 获得目标所有属性与对应值
        /// </summary>
        /// <param name="tatget"></param>
        /// <returns></returns>
        public static Dictionary<PropertyInfo, object> GetPropertyValues(object obj)
        {
            //获取所有属性信息

            var type = obj.GetType();

            var propertyInfos = type.GetProperties();

            Dictionary<PropertyInfo, object> dic = new Dictionary<PropertyInfo, object>();

            foreach (var p in propertyInfos)
            {
                dic.Add(p, p.GetValue(obj));
            }

            return dic;
        }

        /// <summary>
        /// <泛型>反射创建实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetInstance<T>()
        {
            return Activator.CreateInstance<T>();
        }

        /// <summary>
        /// 反射创建实例
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static object GetInstance(Type type)
        {
            return Activator.CreateInstance(type);
        }
    }
}

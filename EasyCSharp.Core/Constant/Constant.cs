﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.Core
{
    internal class Constant
    {
        #region 正则表达式

        /// <summary>
        /// 创建Cookie
        /// </summary>
        public readonly static string VALUE_REGEX_COOKIE_BUILD_PATTERN = @"[^,][\S\s]+?;+[\S\s]+?(?=,\S)";

        /// <summary>
        /// IP地址格式
        /// </summary>
        public readonly static string VALUE_REGEX_IP_PATTERN = @"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";

        /// <summary>
        /// 域名格式
        /// </summary>
        public readonly static string VALUE_REGEX_DOMAIN_PATTERN = @"(?=^.{4,253}$)(^((?!-)[a-zA-Z0-9-]{1,63}(?<!-)\.)+[a-zA-Z]{2,63}\.?$)";

        #endregion
    }
}

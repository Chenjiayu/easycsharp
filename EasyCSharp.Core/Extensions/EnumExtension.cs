﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.Core
{
    public static class EnumExtension
    {
        /// <summary>
        /// 获取描述特性内容
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetEnumDescription(this Enum value)
        {
            return AttributeHelper.GetEnumDescription(value);
        }

        public static IEnumerable<EnumMapObject> GetEnumMaps(this Type enumType)
        {
            if (!enumType.IsEnum)
            {
                throw new ArgumentException($"{enumType.Name}并非枚举类型");
            }

            string description = string.Empty;
            foreach (var item in Enum.GetValues(enumType))
            {
                description = item.ToString();
                var attrs = item.GetType().GetField(item.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                {
                    DescriptionAttribute descAttr = attrs[0] as DescriptionAttribute;
                    description = descAttr.Description;
                }

                yield return new EnumMapObject((Enum)item, description);
            }
        }
    }
}

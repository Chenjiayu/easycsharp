﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.Core
{
    public static class ExceptionExtension
    {
        public static string ToFullString(this Exception exception)
        {
            string result = string.Format("\r\n{0}:{1}\r\n{2}{3}",
                exception.GetType(),
                exception.Message,
                exception.StackTrace,
                exception.InnerException?.ToFullString());

            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EasyCSharp.Core
{
    public static partial class StringExtension
    {
        /// <summary>
        /// 简化写法，判断字符串是否为空,null或只含空格（多个）
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public static bool IsEmpty(this string line)
        {
            return string.IsNullOrWhiteSpace(line) || string.IsNullOrEmpty(line);
        }

        /// <summary>
        /// 获取字符串的MD5
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string GetMd5Hash(this string input)
        {
            var provider = new MD5CryptoServiceProvider();
            var bytes = Encoding.UTF8.GetBytes(input);
            bytes = provider.ComputeHash(bytes);
            return BitConverter.ToString(bytes).Replace("-", "").ToLowerInvariant();
        }

        /// <summary>
        /// 判断字符串是否为IP地址
        /// </summary>
        /// <param name="ipString"></param>
        /// <returns></returns>
        public static bool IsIP(this string ipString)
        {
            return Regex.IsMatch(ipString, Constant.VALUE_REGEX_IP_PATTERN);
        }

        /// <summary>
        /// 验证是否为合法域名
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsDomain(this string value)
        {
            return Regex.IsMatch(value, Constant.VALUE_REGEX_DOMAIN_PATTERN);
        }

        /// <summary>
        /// 验证是否为合法的端口号
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsPort(this string value)
        {
            ushort port = 0;
            bool ret = ushort.TryParse(value, out port);
            if (ret)
            {
                return port > 0;
            }

            return false;
        }

        /// <summary>
        /// 在字符串之间插入制定字符串，并拼凑成一个连续字符串
        /// </summary>
        /// <param name="strings"></param>
        /// <param name="insertString"></param>
        /// <returns></returns>
        public static string InsertString(this IEnumerable<string> strings, string insertString)
        {
            if (strings == null || strings.Count() <= 0)
            {
                throw new Exception("Null IEnumerable<string> strings When InsertString");
            }

            var list = strings.ToList();

            //只有一个字符串，则直接返回，不插入
            if (list.Count() == 1)
            {
                return list[0];
            }
            //多个字符串
            else
            {
                string first = string.Copy(list[0]);

                list = list.Select(i => { return insertString + i; }).ToList();

                StringBuilder builder = new StringBuilder(first);

                for (int i = 1; i < list.Count; i++)
                {
                    builder.Append(list[i]);
                }

                return builder.ToString();
            }
        }

        /// <summary>
        /// 转换为IPAddress
        /// </summary>
        /// <param name="ipString"></param>
        /// <returns></returns>
        public static IPAddress ToIPAddress(this string ipString)
        {
            return IPAddress.Parse(ipString);
        }

        /// <summary>
        /// 转换为IPEndPoint
        /// </summary>
        /// <param name="ipString"></param>
        /// <param name="port"></param>
        /// <returns></returns>
        public static IPEndPoint ToIPEndPoint(this string ipString, int port)
        {
            return new IPEndPoint(ipString.ToIPAddress(), port);
        }

        ///// <summary>
        ///// HTML解码
        ///// </summary>
        ///// <param name="htmlString"></param>
        ///// <returns></returns>
        //public static string HTMLDecode(this string htmlString)
        //{
        //    return HttpUtility.HtmlDecode(htmlString);
        //}

        /// <summary>
        /// 去除html标签
        /// </summary>
        /// <param name="htmlString"></param>
        /// <returns></returns>
        public static string RemoveHTMLFlag(this string htmlString)
        {
            string strText = Regex.Replace(htmlString, "<[^>]+>", "");
            strText = Regex.Replace(strText, "&[^;]+;", "");
            return strText;
        }
    }
}

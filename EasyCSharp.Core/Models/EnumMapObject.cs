﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.Core
{
    public class EnumMapObject
    {
        public EnumMapObject(Enum value, string description)
        {
            Value = value;
            Description = description;
        }

        public Enum Value { get; protected set; }
        public string Description { get; protected set; }
    }

    public class EnumMapObject<TEnum> : EnumMapObject where TEnum : Enum
    {
        public EnumMapObject(TEnum value, string description) : base(value, description)
        {

        }

        public new TEnum Value
        {
            get => (TEnum)base.Value;
            protected set => base.Value = value;
        }
    }
}

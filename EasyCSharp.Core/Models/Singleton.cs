﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.Core
{
    /// <summary>
    /// 单例模式基类
    /// </summary>
    public class Singleton<T> where T : class, new()
    {
        private static readonly object locker = new object();

        private static T instance = null;
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (locker)
                    {
                        if (instance == null)
                        {
                            instance = new T();
                        }
                    }
                }
                return instance;
            }
        }
    }
}

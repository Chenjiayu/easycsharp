﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.Core
{
    public abstract class BaseConfigService
    {
        public virtual Config Config
        {
            get; private set;
        }

        public virtual ConfigItem this[string path]
        {
            get
            {
                return Config?.Get(path);
            }
        }

        public virtual void Load(string path)
        {
            Config = Config.LoadXML(path);
        }

        public virtual void Save(string path)
        {
            Config.SaveXML(path);
        }
    }
}

﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace EasyCSharp.Core
{
    /// <summary>
    /// 配置内容项
    /// </summary>
    public class ConfigItem
    {
        public ConfigItem() { }

        public ConfigItem(string key, string value)
        {
            Key = key;
            Value = value;
        }

        [XmlElement("item")]
        public List<ConfigItem> Items { get; set; }

        [XmlAttribute("key")]
        /// <summary>
        /// 配置项键名
        /// </summary>
        public string Key { get; set; }

        [XmlAttribute("value")]
        /// <summary>
        /// 配置项值
        /// </summary>
        public string Value { get; set; }

        [XmlAttribute("default")]
        /// <summary>
        /// 配置项值
        /// </summary>
        public bool IsDefault { get; set; }

        [XmlAttribute("description")]
        /// <summary>
        /// 配置项描述内容
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 任意属性
        /// </summary>
        [XmlAnyAttribute]
        public List<XmlAttribute> Attributes { get; set; }

        #region 索引器       

        public ConfigItem this[string key]
        {
            get
            {
                foreach (var item in Items)
                {
                    if (item.Key == key)
                    {
                        return item;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// 是否存在某项
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Exist(string key)
        {
            return this[key] != null;
        }

        #endregion

        public override string ToString()
        {
            return Value.IsEmpty() ? Key : (Key + ":" + Value);
        }
    }
}

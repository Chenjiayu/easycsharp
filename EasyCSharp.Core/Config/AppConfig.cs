﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.Core
{
    public class AppConfig
    {
        public string this[string key]
        {
            get
            {
                return LoadAppSettingValue(key);
            }
        }
        public string this[int index]
        {
            get
            {
                return LoadAppSettingValue(index);
            }
        }

        /// <summary>
        /// 根据key加载appsetting下的项
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string LoadAppSettingValue(string key)
        {
            return ConfigurationManager.AppSettings[key] ?? "";
        }

        /// <summary>
        /// 根据index加载appsetting下的项
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string LoadAppSettingValue(int index)
        {
            return ConfigurationManager.AppSettings[index] ?? "";
        }
    }
}

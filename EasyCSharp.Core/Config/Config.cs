﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace EasyCSharp.Core
{
    [XmlRoot("modules")]
    public class Config
    {
        [XmlIgnore]
        /// <summary>
        /// 文件绝对路径
        /// </summary>
        public string Path { get; private set; }

        [XmlElement("module")]
        /// <summary>
        /// 下级配置内容项
        /// </summary>
        public List<ConfigItem> Items { get; set; }

        #region 索引器

        public ConfigItem this[int index]
        {
            get
            {
                return Items[index];
            }
            set
            {
                Items[index] = value;
            }
        }

        public ConfigItem this[string key]
        {
            get
            {
                if (Items == null)
                {
                    return new ConfigItem();
                }

                foreach (var item in Items)
                {
                    if (item.Key == key)
                    {
                        return item;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// 通过路径获取ConfigItem
        /// </summary>
        /// <param name="configPath"></param>
        /// <returns></returns>
        public ConfigItem Get(string configPath)
        {
            string[] items = configPath.Split('/');

            if (items.Count() <= 0)
            {
                return null;
            }

            ConfigItem configItem = default(ConfigItem);
            for (int i = 0; i < items.Count(); i++)
            {
                if (configItem == null)
                {
                    configItem = this[items[0]];
                }
                else
                {
                    if (configItem.Exist(items[i]))
                    {
                        configItem = configItem[items[i]];
                    }
                    else
                    {
                        configItem = null;
                        break;
                    }
                }
            }

            return configItem;
        }

        #endregion

        #region XML

        /// <summary>
        /// 加载XML配置文件
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Config LoadXML(string path)
        {
            //若给定路径找不到配置文件，
            if (!File.Exists(path))
            {
                //throw new FileNotFoundException(path);
                return new Config();
            }

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                XmlSerializer xs = new XmlSerializer(typeof(Config));

                using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(doc.InnerXml)))
                {
                    Config config = xs.Deserialize(ms) as Config;
                    config.Path = path;
                    return config;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 保存配置文件
        /// </summary>
        /// <param name="filePath"></param>
        public void SaveXML(string filePath)
        {
            try
            {
                var type = typeof(Config);

                using (StreamWriter writer = new StreamWriter(filePath))
                {
                    System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(type);
                    xmlSerializer.Serialize(writer, this);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion
    }
}

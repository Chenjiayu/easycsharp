﻿using EasyCSharp.Core;
using System;
using System.Globalization;
using System.Windows.Data;

namespace EasyCSharp.WPF.Core
{
    public class EnumToDescriptionConverter : GenericInstanceMarkupExtension<EnumToDescriptionConverter>, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Enum @enum)
            {
                return @enum?.GetEnumDescription() ?? value.ToString();
            }

            return value?.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace EasyCSharp.WPF.Core
{
    public class BooleanToVisibilityConverter : GenericInstanceMarkupExtension<BooleanToVisibilityConverter>, IValueConverter
    {
        public bool Inverse { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool val = value == null ? false : (bool)value;
            return Inverse ? (val ? Visibility.Collapsed : Visibility.Visible) : (val ? Visibility.Visible : Visibility.Collapsed);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class BooleansToVisibilityConverter : GenericInstanceMarkupExtension<BooleansToVisibilityConverter>, IMultiValueConverter
    {
        public bool Inverse { get; set; }

        public bool Any { get; set; }


        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length <= 0)
            {
                return Inverse ? Visibility.Collapsed : Visibility.Visible;
            }
            else
            {
                var val = true;
                foreach (var v in values)
                {
                    val &= ((bool)v);
                    if (Any && val)
                    {
                        break;
                    }
                }

                return Inverse ? (val ? Visibility.Collapsed : Visibility.Visible) : (val ? Visibility.Visible : Visibility.Collapsed);
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}

﻿using System;
using System.Collections;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace EasyCSharp.WPF.Core
{
    public class ItemsCountToVisibilityConverter : GenericInstanceMarkupExtension<NullToVisibilityConverter>, IValueConverter
    {
        public bool Inverse { get; set; }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is IEnumerable list)
            {
                foreach(var i in list)
                {
                    return Inverse ? Visibility.Collapsed : Visibility.Visible;
                }

                return Inverse ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                return Inverse ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

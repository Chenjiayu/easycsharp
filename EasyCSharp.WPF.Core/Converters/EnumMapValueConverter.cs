﻿using EasyCSharp.Core;
using System;
using System.Globalization;
using System.Windows.Data;

namespace EasyCSharp.WPF.Core
{
    public class EnumMapValueConverter : GenericInstanceMarkupExtension<EnumMapValueConverter>, IValueConverter
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Type type = value.GetType();
            var em = Enum.Parse(type, value.ToString()) as Enum;
            return em.GetEnumDescription();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

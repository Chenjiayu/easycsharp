﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace EasyCSharp.WPF.Core
{
    public class NullToVisibilityConverter : GenericInstanceMarkupExtension<NullToVisibilityConverter>, IValueConverter
    {
        public bool Inverse { get; set; }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool val = (value is string) ? string.IsNullOrEmpty(value.ToString()) : value == null;
            return !Inverse ? (val ? Visibility.Collapsed : Visibility.Visible) : (val ? Visibility.Visible : Visibility.Collapsed);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

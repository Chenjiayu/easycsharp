﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace EasyCSharp.WPF.Core
{
    public static class Execute
    {
        // 主UI线程
        private static readonly Dispatcher dispatcher = Application.Current.Dispatcher;

        /// <summary>
        /// 同步执行
        /// </summary>
        /// <param name="action"></param>
        public static void OnUIThread(this Action action)
        {
            if (dispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                Exception exception = null;
                Action method = () =>
                {
                    try
                    {
                        action();
                    }
                    catch (Exception ex)
                    {
                        exception = ex;
                    }
                };

                dispatcher.Invoke(method);

                if (exception != null)
                {
                    throw new Exception("UI线程调用异常:" + exception.Message, exception);
                    //throw new TargetInvocationException("UI线程调用异常", exception);
                }
            }
        }

        /// <summary>
        /// 异步执行
        /// </summary>
        /// <param name="Action"></param>
        public static void BeginOnUIThread(this Action action)
        {
            if (dispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                dispatcher.BeginInvoke(action);
            }
        }

        /// <summary>
        /// Task方式异步执行
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public static Task OnUIThreadAsync(this Action action)
        {
            if (dispatcher.CheckAccess())
            {
                action();
                return Task.FromResult(false);
            }
            else
            {
                var tcs = new TaskCompletionSource<object>();
                Action method = () =>
                {
                    try
                    {
                        action();
                        tcs.SetResult(null);
                    }
                    catch (Exception ex)
                    {
                        tcs.SetException(ex);
                    }
                };
                dispatcher.BeginInvoke(method);

                return tcs.Task;
            }
        }
    }
}

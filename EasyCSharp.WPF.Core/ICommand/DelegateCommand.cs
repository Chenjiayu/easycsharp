﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace EasyCSharp.WPF.Core
{
    public abstract class DelegateCommandBase : ICommand
    {
        private Action<object> execute;
        private Predicate<object> canExecute;

        protected DelegateCommandBase(Action<object> execute) : this(execute, null)
        {

        }

        protected DelegateCommandBase(Action<object> execute, Predicate<object> canExecute)
        {
            this.execute = execute ?? throw new ArgumentNullException("execute", "命令方法不能为空");
            this.canExecute = canExecute;
        }


        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public virtual bool CanExecute(object parameter)
        {
            return this.canExecute == null ? true : canExecute(parameter);
        }

        public virtual void Execute(object parameter)
        {
            if (CanExecute(parameter))
            {
                this.execute?.Invoke(parameter);
            }
        }
    }

    public class DelegateCommand : DelegateCommandBase
    {
        public DelegateCommand(Action<object> excute) : base(excute)
        {

        }

        public DelegateCommand(Action<object> excute, Func<object, bool> canExcute) : base(excute, o => canExcute(o))
        {

        }

        public DelegateCommand(Action excute) : base(o => excute())
        {

        }

        public DelegateCommand(Action excute, Func<bool> canExcute) : base(o => excute(), o => canExcute())
        {

        }
    }

    public class DelegateCommand<T> : DelegateCommandBase
    {
        public DelegateCommand(Action<T> excute) : base(o => excute((T)o))
        {

        }

        public DelegateCommand(Action<T> excute, Func<T, bool> canExcute) : base(o => excute((T)o), o => canExcute((T)o))
        {

        }
    }
}

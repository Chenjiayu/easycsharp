﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Interactivity;

namespace EasyCSharp.WPF.Core
{
    public class AutoSelectFirstBehavior : Behavior<Selector>
    {
        protected override void OnAttached()
        {
            base.OnAttached();

            if (AssociatedObject.Items != null && AssociatedObject.SelectedItem == null)
            {
                AssociatedObject.SelectedIndex = 0;
            }

            var dpd = DependencyPropertyDescriptor.FromProperty(ItemsControl.ItemsSourceProperty, typeof(Selector));
            dpd.AddValueChanged(AssociatedObject, (s, e) =>
            {
                if (AssociatedObject.SelectedItem == null)
                {
                    AssociatedObject.SelectedIndex = 0;
                }
            });

            var dpd2 = DependencyPropertyDescriptor.FromProperty(FrameworkElement.DataContextProperty, typeof(Selector));
            dpd2.AddValueChanged(AssociatedObject, (s, e) =>
            {
                if (AssociatedObject.SelectedItem == null)
                {
                    AssociatedObject.SelectedIndex = 0;
                }
            });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace EasyCSharp.WPF.Core
{
    public class ThumbDragDeltaBehavior : Behavior<Thumb>
    {
        Cursor old;
        public FrameworkElement Target
        {
            get { return (FrameworkElement)GetValue(TargetProperty); }
            set { SetValue(TargetProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Target.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TargetProperty =
            DependencyProperty.Register("Target", typeof(FrameworkElement), typeof(ThumbDragDeltaBehavior), new PropertyMetadata(null));


        protected override void OnAttached()
        {
            base.OnAttached();
            old = this.AssociatedObject.Cursor;
            this.AssociatedObject.Cursor = Cursors.SizeAll;
            this.AssociatedObject.DragDelta += AssociatedObject_DragDelta;
            this.AssociatedObject.DragStarted += AssociatedObject_DragStarted;
            this.AssociatedObject.DragCompleted += AssociatedObject_DragCompleted;

        }

        private void AssociatedObject_DragStarted(object sender, DragStartedEventArgs e)
        {
            if (Target == null)
            {
                var top = Canvas.GetTop(AssociatedObject);
                var left = Canvas.GetLeft(AssociatedObject);

                if (double.IsNaN(top))
                {
                    Canvas.SetTop(AssociatedObject, 0);
                }

                if (double.IsNaN(left))
                {
                    Canvas.SetLeft(AssociatedObject, 0);
                }
            }
            else
            {
                var top = Canvas.GetTop(Target);
                var left = Canvas.GetLeft(Target);

                if (double.IsNaN(top))
                {
                    Canvas.SetTop(Target, 0);
                }

                if (double.IsNaN(left))
                {
                    Canvas.SetLeft(Target, 0);
                }
            }
        }

        private void AssociatedObject_DragCompleted(object sender, DragCompletedEventArgs e)
        {

        }

        private void AssociatedObject_DragDelta(object sender, DragDeltaEventArgs e)
        {
            if (Target == null)
            {
                var top = Canvas.GetTop(AssociatedObject);
                var left = Canvas.GetLeft(AssociatedObject);
                Canvas.SetTop(AssociatedObject, top + e.VerticalChange);
                Canvas.SetLeft(AssociatedObject, left + e.HorizontalChange);
            }
            else
            {
                var top = Canvas.GetTop(Target);
                var left = Canvas.GetLeft(Target);
                Canvas.SetTop(Target, top + e.VerticalChange);
                Canvas.SetLeft(Target, left + e.HorizontalChange);
            }
        }

        protected override void OnDetaching()
        {
            this.AssociatedObject.Cursor = old;
            this.AssociatedObject.DragDelta -= AssociatedObject_DragDelta;
            this.AssociatedObject.DragStarted -= AssociatedObject_DragStarted;
            this.AssociatedObject.DragCompleted -= AssociatedObject_DragCompleted;
            base.OnDetaching();
        }
    }
}

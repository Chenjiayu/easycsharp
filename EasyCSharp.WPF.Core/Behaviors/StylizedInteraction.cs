﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interactivity;

namespace EasyCSharp.WPF.Core
{
    public class CustomBehaviorCollection : Collection<Behavior>
    {
    }

    public class CustomTriggerCollection : Collection<System.Windows.Interactivity.TriggerBase>
    {
    }

    public class StylizedInteraction
    {
        #region Behaviors

        public static readonly DependencyProperty BehaviorsProperty = DependencyProperty.RegisterAttached(
            "Behaviors",
            typeof(CustomBehaviorCollection),
            typeof(StylizedInteraction),
            new FrameworkPropertyMetadata(null, OnBehaviorsChanged));

        public static CustomBehaviorCollection GetBehaviors(DependencyObject obj)
        {
            return (CustomBehaviorCollection)obj.GetValue(BehaviorsProperty);
        }

        public static void SetBehaviors(DependencyObject obj, CustomBehaviorCollection value)
        {
            obj.SetValue(BehaviorsProperty, value);
        }

        private static void OnBehaviorsChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var collection = e.NewValue as CustomBehaviorCollection;
            if (collection != null)
            {
                var behaviors = Interaction.GetBehaviors(obj);
                behaviors.Clear();
                foreach (var behavior in collection)
                {
                    behaviors.Add(behavior);
                }
            }
        }

        #endregion

        #region Triggers

        public static readonly DependencyProperty TriggersProperty = DependencyProperty.RegisterAttached(
            "Triggers",
            typeof(CustomTriggerCollection),
            typeof(StylizedInteraction),
            new FrameworkPropertyMetadata(null, OnTriggersChanged));

        public static CustomTriggerCollection GetTriggers(DependencyObject obj)
        {
            return (CustomTriggerCollection)obj.GetValue(TriggersProperty);
        }

        public static void SetTriggers(DependencyObject obj, CustomTriggerCollection value)
        {
            obj.SetValue(TriggersProperty, value);
        }

        private static void OnTriggersChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var collection = e.NewValue as CustomTriggerCollection;
            if (collection != null)
            {
                var triggers = Interaction.GetTriggers(obj);
                triggers.Clear();
                foreach (var trigger in collection)
                {
                    triggers.Add(trigger);
                }
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace EasyCSharp.WPF.Core
{
    public class SizeModeContentBehavior : Behavior<ContentControl>
    {
        ContentSizeMapCollection collection;
        object DefaultContent;

        protected override void OnAttached()
        {
            base.OnAttached();

            collection = ContentSizeMap.GetContentSizeMapCollection(AssociatedObject);

            if (AssociatedObject.IsLoaded)
            {
                DefaultContent = AssociatedObject.Content;
                ReLoad(AssociatedObject.DesiredSize);
            }

            AssociatedObject.SizeChanged += AssociatedObject_SizeChanged;
        }

        private void AssociatedObject_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //去小于等于某一区间，则取改区间值
            ReLoad(e.NewSize);
        }

        public void ReLoad(Size size)
        {
            if (collection == null)
            {
                return;
            }

            foreach (var item in collection)
            {
                if (item.Suit(size.Width, size.Height))
                {
                    AssociatedObject.Content = item.Content;
                    return;
                }
            }

            AssociatedObject.Content = DefaultContent;
        }

        protected override void OnDetaching()
        {
            AssociatedObject.SizeChanged -= AssociatedObject_SizeChanged;
            AssociatedObject.Content = DefaultContent;
        }
    }
}

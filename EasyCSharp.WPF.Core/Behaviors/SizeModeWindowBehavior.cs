﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interactivity;

namespace EasyCSharp.WPF.Core
{
    public class SizeModeWindowBehavior : Behavior<Window>
    {
        private double PreviousWidth;
        private double PreviousHeight;
        private double ScreenWidth;
        private double ScreenHeight;
        private ResizeMode PreviousWindowResizeMode;
        public bool ChangeResizeMode { get; set; }
        protected override void OnAttached()
        {
            base.OnAttached();
            ScreenWidth = SystemParameters.PrimaryScreenWidth;
            ScreenHeight = SystemParameters.PrimaryScreenHeight;

            if (AssociatedObject.IsLoaded)
            {
                Resize();
            }
            else
            {
                AssociatedObject.Loaded += AssociatedObject_Loaded;
            }
        }

        protected override void OnDetaching()
        {
            Restore();
            base.OnDetaching();
        }

        private void AssociatedObject_Loaded(object sender, RoutedEventArgs e)
        {
            Resize();
        }

        private void Resize()
        {
            var mode = WindowSizeMap.GetWindowSizeMapMode(AssociatedObject);
            if (mode != WindowSizeMapMode.Default)
            {
                PreviousWidth = AssociatedObject.ActualWidth;
                PreviousHeight = AssociatedObject.ActualHeight;
                PreviousWindowResizeMode = AssociatedObject.ResizeMode;

                if (mode == WindowSizeMapMode.AutoProportion)
                {
                    var scale = WindowSizeMap.GetScale(AssociatedObject);
                    AssociatedObject.Width = ScreenWidth * scale;
                    AssociatedObject.Height = ScreenHeight * scale;
                }
                else
                {
                    var collection = WindowSizeMap.GetWindowSizeMaps(AssociatedObject);
                    if (collection?.Count >= 0)
                    {
                        foreach (var item in collection)
                        {
                            if (item.Suit(ScreenWidth, ScreenHeight))
                            {
                                if (ChangeResizeMode)
                                {
                                    AssociatedObject.ResizeMode = ResizeMode.NoResize;
                                }
                                AssociatedObject.Width = item.WindowSize.Width;
                                AssociatedObject.Height = item.WindowSize.Height;

                                if (AssociatedObject.WindowStartupLocation == WindowStartupLocation.CenterScreen)
                                {
                                    AssociatedObject.Left = (ScreenWidth - item.WindowSize.Width) / 2;
                                    AssociatedObject.Top = (ScreenHeight - item.WindowSize.Height) / 2;
                                }

                                break;
                            }
                        }
                    }
                }
            }
        }

        private void Restore()
        {
            var mode = WindowSizeMap.GetWindowSizeMapMode(AssociatedObject);
            if (mode == WindowSizeMapMode.Default)
            {
                return;
            }
            AssociatedObject.ResizeMode = PreviousWindowResizeMode;
            AssociatedObject.Width = PreviousWidth;
            AssociatedObject.Height = PreviousHeight;
        }
    }
}

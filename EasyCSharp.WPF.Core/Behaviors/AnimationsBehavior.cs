﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interactivity;
using System.Windows.Media.Animation;

namespace EasyCSharp.WPF.Core
{
    public abstract class AnimationsBehavior : Behavior<FrameworkElement>
    {
        public bool Enable
        {
            get { return (bool)GetValue(EnableProperty); }
            set { SetValue(EnableProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Enable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnableProperty =
            DependencyProperty.Register("Enable", typeof(bool), typeof(AnimationsBehavior), new PropertyMetadata(true));


        public Storyboard Storyboard
        {
            get { return (Storyboard)GetValue(StoryboardProperty); }
            set { SetValue(StoryboardProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Storyboard.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StoryboardProperty =
            DependencyProperty.Register("Storyboard", typeof(Storyboard), typeof(AnimationsBehavior), new PropertyMetadata(null));
    }
    public class MouseEnterAnimationBehavior : AnimationsBehavior
    {
        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.MouseEnter += AssociatedObject_MouseEnter;
        }

        private void AssociatedObject_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (Storyboard != null && Enable)
                AssociatedObject.BeginStoryboard(Storyboard);
        }

        protected override void OnDetaching()
        {
            AssociatedObject.MouseEnter -= AssociatedObject_MouseEnter;
            base.OnDetaching();
        }
    }
    public class MouseLeaveAnimationBehavior : AnimationsBehavior
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.MouseLeave += AssociatedObject_MouseLeave;
        }

        private void AssociatedObject_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (Storyboard != null && Enable)
                AssociatedObject.BeginStoryboard(Storyboard);
        }

        protected override void OnDetaching()
        {
            AssociatedObject.MouseLeave -= AssociatedObject_MouseLeave;
            base.OnDetaching();
        }
    }

    public class GotFocusAnimationBehavior : AnimationsBehavior
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.GotFocus += AssociatedObject_GotFocus;
        }

        private void AssociatedObject_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Storyboard != null && Enable)
                AssociatedObject.BeginStoryboard(Storyboard);
        }

        protected override void OnDetaching()
        {
            AssociatedObject.GotFocus -= AssociatedObject_GotFocus;
            base.OnDetaching();
        }
    }

    public class LostFocusAnimationBehavior : AnimationsBehavior
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.LostFocus += AssociatedObject_LostFocus;
        }

        private void AssociatedObject_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Storyboard != null && Enable)
                AssociatedObject.BeginStoryboard(Storyboard);
        }

        protected override void OnDetaching()
        {
            AssociatedObject.LostFocus -= AssociatedObject_LostFocus;
            base.OnDetaching();
        }
    }
}

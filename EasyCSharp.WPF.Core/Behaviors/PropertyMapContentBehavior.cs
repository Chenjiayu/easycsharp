﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace EasyCSharp.WPF.Core
{
    public class PropertyMapContentBehavior : Behavior<ContentControl>
    {
        public PropertyMapContentCollection Collection
        {
            get; set;
        }

        public object Target
        {
            get { return (object)GetValue(TargetProperty); }
            set { SetValue(TargetProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Target.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TargetProperty =
            DependencyProperty.Register("Target", typeof(object), typeof(PropertyMapContentBehavior), new PropertyMetadata(null, OnTargetChanged));

        private static void OnTargetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as PropertyMapContentBehavior).Render();
        }

        private object PreviousContent;

        protected override void OnAttached()
        {
            base.OnAttached();           
            PreviousContent = AssociatedObject.Content;
            Render();
        }

        private void Render()
        {
            if (Collection == null)
            {
                return;
            }

            foreach (var item in Collection)
            {
                if (item.Suit(Target))
                {
                    AssociatedObject.SetValue(ContentControl.ContentProperty, item.Content);
                    return;
                }
            }

            AssociatedObject.Content = PreviousContent;
        }

        protected override void OnDetaching()
        {           
            base.OnDetaching();
        }
    }

    public class PropertyMapContentCollection : Collection<PropertyMapContentItem>
    {

    }

    public class PropertyMapContentItem : DependencyObject
    {
        public object Value
        {
            get { return (object)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(object), typeof(PropertyMapContentItem), new PropertyMetadata(null));


        public object Content
        {
            get { return (object)GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Content.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContentProperty =
            DependencyProperty.Register("Content", typeof(object), typeof(PropertyMapContentItem), new PropertyMetadata(null));

        public bool Suit(object value)
        {
            if (value == null) return false;
            var str = Convert.ToString(value).ToLower();
            return Value.ToString() == str;
        }
    }
}

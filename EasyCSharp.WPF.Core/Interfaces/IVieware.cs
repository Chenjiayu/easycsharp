﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace EasyCSharp.WPF.Core
{
    public interface IVieware
    {
        FrameworkElement View { get; }       
        void AttachView(FrameworkElement view);
        void TryClose(bool? result = null);
        bool CanClose(CancelEventArgs args);
        //bool Focus(Expression<Func<object>> lambda);
        void OnOpened();
        void OnReOpened();
        void OnClosed();
        void Refresh();
        bool IsBusy { get; }
    }
}

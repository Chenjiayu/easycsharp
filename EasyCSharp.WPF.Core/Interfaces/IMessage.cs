﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.WPF.Core
{
    public interface IMessage
    {
        object Group { get; set; }

        object Content { get; set; }
    }

    public interface IMessage<TContent> : IMessage
    {
        new TContent Content { get; set; }
    }
}

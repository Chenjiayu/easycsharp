﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace EasyCSharp.WPF.Core
{
    /// <summary>
    /// 根据窗户口尺寸改变自身尺寸的响应式方法
    /// </summary>
    public class WindowSizeMap : DependencyObject
    {
        public static WindowSizeMapCollection GetWindowSizeMaps(DependencyObject obj)
        {
            return (WindowSizeMapCollection)obj.GetValue(WindowSizeMapsProperty);
        }

        public static void SetWindowSizeMaps(DependencyObject obj, WindowSizeMapCollection value)
        {
            obj.SetValue(WindowSizeMapsProperty, value);
        }

        // Using a DependencyProperty as the backing store for WindSizeMaps.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WindowSizeMapsProperty =
            DependencyProperty.RegisterAttached("WindowSizeMaps", typeof(WindowSizeMapCollection), typeof(WindowSizeMap), new PropertyMetadata(null));

        public static WindowSizeMapMode GetWindowSizeMapMode(DependencyObject obj)
        {
            return (WindowSizeMapMode)obj.GetValue(WindowSizeMapModeProperty);
        }

        public static void SetWindowSizeMapMode(DependencyObject obj, WindowSizeMapMode value)
        {
            obj.SetValue(WindowSizeMapModeProperty, value);
        }

        // Using a DependencyProperty as the backing store for WindowSizeMapMode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WindowSizeMapModeProperty =
            DependencyProperty.RegisterAttached("WindowSizeMapMode", typeof(WindowSizeMapMode), typeof(WindowSizeMap), new PropertyMetadata(WindowSizeMapMode.Default));

        public static double GetScale(DependencyObject obj)
        {
            return (double)obj.GetValue(ScaleProperty);
        }

        public static void SetScale(DependencyObject obj, double value)
        {
            obj.SetValue(ScaleProperty, value);
        }

        // Using a DependencyProperty as the backing store for Scale.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScaleProperty =
            DependencyProperty.RegisterAttached("Scale", typeof(double), typeof(WindowSizeMap), new PropertyMetadata(1.0));

    }
    public class WindowSizeMapCollection : Collection<WindowSizeMapItem>
    {

    }

    public class WindowSizeMapItem
    {
        public Size ScreenSize { get; set; }
        public Size WindowSize { get; set; }

        public bool Suit(double width, double height)
        {
            return ScreenSize.Equals(new Size(width, height));
        }
    }


    public enum WindowSizeMapMode
    {
        /// <summary>
        /// 默认，不更改模式、长款宽
        /// </summary>
        Default,
        /// <summary>
        /// 按比例修改长宽，不更改模式
        /// </summary>
        AutoProportion,
        /// <summary>
        /// 映射模式，寻找相应分辨率下的显示尺寸，只适用模式NoResize
        /// </summary>
        Map
    }
}

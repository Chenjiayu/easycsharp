﻿using System.Collections.ObjectModel;
using System.Windows;

namespace EasyCSharp.WPF.Core
{
    /// <summary>
    /// 根据控件尺寸改变内容
    /// </summary>
    public class ContentSizeMap : DependencyObject
    {
        public static ContentSizeMapCollection GetContentSizeMapCollection(DependencyObject obj)
        {
            return (ContentSizeMapCollection)obj.GetValue(ContentSizeMapCollectionProperty);
        }

        public static void SetContentSizeMapCollection(DependencyObject obj, ContentSizeMapCollection value)
        {
            obj.SetValue(ContentSizeMapCollectionProperty, value);
        }

        // Using a DependencyProperty as the backing store for ContentSizeMapCollection.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContentSizeMapCollectionProperty =
            DependencyProperty.RegisterAttached("ContentSizeMapCollection", typeof(ContentSizeMapCollection), typeof(ContentSizeMap), new PropertyMetadata(null));


    }
    public class ContentSizeMapCollection : Collection<ContentSizeMapItem>
    {

    }
    public class ContentSizeMapItem
    {
        public Size ControlSize { get; set; }
        public object Content { get; set; }

        public bool Suit(double width, double height)
        {           
            return (ControlSize.Width == 0.0 || ControlSize.Width > width)
                && (ControlSize.Height == 0.0 || ControlSize.Height > height);
        }
    }
}

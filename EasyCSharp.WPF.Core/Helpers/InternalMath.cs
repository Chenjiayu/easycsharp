﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCSharp.WPF.Core
{
    internal class InternalMath
    {
        public static bool IsVerySmall(double value) => Math.Abs(value) < 1E-06;
    }  
}

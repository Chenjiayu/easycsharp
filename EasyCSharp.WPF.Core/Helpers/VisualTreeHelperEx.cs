﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace EasyCSharp.WPF.Core
{
    public class VisualTreeHelperEx
    {
        public static TChild FindChild<TChild>(DependencyObject obj, Predicate<TChild> key = null) where TChild : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                if (child != null && child is TChild tChild && (key == null || key.Invoke(tChild)))
                {
                    return tChild;
                }
                else
                {
                    TChild childOfChild = FindChild(child, key);
                    if (childOfChild != null)
                    {
                        return childOfChild;
                    }
                }
            }
            return null;
        }

        public static TParent FindParent<TParent>(DependencyObject obj, Predicate<TParent> key = null) where TParent : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject parent = VisualTreeHelper.GetParent(obj);

                if (parent == null)
                {
                    return null;
                }

                if (parent != null && parent is TParent tParent && (key == null || key.Invoke(tParent)))
                {
                    return tParent;
                }
                else
                {
                    TParent tP = FindParent(parent, key);
                    if (tP != null)
                    {
                        return tP;
                    }
                }
            }
            return null;
        }
    }
}

﻿using EasyCSharp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace EasyCSharp.WPF.Core
{
    public class GenericInstanceMarkupExtension<T> : MarkupExtension where T : class, new()
    {
        private static T Instance;

        static GenericInstanceMarkupExtension()
        {
            Instance = new T();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Instance;
        }
    }

    public class EnumMapsExtension : MarkupExtension
    {
        private Type _enumType;

        public EnumMapsExtension(Type enumType)
        {
            EnumType = enumType ?? throw new ArgumentNullException("enumType");
        }

        public Type EnumType
        {
            get { return _enumType; }
            private set
            {
                if (_enumType == value)
                    return;

                var enumType = Nullable.GetUnderlyingType(value) ?? value;

                if (enumType.IsEnum == false)
                    throw new ArgumentException("Type must be an Enum.");

                _enumType = value;
            }
        }

        public override object ProvideValue(IServiceProvider serviceProvider) => _enumType.GetEnumMaps();
    }
}

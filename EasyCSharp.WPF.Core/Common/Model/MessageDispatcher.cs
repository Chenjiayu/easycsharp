﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace EasyCSharp.WPF.Core
{
    public class MessageDispatcher<TMessage> where TMessage : IMessage
    {
        public static Dictionary<Type, List<Subscriber<TMessage>>> Tasks = new Dictionary<Type, List<Subscriber<TMessage>>>();

        /// <summary>
        /// 订阅
        /// </summary>
        /// <typeparam name="TMessage"></typeparam>
        /// <param name="target"></param>
        /// <param name="action"></param>
        public static void Subscribe(object target, Action<TMessage> action, Predicate<TMessage> predicate = null, ThreadOption threadOption = ThreadOption.Background)
        {
            Type type = typeof(TMessage);

            if (!Tasks.Keys.Contains(type))
                Tasks.Add(type, new List<Subscriber<TMessage>>());

            var subscriber = Tasks[type].Where(i => i.Target == target).FirstOrDefault();

            if (subscriber == null)
            {
                subscriber = new Subscriber<TMessage>(target, action, predicate)
                {
                    ThreadOption = threadOption
                };
                Tasks[type].Add(subscriber);
            }
        }

        /// <summary>
        /// 推送
        /// </summary>
        /// <typeparam name="TMessage"></typeparam>
        /// <param name="message"></param>
        public static void Publish(TMessage message)
        {
            Type type = typeof(TMessage);

            if (!Tasks.Keys.Contains(type))
            {
                return;
            }
            foreach (var suber in Tasks[type])
            {
                InvokeAsync(suber, message);
            }
        }

        private static void InvokeAsync(Subscriber<TMessage> subscriber, TMessage msg)
        {
            if (subscriber.Predicate == null || subscriber.Predicate.Invoke(msg))
            {
                if (subscriber.ThreadOption == ThreadOption.Background)
                {
                    Task.Run(() =>
                    {
                        subscriber.Action?.Invoke(msg);
                    });
                }
                else if (subscriber.ThreadOption == ThreadOption.InDispatcher)
                {
                    subscriber.Action?.Invoke(msg);
                }
                else if (subscriber.ThreadOption == ThreadOption.UI)
                {
                    Application.Current.Dispatcher.Invoke(subscriber.Action, new object[] { msg });
                }
            }
        }
    }

    public class Subscriber<TMessage> where TMessage : IMessage
    {
        public Subscriber(object target, Action<TMessage> action, Predicate<TMessage> predicate = null)
        {
            Target = target;
            Action = action;
            Predicate = predicate;
        }

        public object Target { get; private set; }

        public Predicate<TMessage> Predicate { get; set; }

        public Action<TMessage> Action { get; set; }

        public ThreadOption ThreadOption { get; set; }
    }

    public enum ThreadOption
    {
        [Description("同调度线程")]
        InDispatcher,
        [Description("UI主线程")]
        UI,
        [Description("后台线程")]
        Background
    }
}

﻿using EasyCSharp.Core;
using EasyCSharp.IOC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace EasyCSharp.WPF.Core
{
    public abstract class BootStrapperBase
    {
        public string Startup { get; set; }
        public BootStrapperBase()
        {
            Initialize();
        }

        #region protected

        protected CompositionContainer Container;

        protected virtual void OnDeactivated(object sender, EventArgs e)
        {

        }

        protected virtual void OnActivated(object sender, EventArgs e)
        {

        }

        protected virtual void OnExit(object sender, ExitEventArgs e)
        {
            Environment.Exit(0);
        }

        protected virtual void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayEntryView();
        }

        protected virtual void Current_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            OnUnhandledException(e.Exception);
            e.Handled = true;
        }

        protected virtual void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            OnUnhandledException(e.ExceptionObject as Exception);
        }

        protected virtual void OnUnhandledException(Exception e)
        {
            System.Windows.MessageBox.Show(e.Message);
            Environment.Exit(0);
        }

        protected virtual void DisplayEntryView()
        {
            if (Startup.IsEmpty())
            {
                throw new Exception($"无法定位入口视图{Startup}");
            }
        }

        /// <summary>
        /// 获取对象实例
        /// </summary>
        /// <param name="service"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        protected virtual object GetInstance(Type service, string key)
        {
            return Activator.CreateInstance(service);
        }

        /// <summary>
        /// 获取指定类型所有实例
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        protected virtual IEnumerable<object> GetAllInstances(Type service)
        {
            return new[] { Activator.CreateInstance(service) };
        }

        /// <summary>
        /// 向IoC容器注册类型实例
        /// </summary>
        /// <param name="instance"></param>
        protected virtual void BuildUp(object instance)
        {
            Container.SatisfyImportsOnce(instance);
        }

        protected virtual void RegisterPlugins(string[] folders = null, Predicate<string> key = null)
        {

            if (folders != null && folders.Length > 0)
            {
                IoC.Assemblies = new List<System.Reflection.Assembly>();               
                var currentPath = Directory.GetCurrentDirectory();
                foreach (var folder in folders)
                {
                    if (Directory.Exists(folder))
                    {
                        var folderPath = Path.Combine(currentPath, folder);
                        IoC.Assemblies.AddRange(AssemblyHelper.Load(folderPath, key).ToList());
                    }
                }

                Container = new CompositionContainer(
              new AggregateCatalog(IoC.Assemblies.Select(x => new AssemblyCatalog(x)).OfType<ComposablePartCatalog>()));
            }
            else
            {
                IoC.Assemblies = new List<System.Reflection.Assembly>();
                Container = new CompositionContainer();
            }
        }

        protected virtual void RegisterServiceComponets(CompositionBatch batch)
        {

        }

        protected virtual void InitDynamicResources()
        {

        }

        #endregion

        #region private

        private void Initialize()
        {
            EventsHandler();
            InitIoC();
            InitDynamicResources();
        }

        private void InitIoC()
        {
            IoC.GetInstance = GetInstance;
            IoC.GetAllInstances = GetAllInstances;
            IoC.BuildUp = BuildUp;

            IOCRegister();
        }

        private void IOCRegister()
        {
            var batch = new CompositionBatch();
            RegisterPlugins();
            RegisterServiceComponets(batch);
            Container.Compose(batch);
        }

        /// <summary>
        /// 应用程序事件绑定
        /// </summary>
        private void EventsHandler()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            //AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
            //AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += CurrentDomain_ReflectionOnlyAssemblyResolve; ;
            //AppDomain.CurrentDomain.AssemblyLoad += CurrentDomain_AssemblyLoad;
           
            Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
            Application.Current.Startup += OnStartup;
            Application.Current.Exit += OnExit;
            Application.Current.Activated += OnActivated;
            Application.Current.Deactivated += OnDeactivated;
        }

        private void CurrentDomain_AssemblyLoad(object sender, AssemblyLoadEventArgs args)
        {
           
        }

        private System.Reflection.Assembly CurrentDomain_ReflectionOnlyAssemblyResolve(object sender, ResolveEventArgs args)
        {
            return null;
        }

        private System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            return null;
        }

        #endregion
    }
}

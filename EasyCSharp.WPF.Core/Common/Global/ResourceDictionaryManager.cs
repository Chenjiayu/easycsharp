﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace EasyCSharp.WPF.Core
{
    public class ResourceDictionaryManager
    {
        public static T Get<T>(string key) where T : class
        {
            var resource = Find(Application.Current.Resources, key);

            return resource == null ? null : resource as T;
        }
        public static object Find(ResourceDictionary dictionnary, string key)
        {
            var resource = dictionnary[key];
            if (resource == null)
            {
                foreach (var sub in dictionnary.MergedDictionaries)
                {
                    resource = Find(sub, key);
                    if (resource != null)
                    {
                        break;
                    }
                }
            }

            return resource;
        }

        public ResourceDictionary Current { get; protected set; }

        public void ApplyResource(Uri dictionaryUri)
        {
            var dict = new ResourceDictionary { Source = dictionaryUri };
            Collection<ResourceDictionary> mergedDicts = Application.Current.Resources.MergedDictionaries;
            if (!mergedDicts.Contains(dict))
            {
                // 移除资源
                mergedDicts.Remove(Current);
                // 添加资源
                mergedDicts.Add(dict);
                // 保存到当前
                Current = dict;
            }
        }
    }
}
